<?php

namespace App\Http\Controllers\WebGymUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Session;
use DB;
class ContactusController extends Controller
{
    public function index(){
    	return view('web.contact_us.contact_us');
    }

    public function store(Request $r){
        $input = $r->all();
        $msg = "";
        $rules = [
            'name'=>'required',
            'email'=>'required|email',
            'contact'=>'required',
            'message'=>'required',
            'g-recaptcha-response'=>'required',
        ];
        $message = [
            'g-recaptcha-response.required' => 'Captcha is required',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $msg = "Captacha verification failed";
            Session::flash('message', 'alert-danger');
        }

    	if($r->input('g-recaptcha-response') != '') {
            $data['name'] = $r->input('name');
            $data['email'] = $r->input('email');
            $data['contact'] = $r->input('contact');
            $data['message'] = $r->input('message');

            unset($input['_token']);
            $insert = DB::table('contact_us')->insert($data);
            if ($insert) {
                $msg = "Thank you for your query. A member of our team will contact you within 24 hours";
                Session::flash('message', 'alert-success');
            } else {
                $msg = trans('lang_data.error');
                Session::flash('message', 'alert-danger');
            }
        }else{
            $msg = "Captacha verfication failed";
            Session::flash('message', 'alert-danger');
        }
		Session::flash('msg',$msg);
		return redirect()->back();
		
    }
}

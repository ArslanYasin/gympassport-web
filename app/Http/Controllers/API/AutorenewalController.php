<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Subscription;
use App\API\Usersubscription;
use App\API\Plancategory;
use Validator;
use DB;
use App\Usergym;
use App\Uservisitedgym;
use App\Http\Controllers\API\ApiCommanFunctionController;
use App\Autorenewalfaild;
use App\Usercard;

class AutorenewalController extends Controller {

    function __construct() { //json_sendResponse
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->plan_category = new Plancategory;
        $this->user_sub = new Usersubscription;
        $this->user_gym = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;
    }

    public function fetch_last_day_subscription_plan() {
//        Autorenewalfaild::create([
//            'user_id'=>'11',
//            'failed_message'=>'testing cron job',
//        ]);
        $date = date('Y-m-d');
        $status['status'] = '0';

        $today_end_plan = $this->user_sub->getautorenewaldata($date);
        //echo '<pre>';print_r($today_end_plan);die;
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        foreach ($today_end_plan as $key => $value) { //loop start here..
            //echo '<pre>';print_r($value);die;
            $card_number = $value->user_card[0]->user_card_number;
            $amount = $value->amount;
            try {
                $amount = trim($amount * 100);
                $curr = 'pkr';
                $customer_id = $value->customer_id;
                $charge = \Stripe\Charge::create([
                    'amount' => $amount,
                    'currency' => $curr,
                    'customer' => $customer_id,
                ]);
            } catch (\Stripe\Exception\CardException $e) {
                // Since it's a decline, \Stripe\Exception\CardException will be caught
                echo 'Status is:' . $e->getHttpStatus() . '\n';
                echo 'Type is:' . $e->getError()->type . '\n';
                echo 'Code is:' . $e->getError()->code . '\n';
                // param is '' in this case
                echo 'Param is:' . $e->getError()->param . '\n';
                echo 'Message is:' . $e->getError()->message . '\n';
                $failed['failed_message'] = $e->getError()->message;
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            } catch (\Stripe\Exception\RateLimitException $e) {
                $failed['failed_message'] = "Too many requests made to the API too quickly";
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            } catch (\Stripe\Exception\InvalidRequestException $e) {
                $failed['failed_message'] = "Invalid card parameters";
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            } catch (\Stripe\Exception\AuthenticationException $e) {
                $failed['failed_message'] = "Authentication with Stripe's API failed";
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                $failed['failed_message'] = "Network communication with Stripe failed";
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            } catch (\Stripe\Exception\ApiErrorException $e) {
                $failed['failed_message'] = "Something went wrong please try again";
                $failed['user_id'] = $value->user_id;
                Usersubscription::where('user_id', $value->user_id)->update($status);
                Autorenewalfaild::create($failed);
            } catch (Exception $e) {
                $failed['failed_message'] = "Something went wrong please try again";
                $failed['user_id'] = $value->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $value->user_id)->update($status);
            }

            // save payment here
            if (!empty($charge)) {
                $charge_id = $charge->id ? $charge->id : '';
                $charge_object = $charge->object ? $charge->object : '';
                $charge_amount = $charge->amount ? $charge->amount : '';
                $balance_transaction_id = $charge->balance_transaction ? $charge->balance_transaction : '';
                $balance_created = $charge->created ? $charge->created : '';
                $balance_currency = $charge->currency ? $charge->currency : '';
                $balance_description = $charge->description ? $charge->description : '';
                $payment_method = $charge->payment_method ? $charge->payment_method : '';
                $get_subscription_data = Subscription::whereid($value->subscription_id)->first();
                //User Subscription Data
                $insert['user_id'] = $value->user_id;
                $insert['subscription_id'] = $value->subscription_id;
                $insert['customer_id'] = $customer_id; //customer id
                $insert['transaction_id'] = $balance_transaction_id;
                $insert['purchased_at'] = date('Y-m-d H:i:s', $balance_created);
                $insert['plan_name'] = $get_subscription_data->plan_name;
                $insert['visit_pass'] = $get_subscription_data->visit_pass;
                $insert['plan_duration'] = $get_subscription_data->plan_duration;
                $insert['plan_time'] = $get_subscription_data->plan_time;
                $insert['amount'] = $get_subscription_data->amount;
                $insert['description'] = $get_subscription_data->description ? $get_subscription_data->description :'';
                $insert['payment_method'] = $payment_method;
                $insert['status'] = ($charge->status == 'succeeded') ? 1 : 0;
                $insert['currency'] = $balance_currency;
                $insert['charge_id'] = $charge_id;
                $insert['integrated_gst'] = $value->integrated_gst;
                $insert['total_payable_amount'] = $value->amount;//$value->total_payable_amount;
                // add 23-12-2019
                $insert['failure_code'] = $charge->failure_code ? $charge->failure_code : '';
                $insert['failure_message'] = $charge->failure_message ? $charge->failure_message : '';
                //end 23-12-2019
                $plan_duration = $insert['plan_duration'];
                $plan_time = $insert['plan_time'];
                $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
                // User Card Data
                $card['card_brand'] = $charge->payment_method_details->card->brand;
                $card['card_country'] = $charge->payment_method_details->card->country;
                $card['card_number'] = $charge->payment_method_details->card->last4;
                $card['card_exp_month'] = $charge->payment_method_details->card->exp_month;
                $card['card_exp_year'] = $charge->payment_method_details->card->exp_year;
                $card['user_card_brand'] = $charge->payment_method_details->card->brand;
                $card['user_card_number'] = $card_number; //$charge->payment_method_details->card->last4; //here full card
                $card['user_card_exp_month'] = $charge->payment_method_details->card->exp_month;
                $card['user_card_exp_year'] = $charge->payment_method_details->card->exp_year;
                $card['status'] = 1;
                $card['user_id'] = $value->user_id;
                $status['status'] = '0';
                $update = Usersubscription::where('user_id', $value->user_id)->update($status);
                $add_payment = Usersubscription::create($insert);
                if (isset($add_payment->id) && $add_payment->id > 0) {
                    $card['usersubscription_id'] = $add_payment->id;
                    $add_card = Usercard::create($card);
                    //28-11-2019 for coupon code
//                    if (!empty($input['coupon_id']) && $input['coupon_code']) {
//                        $coupon['usersubscription_id'] = $add_payment->id;
//                        $coupon['coupon_id'] = $input['coupon_id'];
//                        $coupon['coupon_code'] = $input['coupon_code'];
//                        $coupon['counpon_discount'] = $input['counpon_discount'];
//                        $coupon['coupon_discount_price'] = $input['coupon_discount_amount'];
//                        $coupon['user_id'] = $value->user_id;
//                        Userusecoupon::create($coupon);
//                    }
                } else {
                    $failed['failed_message'] = "Something went wrong payment saved failed";
                    $failed['user_id'] = $value->user_id;
                    Autorenewalfaild::create($failed);

                }
            }
        }
    }

}

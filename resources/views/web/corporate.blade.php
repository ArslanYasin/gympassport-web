<!DOCTYPE html>
<html>
<head>
    <title>Corporate-Partner</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=devic e-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('landing_page/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('landing_page/css/media.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('become_patner/css-become/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('become_patner/css-become/style-become-partner.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

</head>
<body>
<style type="text/css">
    .text {
        color: red;
        border-color: red;
    }
</style>
<style type="text/css">
    #loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /* Add animation to "page content" */
    .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
    }

    @-webkit-keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }
        to {
            bottom: 0px;
            opacity: 1
        }
    }

    @keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }
        to {
            bottom: 0;
            opacity: 1
        }
    }

    #myDiv {
        display: none;
        text-align: center;
    }

    .successmsg-from-popup-wraper {
        background: #efe0e3;
        margin: 0px 0px 5px;
        padding: -18px 16px 9px 18px;
        text-align: center;
        max-width: 102%;
        padding: 17px -5px;
        margin-top: -39px;
        border-radius: 9px;
    }

    .successmsg-popup {
        font-size: 20px;
        color: green;
        font-weight: 700;
        font-family: Releway;
        text-transform: capitalize;
        padding: 59px 12px;
        line-height: 26px;
        text-align: center;
    }

    .close-button {
        float: right;
        font-size: 2.5rem;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .5;
        /* top: 0; */
        /* left: 0px; */
        margin-top: -58px;
    }

    .bold {

        font-weight: 600;
        font-size: 22px;
    }

    .numm_neww {
        margin: 0px 10px;
    }
</style>
<!--  <header class="header-become-partner">
      <div class="container">
        <div class="row">
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
          <div class="icon">
            <div class="icon-become-partner">
               <img src="{{ asset('become_patner/images/logo.png')}}" alt="icon">
              </div>
          </div>
                </div>
                <div class="col-6 col-sm-6 col-md-9 col-lg-9">
                    <div class="home-btn-box">
                        <button type="button" class="btn btn-primary home-btn home">Home</button>
                    </div>
                    <div class="home-btn-box">
                        <button type="button" class="btn btn-primary home-btn login-btn">Login</button>
                    </div>
                </div> -->

<header class="header">
    <div class="container-fluid">
        <div class="icon-hm-pg">
            <!-- <img src="images/logo.png" alt="icon" class="logo-image img-fluid"> -->
            <a href="{{url('/')}}">
                <img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="">
            </a>
        </div>
        <div class="custom-navbar-hm-pg">
            <ul>
                @if(isset(Auth::user()->id))
                    @if(Auth::user()->user_type=='3')
                        <li><a href="{{route('userhome')}}">Home</a></li>
                        <li><a href="{{route('findgym')}}">Find Gyms</a></li>
                    <!--                                     <li><a href="{{route('subscriptionplan')}}">Membership Plan</a></li>-->
                    @elseif(Auth::user()->user_type=='2')
                        <li><a href="{{route('ufppatnerhome')}}">Home</a></li>
                    @else
                        <li><a href="{{route('home')}}">Home</a></li>
                    @endif
                @else
                    <li class="@if(request()->route()->getName()=='findgym') active @endif">
                    {{--                        <a href="{{route('findgym')}}">Find Gyms</a></li>--}}
                    <!--                                <li class="@if(request()->route()->getName()=='subscriptionplan') active @endif">
                                  <a href="{{route('subscriptionplan')}}">Pricing</a></li>-->
                    <li class="@if(request()->route()->getName()=='becomepatner') active @endif">
                        <a href="{{route('becomepatner')}}">Become a Gym Partner</a></li>
                    <li class="@if(request()->route()->getName()=='corporate-partner') active @endif">
                        <a href="{{route('corporate-partner')}}">Corporate Partners</a></li>

                    <li><a href="{{route('singin')}}">Login</a></li>
                @endif
            </ul>
        </div>
        <div class="trigger-navbar-home-page">
            <span></span>
        </div>
    </div>
    <style type="text/css">
        li.active a {
            color: red;
        }
    </style>
    <script type="text/javascript">
        $('.home').click(function () {
            location.href = "{{url('/')}}";
        });
        $('.login-btn').click(function () {
            location.href = "{{route('singin')}}";
        });
    </script>

</header>

<style>
    .corporate-banner-section {
        background-image: url("{{ asset('become_patner/images/corporate-banner.jpg')}}");
        height: 100vh;
        background-size: cover;
        background-repeat: no-repeat;
    }

    .corporate-banner-section.banner-overlay {
        color: #0a0a0a;
    }

    .banner-heading-text span, .section-heading span {
        color: #be0027;
    }

    .corporate-banner-section .btn-danger.register-now-btn, .pricing-section .btn-danger.register-now-btn {
        padding: 5px 76px;
        font-weight: 400;
        border: 2px solid #fefefe;
        border-radius: 18px 0px 18px 19px;
        color: #ffffff;
        background-color: #be0027;
        margin: 0 auto;
        text-align: center;
        display: flex;
        margin-top: 30px;
        margin-bottom: 90px;
        font-family: 'Accidental Presidency';
        font-size: 26px;
        box-shadow: -2px 0 4px 0 #97092e;
    }

    .pricing-section .btn-danger.register-now-btn {
        margin-top: 0;
    }

    .section-heading {
        padding: 5rem 0;
    }

    .corporate-properties .mind h1 {
        font-family: Raleway;
        font-size: 36px;
        font-weight: 700;
        color: #be0027;
        padding-bottom: 1rem;
    }

    .corporate-properties p {
        font-weight: bold;
        /*margin: 40px 0 15px 10px;*/
        line-height: 1.5;
        font-size: 20px;
    }

    /* Slider */

    .slick-slide {
        margin: 0px 20px;
    }

    .slick-slide img {
        width: 100%;
    }

    .slick-slider {
        position: relative;
        display: block;
        box-sizing: border-box;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-touch-callout: none;
        -khtml-user-select: none;
        -ms-touch-action: pan-y;
        touch-action: pan-y;
        -webkit-tap-highlight-color: transparent;
    }

    .slick-list {
        position: relative;
        display: block;
        overflow: hidden;
        margin: 0;
        padding: 0;
    }

    .slick-list:focus {
        outline: none;
    }

    .slick-list.dragging {
        cursor: pointer;
        cursor: hand;
    }

    .slick-slider .slick-track,
    .slick-slider .slick-list {
        -webkit-transform: translate3d(0, 0, 0);
        -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
        -o-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }

    .slick-track {
        position: relative;
        top: 0;
        left: 0;
        display: block;
    }

    .slick-track:before,
    .slick-track:after {
        display: table;
        content: '';
    }

    .slick-track:after {
        clear: both;
    }

    .slick-loading .slick-track {
        visibility: hidden;
    }

    .slick-slide {
        display: none;
        float: left;
        height: 100%;
        min-height: 1px;
    }

    [dir='rtl'] .slick-slide {
        float: right;
    }

    .slick-slide img {
        display: block;
    }

    .slick-slide.slick-loading img {
        display: none;
    }

    .slick-slide.dragging img {
        pointer-events: none;
    }

    .slick-initialized .slick-slide {
        display: block;
    }

    .slick-loading .slick-slide {
        visibility: hidden;
    }

    .slick-vertical .slick-slide {
        display: block;
        height: auto;
        border: 1px solid transparent;
    }

    .slick-arrow.slick-hidden {
        display: none;
    }

    .testimonial {
        margin: 0 20px 40px;
    }

    .testimonial .testimonial-content {
        padding: 35px 25px 35px 50px;
        margin-bottom: 35px;
        background: #fff;
        position: relative;
    }

    .testimonial .testimonial-content:before {
        content: "";
        position: absolute;
        bottom: -30px;
        left: 0;
        border-top: 15px solid #718076;
        border-left: 15px solid transparent;
        border-bottom: 15px solid transparent;
    }

    .testimonial .testimonial-content:after {
        content: "";
        position: absolute;
        bottom: -30px;
        right: 0;
        border-top: 15px solid #718076;
        border-right: 15px solid transparent;
        border-bottom: 15px solid transparent;
    }

    .testimonial-content .testimonial-icon {
        width: 50px;
        height: 45px;
        background: #be0027;
        text-align: center;
        font-size: 22px;
        color: #fff;
        line-height: 42px;
        position: absolute;
        top: 37px;
        left: -19px;
    }

    .testimonial-content .testimonial-icon:before {
        content: "";
        border-bottom: 16px solid #be0027;
        border-left: 18px solid transparent;
        position: absolute;
        top: -16px;
        left: 1px;
    }

    .testimonial .description {
        font-size: 15px;
        font-style: italic;
        color: #8a8a8a;
        line-height: 23px;
        margin: 0;
    }

    .testimonial .title {
        display: block;
        font-size: 18px;
        font-weight: 700;
        color: white;
        text-transform: capitalize;
        letter-spacing: 1px;
        margin: 0 0 15px 0;
    }

    .testimonial .post {
        display: block;
        font-size: 14px;
        color: #fff;
    }

    .owl-theme .owl-controls {
        margin-top: 20px;
    }

    .owl-theme .owl-controls .owl-page span {
        background: #ccc;
        opacity: 1;
        transition: all 0.4s ease 0s;
    }

    .owl-theme .owl-controls .owl-page.active span,
    .owl-theme .owl-controls.clickable .owl-page:hover span {
        background: #be0027;
    }

    .reviews-section .section-heading h1 {
        color: #fff;
    }

    .interest-box {
        height: 100%;
        background-size: cover;
    }

    @media (min-width: 2000px) {
        .banner-heading-text h1 {
            font-size: 110px;
        }

        .interest-box {
            height: 100vh;
        }
    }

    @media (min-width: 577px) and (max-width: 768px) {
        .custom-navbar-hm-pg {
            margin-right: 50px;
        }

        .banner-heading-text h1 {
            margin-top: 18rem;
        }

        .light-clr-heading-join-ufp {
            margin-top: 40px;
        }

        /*.interest-box{*/
        /*    background-size: cover;*/
        /*}*/
    }

    @media (max-width: 576px) {
        .trigger-navbar-home-page {
            display: block;
            margin: 25px 0 0 300px;
        }

        .icon-hm-pg {
            padding: 0;
        }

        .banner-heading-text h1 {
            font-size: 44px;
            margin-top: 12rem;
        }

        .section-heading {
            padding: 3rem 0;
        }

        .interest-heading {
            margin-top: 0;
            font-size: 55px;
        }

        .light-clr-heading-join-ufp {
            font-size: 100px;
            margin-top: 30px;
        }

        .corporate-properties .section-heading h1 {
            font-size: 30px;
        }

        .section-heading h1 {
            font-size: 30px;
        }

        .clients-section {
            padding-bottom: 2rem;
        }
    }


</style>
<section class="corporate-banner-section bg-danger banner-overlay">
    <div class="container align-items-center">
        <div class="row justify-content-center text-center">
            <div class="col-lg-9">
                <div class="banner-heading-text mt-xl-5">
                    <h1>Introduce <span>health and wellbeing</span> into your company</h1>
                </div>
                <button type="button" class="btn btn-danger register-now-btn">Get In Touch</button>
            </div>
        </div>
    </div>
</section>

<section class="corporate-properties pb-lg-3">
    <div class="container">
        <div class="section-heading">
            <div class="row justify-content-center text-center">
                <div class="col-lg-7">
                    <h1>How can <span>Gympassport</span> help
                        your<span> company</span>?</h1>
                </div>
            </div>
        </div>

        <div class="row mt-5 justify-content-md-center">
            <div class="col-lg-4 col-md-6 text-center justify-content-center p-4">
                <img width="150px" height="150px" src="{{ asset('become_patner/images/1-Gyms.jpg')}}" alt="">
                <p><strong>Unlimited Access to over {{$total_gyms}}+ Gyms and Fitness centers across Pakistan. </strong></p>
            </div>
            <div class="col-lg-4 col-md-6 text-center justify-content-center p-4">
                <img width="180px" height="150px" src="{{ asset('become_patner/images/2-virtual-healh.jpg')}}" alt="">
                <p><strong>Track your employees fitness and wellness journey.</strong></p>
            </div>
            <div class="col-lg-4 col-md-6 text-center justify-content-center p-4">
                <img width="150px" height="150px" src="{{ asset('become_patner/images/3-mental-health.jpg')}}" alt="">
                <p><strong>Have peace off mind. Only pay when employees use our service.</strong></p>
            </div>
        </div>

        {{--        <div class="row justify-content-between mind mb-5">--}}
        {{--            <div class="col-lg-3">--}}
        {{--                <h1>Mind</h1>--}}
        {{--                <p>1:1 therapy sessions and partners to help calm the mind, track goals, and manage stress, anxiety,--}}
        {{--                    depression, based on the latest scientific research.</p>--}}
        {{--            </div>--}}
        {{--            <div class="col-lg-8">--}}
        {{--                <div class="row">--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/calm.png')}}" alt="" style="width: 100px">--}}
        {{--                        <p><span>Calm</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/ifeel.svg')}}" alt="" style="width: 150px">--}}
        {{--                        <p><span>iFeel</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/mindshine.png')}}" alt="">--}}
        {{--                        <p><span>Mindshine</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/rooted.png')}}" alt="">--}}
        {{--                        <p><span>Rooted</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <hr style="padding-bottom: 3rem">--}}
        {{--        <div class="row justify-content-between mind">--}}
        {{--            <div class="col-lg-3">--}}
        {{--                <h1>Body</h1>--}}
        {{--                <p>1:1 therapy sessions and partners to help calm the mind, track goals, and manage stress, anxiety,--}}
        {{--                    depression, based on the latest scientific research.</p>--}}
        {{--            </div>--}}
        {{--            <div class="col-lg-8">--}}
        {{--                <div class="row">--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/calm.png')}}" alt="" style="width: 100px">--}}
        {{--                        <p><span>Calm</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/ifeel.svg')}}" alt="" style="width: 150px">--}}
        {{--                        <p><span>iFeel</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/mindshine.png')}}" alt="">--}}
        {{--                        <p><span>Mindshine</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/rooted.png')}}" alt="">--}}
        {{--                        <p><span>Rooted</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <div class="properties-list">--}}

        {{--        </div>--}}
    </div>
</section>

<section class="corporate-properties pb-lg-5">
    <div class="container">
        <div class="section-heading">
            <div class="row justify-content-center text-center">
                <div class="col-lg-7">
                    <h1><span>Benefits</span> to your <span> company</span>?</h1>
                </div>
            </div>
        </div>

        <div class="row mt-1 justify-content-md-center">
            <div class="col-lg-4 col-md-6 text-center justify-content-center">
                <img width="230px" height="230px" src="{{ asset('become_patner/images/Wellness.jpg')}}" alt="">

                <p><strong>Show employees you care about their health and fitness by offering employees wellness benefits.</strong></p>
            </div>
            <div class="col-lg-4 col-md-6 text-center justify-content-center">
                <img width="230px" height="230px" src="{{ asset('become_patner/images/6-Mental-health-image.jpg')}}" alt="">
                <p><strong>Increase employee mental health and productivity of work by up to 30%.
                    </strong></p>
            </div>
            <div class="col-lg-4 col-md-6 text-center justify-content-center">
                <img width="230px" height="230px" src="{{ asset('become_patner/images/happy employees.jpg')}}" alt="">
                <p><strong>Reduce your staff turnover by up to 20% by offering unique benefits.</strong></p>
            </div>
        </div>

        {{--        <div class="row justify-content-between mind mb-5">--}}
        {{--            <div class="col-lg-3">--}}
        {{--                <h1>Mind</h1>--}}
        {{--                <p>1:1 therapy sessions and partners to help calm the mind, track goals, and manage stress, anxiety,--}}
        {{--                    depression, based on the latest scientific research.</p>--}}
        {{--            </div>--}}
        {{--            <div class="col-lg-8">--}}
        {{--                <div class="row">--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/calm.png')}}" alt="" style="width: 100px">--}}
        {{--                        <p><span>Calm</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/ifeel.svg')}}" alt="" style="width: 150px">--}}
        {{--                        <p><span>iFeel</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/mindshine.png')}}" alt="">--}}
        {{--                        <p><span>Mindshine</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/rooted.png')}}" alt="">--}}
        {{--                        <p><span>Rooted</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <hr style="padding-bottom: 3rem">--}}
        {{--        <div class="row justify-content-between mind">--}}
        {{--            <div class="col-lg-3">--}}
        {{--                <h1>Body</h1>--}}
        {{--                <p>1:1 therapy sessions and partners to help calm the mind, track goals, and manage stress, anxiety,--}}
        {{--                    depression, based on the latest scientific research.</p>--}}
        {{--            </div>--}}
        {{--            <div class="col-lg-8">--}}
        {{--                <div class="row">--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/calm.png')}}" alt="" style="width: 100px">--}}
        {{--                        <p><span>Calm</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/ifeel.svg')}}" alt="" style="width: 150px">--}}
        {{--                        <p><span>iFeel</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/mindshine.png')}}" alt="">--}}
        {{--                        <p><span>Mindshine</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                    <div class="col-lg-3">--}}
        {{--                        <img src="{{ asset('become_patner/images/rooted.png')}}" alt="">--}}
        {{--                        <p><span>Rooted</span></p>--}}
        {{--                        <p>Guided meditations and mental fitness for a more calm and mindful life</p>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <div class="properties-list">--}}

        {{--        </div>--}}
    </div>
</section>

<section class="clients-section bg-light mt-5">
    <div class="container">
        <div class="section-heading pb-md-5 pb-sm-5">
            <div class="row justify-content-center text-center">
                <div class="col-lg-10">
                    <h1><span>WE HAVE PARTNERED WITH SOME OF THE BEST FITNESS CLUBS IN PAKISTAN</span></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12">
                <section class="customer-logos slider" style="margin: 5%">
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo1.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo2.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo3.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo4.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo5.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo6.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo7.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo8.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo9.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo10.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo11.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo12.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo13.jpg"></div>
                </section>
            </div>
        </div>
    </div>
</section>


<section class="reviews-section bg-dark">
    <div class="container">
        <div class="section-heading">
            <div class="row justify-content-center text-center">
                <div class="col-lg-10">
                    <h1>Our<span> Clients</span></h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="testimonial-slider" class="owl-carousel">
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/daraz.png')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/foodpanda.png')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/jugnoo.png')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/aksa.png')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/jugnu.jpg')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/conard.jpg')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/kickstart.jpg')}}">
                    </div>
                    <div class="testimonial text-center">
                        <img class="bg-white rounded" width="200" height="200" src="{{asset('become_patner/images/popcorn-studio.jpg')}}">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="pricing-section">
    <div class="container">
        <div class="section-heading pb-5">
            <div class="row justify-content-center text-center">
                <div class="col-lg-10 col-md-10">
                    <h1><span>Pricing</span></h1>
                    <p class="color-grey pt-4" style="line-height: 1.4">We are a pay per use service, which means you only pay for employees who use Gym Passport. We will negotiate an affordable plan in
                        providing health and wellbeing
                        services to your company. Register below to learn more</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <button type="button" class="btn btn-danger register-now-btn">Contact Us</button>
            </div>
        </div>

    </div>
</section>
<section class="interest-box mb-5" id="bottom">
    <div class="container" id="interest-box-container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <h3 class="light-clr-heading-join-ufp">Join GYM Passport</h3>
                <h5 class="interest-heading">Register As Corporate Partner!</h5>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" style="padding: 0.5rem 1.25rem" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                @endif
                <div class="successmsg-from-popup-wraper" id="myPopup" style="display: none;">
                    <div class="successmsg-popup">
                        <div class="close-btnbx">
                            <button type="button" class="close close-button close-popup" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        {{--                        @if(Session::has('message'))--}}
                        <script>
                            // document.getElementById('bottom').scrollIntoView();
                            // $("#myPopup").show();
                            //setTimeout(function() { $("#myPopup").hide(); }, 5000);
                        </script>
                        {{--                            {!! session('msg') !!}--}}
                        {{--                        @endif--}}
                    </div>
                </div>

                {{--                <div id="loader"></div>--}}
                <form action="{{ route('add_corporate_partner') }}" method="post" id="submit_formss">
                    {{ csrf_field() }}
                    <div class="form-group user-form">
                        <label class="lbl">Name</label>
                        <input type="text" name="first_name" class="form-control value gry-clr" placeholder=""
                               value="{{ old('first_name') }}">
                        @if ($errors->has('first_name'))<strong class="text">{{ $errors->first('first_name') }}</strong>
                        <br>@endif
                        <label class="lbl">Company</label>
                        <input type="text" name="corporate_name" class="form-control value gry-clr" placeholder=""
                               value="{{ old('corporate_name') }}">
                        @if ($errors->has('corporate_name'))<strong class="text">{{ $errors->first('corporate_name') }}</strong>
                        <br>@endif
                        <label class="lbl">Number of Employees</label>
                        <input type="number" name="no_of_employees" class="form-control value grey-clr" placeholder=""
                               value="{{ old('no_of_employees') }}">
                        @if ($errors->has('no_of_employees'))<strong
                                class="text">{{ $errors->first('no_of_employees') }}</strong><br>@endif
                        <label class="lbl">Email</label>
                        <input type="email" name="email" class="form-control value grey-clr" placeholder=""
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))<strong class="text">{{ $errors->first('email') }}</strong><br>@endif

                        {{--                        <label class="lbl">Password</label>--}}
                        {{--                        <input type="password" name="password" class="form-control value grey-clr" placeholder=""--}}
                        {{--                               value="{{ old('password') }}">--}}
                        {{--                        @if ($errors->has('password'))<strong class="text">{{ $errors->first('password') }}</strong><br>@endif--}}

                        {{--                        <label for="password-confirm" class="lbl">Confirm Password</label>--}}
                        {{--                        <input type="password" name="password_confirmation" id="password-confirm" class="form-control value grey-clr" placeholder="" value="{{ old('password_confirmation') }}" required/>--}}
                        {{--                        @if ($errors->has('password_confirmation'))<strong class="text">{{ $errors->first('password_confirmation') }}</strong><br>@endif--}}

                        <label class="lbl">Phone</label>
                        <input type="number" name="phone_number" class="form-control value grey-clr" placeholder=""
                               value="{{ old('phone_number') }}">
                        @if ($errors->has('phone_number'))<strong
                                class="text">{{ $errors->first('phone_number') }}</strong><br>@endif

                        <div><label class="lbl grey-clr">Address</label></div>
                        <input type="text" name="address" class="form-control value" placeholder=""
                               value="{{ old('address') }}">
                        @if ($errors->has('address'))<strong class="text">{{ $errors->first('address') }}</strong>
                        <br>@endif
                    <!--                <label class="lbl">Postcode</label>
                <input type="text" name="postal_code" class="form-control value" placeholder="" value="{{ old('postal_code') }}">
                @if ($errors->has('postal_code'))<strong class="text">{{ $errors->first('postal_code') }}</strong><br>@endif-->
                    </div>
                    <div class="register-box-btn w-100">
                        <button type="submit" class="submit btn btn-danger register-btn text-center">Register Now
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<section class="access-fitness-section">
    <!-- <div class="container-fluid"> -->
    <div class="row">
        <div class="col-12 col-sm-4">
            <!-- <div class="hm-pg-last-img"> -->
            <div class="access-fitness-section-image">
                <img src="{{ asset('landing_page/images/ufp-bottom.png')}}" alt="image" class="img-fluid">
            </div>
            <!--  </div> -->
        </div>
        <div class="col-12 col-sm-8">
            <div class="access-fitness-wrapper">
                <h4 class="access-fitness-heading">Access fitness</h4>
                <p class="fitness-section-red">WITH OUR MOBILE APPLICATION</p>
                <p class="last-grey">Download the Gym Passport app and</p>
                <p class="last-grey">get started to your ultimate access to fitness!</p>
                <div class="store-signup-hm-pg" id="store-signup-hm-pg-id">
                    <a href="https://apps.apple.com/au/app/gym-passport/id1534995333" target="_blank"><img
                                src="{{ asset('landing_page/images/app-store.png')}}" alt="newpass-page-image-hm-pg"
                                class="img-fluid apple-store-image"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport"
                       target="_blank"><img src="{{ asset('landing_page/images/google-store.png')}}"
                                            alt="newpass-page-image-hm-pg" class="img-fluid google-store-image"></a>
                </div>
            </div>
            {{--                    <div class="coming_soon">--}}
            {{--                        <p class="text_soon">Coming Soon...</p>--}}
            {{--                    </div>--}}
        </div>
    </div>
    <!-- </div> -->
</section>


<!-- Footer started here -->
<footer class="footer">
    <div class="container-fluid">
        <ul>
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{route('terms_conditions')}}">Terms & Conditions</a></li>
            <li><a href="{{route('privacy_policy')}}">Privacy Policy</a></li>
            <li><a href="{{route('refund_policy')}}">Refund Policy</a></li>
            <li><a href="{{route('aboutus')}}">About us</a></li>
            <li><a href="{{route('contactus')}}">Contact us</a></li>
            <li><a href="{{route('faq')}}">FAQ</a></li>
            <li><a href="{{route('becomepatner')}}">Partnerships</a></li>
        <!--                    <li><a href="{{route('subscriptionplan')}}" >Membership</a></li>-->
            {{--            <li><a href="{{route('findgym')}}">Gyms Near Me</a></li>--}}
        </ul>
    </div>
</footer>
<!-- Footer Ended Here -->
</body>
</html>

<script>
    $(document).ready(function () {
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#testimonial-slider").owlCarousel({
            loop: true,
            items: 4,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [980, 2],
            itemsTablet: [768, 2],
            itemsMobile: [650, 1],
            pagination: true,
            navigation: false,
            slideSpeed: 1000,
            autoPlay: true,

        });
    });
</script>

<script>

    $(document).ready(function () {
        $(".trigger-navbar-home-page").click(function () {
            $(".custom-navbar-hm-pg").slideToggle();
        });


        $('.register-now-btn').click(function (event) {
            event.preventDefault();
            $.scrollTo($('.user-form'), 1000);
        });
    });

    $(".register-now-btn").click(function () {
        $('html, body').animate({
            scrollTop: $(".user-form").offset().top - 200
        }, 1000);
    });
    @if($errors->all())
    var url = window.location.href + '#interest-box-container'
    $(location).attr('href', url);
    @endif
    @if(Session::has('message'))
    var url = window.location.href + '#interest-box-container'
    $(location).attr('href', url);
    @endif

    $('.close-popup').click(function () {
        $('#myPopup').hide();
    });
</script>


<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=devic e-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="head-signup">
    <div class="container-fluid">
      <div class="icon-signup">
        <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="img-fluid signup-icon">
      </div>
      </div>
  </header>
  <section class="login-left-part">
    <img src="{{ asset('website_file/images/login-pg.png')}}" alt="newpass-page-image" class="img-fluid login-left-img">
    <div class="login-stores-icon">
      <img src="{{ asset('website_file/images/app-store.png')}}" alt="newpass-page-image" class="img-fluid apple-store-image">
      <img src="{{ asset('website_file/images/google-store.png')}}" alt="newpass-page-image" class="img-fluid google-store-image">
      </div>
  </section>
  <section class="login-right-part">
     @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
      @endif 
    <h5 class="newpass-heading">Login</h5>
    <form action="{{ route('signin.submit')}}" method="post">
         {{ csrf_field() }}
    <label class="newpass-labels">Email Address</label>
        <input type="email" name="email" placeholder="ultimatefitness@gmail.com" class="login-form" autocomplete="off">
         @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif

            <label class="newpass-labels">Password</label>
      <div class="pass-box-wrapper">
        <input type="password" name="password" class="login-form" placeholder="*********" id="pwd" autocomplete="off">
          <p class="show-hide-para-login" id="pwdbtn">Show</p>
           @if ($errors->has('password'))<label class="error" for="">{{ $errors->first('password') }}</label>@endif
            </div>
            <div class="forgot-password-btn-box">
               <a href="{{route('forgate_password')}}"><button type="button" class="forgot-password-btnn">Forgot Password?</button></a>
            </div>
            <div class="login-pg-btn-box">
              <a href="{{route('singup')}}"><button type="button" class="btn btn-danger login-create-acc-btn">Create Account</button></a>
                 <button type="submit" class="btn btn-danger a-login-btn">Login</button>
            </div>
    </form>
            <div class="store">
                <img src="{{ asset('website_file/images/app-store.png')}}" alt="newpass-page-image" class="img-fluid apple-store-image">
              <img src="{{ asset('website_file/images/google-store.png')}}" alt="newpass-page-image" class="img-fluid google-store-image">
            </div>
  </section>
  <script>
    $(document).ready(function(){
    $('#pwdbtn').click(function(){
      if($('#pwd').attr('type') == 'password'){
        $('#pwd').attr("type","text");
        $(this).text("hide");
      }else {
        $('#pwd').attr("type","password");
        $(this).text("show");
      }
    })
  });
</script>

</body>
</html>
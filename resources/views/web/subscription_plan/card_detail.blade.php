@extends('web_user_dash.design')
@section('content')
<style type="text/css">
  .booking-details-wrapper {
    background-color: #383838;
     height: 100vh; 
}
/*.error {
    color: red;
    text-align: right;
    padding: 1px 86px;
    margin-top: -20px;
    margin-bottom: 4px;
}*/
.alert-success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    margin: 130px 0px -52px 0px;
}
.alert-danger {
    margin: 130px 0px -52px 0px;
}

</style>
<section class="booking-details-wrapper">
   <div class="container-fluid">
    @if(Session::has('message'))
        @if(session('message')=='alert-danger')
          <script type="text/javascript">
            swal({
                  title : "{!! session('msg') !!}",
                  text  :  "",
                  icon  : 'error'
                });
          </script>
         @else
           <script type="text/javascript">
            swal({
                  title : "{!! session('msg') !!}",
                  text  :  "",
                  icon  : 'success'
                });
          </script>
         @endif
 <!--    <div class="col-sm-7">
      <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible" style="width: 87%;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          {!! session('msg') !!}
      </div>
    </div>
    <style type="text/css">.card-box1 { margin-top: 39px; }</style> -->
    @endif 
    <div id="loader" style="display: none;"></div> 
      <form action="{{route('make_payment')}}" method="post">
         {{ csrf_field() }}
        <input type="hidden" name="plan_id" value="{{$plan_id}}">
         <input type="hidden" name="payable_amount" value="{{$payable_amount}}">
         <!-- <input type="hidden" name="gst" value="">
         <input type="hidden" name="payable_amount" value="">
         <input type="hidden" name="coupon_id" value="">
         <input type="hidden" name="coupon_code" value="">
         <input type="hidden" name="counpon_discount" value="">
         <input type="hidden" name="coupon_discount_amount" value="">
         <input type="hidden" name="total_amount" value="">  -->
        <div class="cardboxdiv">
        <!--  <div class="card-box1">

       <div class="input-label">
            <label class="three-label-card same-h4-card">Card Number</label>
             <input type="number" name="card_number" id="card_number" value="{{$card_number}}" class="three-values-card placeholder-clr-card" placeholder="" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "16" value="{{ old('card_number') }}">
             @if ($errors->has('card_number'))<p class="error" for="">{{ $errors->first('card_number') }}</p>@endif

               </div>
               <div class="input-label">
            <label class="three-label-card same-h4-card">Expiry Month</label>
            <input type="number" name="exp_month" id="exp_month" class="three-values-card placeholder-clr-card" value="{{$exp_month}}" placeholder="" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "2"  value="{{ old('exp_month') }}">
             @if ($errors->has('exp_month'))<p class="error" for="">{{ $errors->first('exp_month') }}</p>@endif
             </div>
               <div class="input-label">
             <label class="three-label-card same-h4-card">Expiry Year</label>
            <input type="number" name="exp_year" id="exp_year" class="three-values-card placeholder-clr-card" placeholder="" value="{{$exp_year}}" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "4" value="{{ old('exp_year') }}">
             @if ($errors->has('exp_year'))<p class="error" for="">{{ $errors->first('exp_year') }}</p>@endif
             </div>
               <div class="input-label">
            <label class="three-label-card same-h4-card">Enter Security</label>
           <input type="number" name="cvv" id="cvv" class="three-values-card placeholder-clr-card" placeholder="" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "3" value="{{ old('cvv') }}">
             @if ($errors->has('cvv'))<p class="error" for="">{{ $errors->first('cvv') }}</p>@endif
         </div>
         </div> -->
           <div class="card-box1">
                        <div class="checkin-wrapper-cardd">
                           
                          
                            
          
              <label class="new-cardd">CARD NUMBER</label>
             <!--  <input type="number" name="card_number" id="card_number" value="{{$card_number}}" class="form-control fourth-tab-input-box-cardd" placeholder="" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "16" value="{{ old('card_number') }}" required> -->
             <div class="beautiful-field field-group credit-cart">
       <!--  <label class="label" for="credit-card">Credit card</label> -->
        <input class="field" name="card_number" value="@if(empty(old('card_number'))){{$card_number}} @else {{old('card_number')}} @endif" id="credit-card" autocomplete="off" type="text" placeholder="1111 1111 1111 1111" maxlength="19" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);" />
      </div>
              @if ($errors->has('card_number'))<p class="error" for="">{{ $errors->first('card_number') }}</p>@endif
                <div class="row">
                  <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                      <label class="new-cardd">EXPIRATION DATE</label> 
                     <div class="row expiration-box">
                      <div class="col-2 col-sm-4">
                        <input type="number" name="exp_month" id="first"  value="@if(isset($exp_month)){{$exp_month}}@else {{old('exp_month')}} @endif" class="form-control fourth-tab-input-box-cardd-bor-none" placeholder="" maxlength="2" onkeyup="movetoNext(this, 'second')" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid month');this.value = this.value.slice(0, this.maxLength);" >
                      </div>
                      <div class="col-1 col-sm-1">
                        <label class="slash-card">/</label>
                      </div>
                      <div class="col-4 col-sm-4">
                         <input type="number" name="exp_year" id="second"  value="@if(isset($exp_year)){{substr( $exp_year, -2)}}@else {{old('exp_year')}} @endif" class="form-control fourth-tab-input-box-cardd-bor-none" placeholder="" id="second" maxlength="2" onkeyup="movetoNext(this, 'third')" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid year');this.value = this.value.slice(0, this.maxLength);" >
                       </div>
                     </div>
                     @if ($errors->has('exp_month'))<p class="error" for="">{{ $errors->first('exp_month') }}</p>@endif
                      @if ($errors->has('exp_year'))<p class="error" for="">{{ $errors->first('exp_year') }}</p>@endif
                  </div>
                  <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                      <label class="new-cardd">SECURITY CODE</label>
                     <input type="number" name="cvv" oninput="javascript: if (this.value.length > this.maxLength)alert('Only 3 digit accepted');this.value = this.value.slice(0, this.maxLength);"   maxlength = "3" value="{{ old('cvv') }}" class="form-control fourth-tab-input-box-cardd" placeholder="" id="third" maxlength="3" >
                      @if ($errors->has('cvv'))<p class="error" for="">{{ $errors->first('cvv') }}</p>@endif
                  </div>
              </div>
                            
                        </div>
                </div>
             <div class="card-box2" >
               <div class="cardimage">
               <figure>
                 <img src="https://ufitpass.com/public/website_file/images/powered_by_stripe@3x.png" alt="stripe-img">
              </figure>
              </div>
              </div>  
              </div> 
         <div class="make-payment-btn-box">
            <button type="submit" class="btn btn-danger make-payment-btn submit">
            Make Payment
            </button>
         </div>
      </form>
   </div>
</section>

@if($errors->has('card_number') || $errors->has('exp_month') || $errors->has('exp_year') || $errors->has('cvv'))
<script type="text/javascript">
    setTimeout(function(){ $('.error').hide(); }, 4000);
    $('.booking-details-wrapper').removeClass("fadace");
    document.getElementById('formpopup').classList.add('show');
    $("#loader").hide();
</script>
@endif
 @if(Session::has('message'))
    <script type="text/javascript">
        $('.booking-details-wrapper').removeClass("fadace");
          $("#loader").hide();
   //document.getElementById('formpopup').classList.add('show');
   </script>
 @endif
 <script>
document.getElementById("loader").style.display = "none";
$('.submit').click(function(){
    var myVar;
    $('.booking-details-wrapper').addClass("fadace");
    $("#loader").show();
    myVar = setTimeout(showPage, 9000);
});

function showPage() {
 // document.getElementById("loader").style.display = "none";
}
</script>

<script> 
 function movetoNext(current, nextFieldID) {  
    if (current.value.length >= current.maxLength) {  
    document.getElementById(nextFieldID).focus();  
    }  
    }  
</script>
<script>
  $(document).ready(function() {
      $('#credit-card').val(function (index, value) {
         return value.replace(/[^a-z0-9]+/gi, '').replace(/(.{4})/g, '$1 '); 
     });
  });

$('#credit-card').on('keypress change blur', function () {
  $(this).val(function (index, value) {
    return value.replace(/[^a-z0-9]+/gi, '').replace(/(.{4})/g, '$1 ');
  });
});

$('#credit-card').on('copy cut paste', function () {
  setTimeout(function () {
    $('#credit-card').trigger("change");
  });
});
</script>
@endsection
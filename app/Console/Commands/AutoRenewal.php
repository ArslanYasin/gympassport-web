<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\API\AutorenewalController;

class AutoRenewal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Daily:AutoRenewalPayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'AutoRenewal Payment For users which is using ulfimate fitness monthly plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->plan_renewal = new AutorenewalController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->plan_renewal->fetch_last_day_subscription_plan();
        $this->info('AutoRenewal Payment For users which is using ulfimate fitness monthly plan!');
    }
}

@extends('admin_dash.design')
@section('content')
    <script src="{{asset('js/jquery.js')}}"></script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if(empty($data_facilities))
                    Revenue Customers
                @else
                    Update Facilities
                @endif

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                @if(empty($data_facilities))
                    <li class="active"> Revenue History</li>
                @else
                    <li class="active"> Update Facilities</li>
                @endif




            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .img_url {
                height: 30px;
                width: 30px;
            }
            .panel-primary{
                border: 1px solid #fff;
            }
            .content_cls {


                border-radius: 4px;

                width: 100%;
                padding: 10px 10px;
                /* margin-left: -14px; */
            }
            .panel-body.custom_panel_body {
                position: relative;
                background-color: #222d32;
                border-radius: 0;

            }
            h3.user_info {
                font-size: 17px;
                color: #fff;
            }

            .panel-body.custom_panel_body {
                position: relative;
                background-color: #222d32;
            }
            .row.panel {
                background-color: #dedede;
            }
        </style>
        <!-- Main content -->
        <section class="content" style="min-height: 0px !important;">
                <div class=" panel panel-default">
                    <div class=" panel-body">
                        <input type="text" id="gym_name" value="{{$gymid}}" hidden>
                        <input type="text" id="gym_type" value="{{$type}}" hidden>
                        <div class="row" style="margin-top: 2%;" id="check-in-history">

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    </div>
    </div>

    <script>
        $(document).ready(function() {
            get_data();
        });

        function get_data() {
            let gym_id = $('#gym_name').val();
            let gym_type = $('#gym_type').val();

            $.ajax({
                url: "{{route('gymrevenue.details')}}",
                type: "post",
                data: {
                    _token: "{{csrf_token()}}",
                    gym_id: gym_id,
                    gym_type: gym_type
                },
                success: function(data) {
                    console.log(data);
                    $('#start_date').text(data.start_date + ' ' + 'to' + ' ' + data.end_date);
                    $('#total_amount').text(data.total_amount);
                    var details = '';
                    $.each(data.visit_details, function(key, value) {
                        details += <div class=" panel panel-primary" style="margin-right:10px; margin-left:10px">
                            <div class="panel-body custom_panel_body" data-toggle="collapse" href="#custom_content_id" aria-expanded="false" aria-controls="footwear">
                            <div class="collapse content_cls" id="custom_content_id">
                            <p>Collapsible content...</p>


                        </div>
                        <div class="col-md-3">
                            <img src="http://placehold.it/150x100" alt="..." class="margin">

                            <div class="col-md-3">
                            <h3>${value.user_detail[0].first_name +' '+ value.user_detail[0].last_name}</h3>
                            <h3>Id: ${value.user_detail[0].user_ufp_id}</h3>
                        </div>
                        <div class="col-md-3">
                            <h3>check in counter</h3>
                            <h3>Status: ${value.user_detail[0].status==1?'Active': 'Expired'}</h3>
                        </div>
                        <div class="col-md-3">
                            <h3>check-In time: ${value.check_in_time}</h3>
                        <h3>check-In date: ${value.check_in_date}</h3>
                        </div>

                        </div>

                        </div>
                    });

                    $('#visit_detail').append(details);


                }
            });

        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var gym_id = $('#gym_name').val();
            var gym_type = $('#gym_type').val();
            var user_id = '';
            if (gym_id != '' && gym_type != '') {
                getuser(gym_id, gym_type);
                gym_earn_amount(gym_id, gym_type);
                select_gym_wise_user_list(gym_id);
            }

            $('#gym_type').change(function(event) {
                event.preventDefault();
                var gym_id = $('#gym_name').val();
                var gym_type = $('#gym_type').val();
                if (gym_id != '' && gym_type != 'advanced') {
                    //getuser(gym_id, gym_type);
                    //gym_earn_amount(gym_id, gym_type);
                }
                if (gym_id != '' && gym_type == 'advanced') {
                    $('.modal').modal('show');
                    //getuser(gym_id,gym_type);
                    //gym_earn_amount(gym_id,gym_type);
                }
            });

            $('#gym_name').change(function(event) {
                event.preventDefault();
                var gym_id = $('#gym_name').val();
                var gym_type = $('#gym_type').val();
                if (gym_id != '' && gym_type != '') {
                    getuser(gym_id, gym_type);
                    gym_earn_amount(gym_id, gym_type);
                }
            });
            $('#user_name').change(function(event) {
                event.preventDefault();
                var user_id = $('#user_name').val();
                var gym_id = $('#gym_name').val();
                var gym_type = $('#gym_type').val();
                if (gym_id != '' && gym_type != '' && user_id != '') {
                    getuser(gym_id, gym_type);
                    gym_earn_amount(gym_id, gym_type);
                }
            });

            function getuser(gym_id, gym_type) {
                $.ajax({
                    url: "{{route('user_check_in_histroy_admin')}}",
                    data: {
                        'gym_id': gym_id,
                        'gym_type': gym_type,
                        '_token': "{{ csrf_token() }}"
                    },
                    type: "post",
                    success: function(data) {
                        $("#check-in-history").html(data);
                    }
                });
            }


            function gym_earn_amount(gym_id, gym_type) {
                $.ajax({
                    url: "{{route('gym_earn_amount_admin')}}",
                    data: {
                        'gym_id': gym_id,
                        'gym_type': gym_type,
                        '_token': "{{ csrf_token() }}"
                    },
                    type: "post",
                    success: function(data) {
                        //console.log(data);
                        if (data.status) {
                            $('#fetch_amount').html(data.amount);
                            $('#month_amount').html(data.monthly_amount);
                            $('#daily_amount').html(data.daily_amount);
                            $('#bydate').html(data.date);
                        } else {
                            var amount = '0';
                            $('#fetch_amount').html(amount);
                            $('#month_amount').html('0');
                            $('#daily_amount').html('0');
                            $('#bydate').html(data.date);
                        }

                    }
                });
            }

            function select_gym_wise_user_list(gym_id) {
                $.ajax({
                    url: "{{route('select_box_user_admin')}}",
                    data: {
                        'gym_id': gym_id,
                        '_token': "{{ csrf_token() }}"
                    },
                    type: "post",
                    success: function(data) {
                        // alert(data);
                        var obj = jQuery.parseJSON(data);
                        $.each(obj, (key, val) => {
                            let user_name = `<option class="cards-sub-plan" value="${val.id}">${val.first_name} ${val.last_name}</option>`;
                            $("#user_name").append(user_name);
                        });
                    }
                });
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: "{{route('get_payment_period_admin')}}",
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                type: "post",
                success: function(data) {
                    // alert(data);
                    var value = jQuery.parseJSON(data);
                    console.log(value);
                    if (value.status) {
                        $('#payment_date').html(value.payout_date);
                        $('#from_date').html(value.start_date);
                        $('#to_date').html(value.to_date);
                    }

                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".image-pay-hist").click(function() {
                $(".popup-paymentaa-second").toggle();
            });
        });

        $(document).ready(function() {
            $(".trigger-navbar").click(function() {
                $(".custom-navbar-pay-hist").slideToggle();
            });
        });
    </script>

@endsection
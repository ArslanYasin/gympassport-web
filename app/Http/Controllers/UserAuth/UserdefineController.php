<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;

class UserdefineController extends Controller
{
        public function index(){
            $authuser = Auth::User();
            $user_type = $authuser->user_type;

            if($authuser->status == '0' && $authuser->is_delete == '0'){
                Auth::logout();
                Session::flush();
                return redirect('/');
            }
            if($user_type == '3'){
                session()->put('user_id', $authuser->id);
                // echo session()->get('user_id');die;
                session()->put('user_token', $authuser->user_token);
                $before_login = session()->get('before_login_data'); //from subscripton page login use
                $before_login_from_gym = session()->get('before_login_gym_data');
                if (empty($before_login)) {
                    return redirect()->route('userhome');
                } elseif (empty($before_login_from_gym)) {
                    return redirect()->route('userhome');
                } else {
                    if ($before_login_from_gym == '400') {
                        session()->forget('before_login_gym_data');
                        return redirect()->route('findgym');
                    } else {
                        session()->forget('before_login_data');
                        return redirect()->route('sub_plan_detail');
                    }
                }
            }else if($user_type == '2'){
                    session()->put('user_id',$authuser->id);
                    session()->put('user_token',$authuser->user_token);
                    return redirect()->route('ufppatnerhome');
            }else if($user_type == '4'){
                    session()->put('user_id',$authuser->id);
                    session()->put('user_token',$authuser->user_token);
                    return redirect()->route('ufppatnerhome');
            }else if($user_type == '5'){
                    session()->put('user_id',$authuser->id);
                    session()->put('user_token',$authuser->user_token);
                    return redirect()->route('corporate-home');
            }else{
                Auth::logout();
                Session::flush();
                return redirect('/');
            }
        }
}

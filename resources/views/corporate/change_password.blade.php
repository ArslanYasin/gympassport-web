@extends('corporate_dash.design')
@section('title','Corporate | Edit Profile')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Profile
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('corporate-home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">edit-profile</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('updateCorporatePassword')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}

                                <div class="box-body">

                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{ auth()->user()->email }}" required>
                                            @if ($errors->has('email'))
                                                <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Current Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="current_password" class="form-control" placeholder="Enter your current password" value="{{ old('current_password') }}">
                                            @if ($errors->has('current_password'))
                                                <strong class="help-block">
                                                    {{ $errors->first('current_password') }}
                                                </strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">New Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="new_password" class="form-control" placeholder="Enter new password" value="{{ old('new_password') }}">
                                            @if ($errors->has('new_password'))
                                                <strong class="help-block">
                                                    {{ $errors->first('new_password') }}
                                                </strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Confirm New Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="confirm_password" class="form-control" placeholder="Enter new password" value="{{ old('confirm_password') }}">
                                            @if ($errors->has('confirm_password'))
                                                <strong class="help-block">
                                                    {{ $errors->first('confirm_password') }}
                                                </strong>
                                            @endif
                                        </div>
                                    </div>


                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>

@endsection

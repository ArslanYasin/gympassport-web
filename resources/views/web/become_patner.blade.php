<!DOCTYPE html>
<html>
<head>
    <title>Become-Partner</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=devic e-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('become_patner/css-become/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('become_patner/css-become/style-become-partner.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('become_patner/css-become/media-become-partner.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<style type="text/css">
    .text {
        color: red;
        border-color: red;
    }
</style>
<style type="text/css">
    #loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /* Add animation to "page content" */
    .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
    }

    @-webkit-keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }
        to {
            bottom: 0px;
            opacity: 1
        }
    }

    @keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }
        to {
            bottom: 0;
            opacity: 1
        }
    }

    #myDiv {
        display: none;
        text-align: center;
    }

    .successmsg-from-popup-wraper {
        background: #efe0e3;
        margin: 0px 0px 5px;
        padding: -18px 16px 9px 18px;
        text-align: center;
        max-width: 102%;
        padding: 17px -5px;
        margin-top: -39px;
        border-radius: 9px;
    }

    .successmsg-popup {
        font-size: 20px;
        color: green;
        font-weight: 700;
        font-family: Releway;
        text-transform: capitalize;
        padding: 59px 12px;
        line-height: 26px;
        text-align: center;
    }

    .close-button {
        float: right;
        font-size: 2.5rem;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .5;
        /* top: 0; */
        /* left: 0px; */
        margin-top: -58px;
    }

    .bold {

        font-weight: 600;
        font-size: 22px;
    }

    .numm_neww {
        margin: 0px 10px;
    }
</style>
<!--  <header class="header-become-partner">
      <div class="container">
        <div class="row">
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3">
          <div class="icon">
            <div class="icon-become-partner">
               <img src="{{ asset('become_patner/images/logo.png')}}" alt="icon">
              </div>
          </div>
                </div>
                <div class="col-6 col-sm-6 col-md-9 col-lg-9">
                    <div class="home-btn-box">
                        <button type="button" class="btn btn-primary home-btn home">Home</button>
                    </div>
                    <div class="home-btn-box">
                        <button type="button" class="btn btn-primary home-btn login-btn">Login</button>
                    </div>
                </div> -->

<header class="header">
    <div class="container-fluid">
        <div class="icon-hm-pg">
            <!-- <img src="images/logo.png" alt="icon" class="logo-image img-fluid"> -->
            <a href="{{url('/')}}">
                <img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="">
            </a>
        </div>
        <div class="custom-navbar-hm-pg">
            <ul>
                @if(isset(Auth::user()->id))
                    @if(Auth::user()->user_type=='3')
                        <li><a href="{{route('userhome')}}">Home</a></li>
                        <li><a href="{{route('findgym')}}">Find Gyms</a></li>
                    <!--                                     <li><a href="{{route('subscriptionplan')}}">Membership Plan</a></li>-->
                    @elseif(Auth::user()->user_type=='2')
                        <li><a href="{{route('ufppatnerhome')}}">Home</a></li>
                    @else
                        <li><a href="{{route('home')}}">Home</a></li>
                    @endif
                @else
{{--                    <li class="@if(request()->route()->getName()=='findgym') active @endif"><a href="{{route('findgym')}}">Find Gyms</a></li>--}}
                <!--                                <li class="@if(request()->route()->getName()=='subscriptionplan') active @endif">
                                  <a href="{{route('subscriptionplan')}}">Pricing</a></li>-->
                    <li class="@if(request()->route()->getName()=='becomepatner') active @endif">
                        <a href="{{route('becomepatner')}}">Become a Gym Partner</a></li>
                    <li class="@if(request()->route()->getName()=='corporate-partner') active @endif">
                        <a href="{{route('corporate-partner')}}">Corporate Partners</a></li>
                    <li><a href="{{route('singin')}}">Login</a></li>
                @endif
            </ul>
        </div>
        <div class="trigger-navbar-home-page">
            <span></span>
        </div>
    </div>
    <style type="text/css">
        li.active a {
            color: red;
        }
    </style>
    <script type="text/javascript">
        $('.home').click(function () {
            location.href = "{{url('/')}}";
        });
        $('.login-btn').click(function () {
            location.href = "{{route('singin')}}";
        });
    </script>
    </div>
    </div>
</header>
<section class="second-header">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="become-partner-banner">
                    <p>Expand your business with the <span class="bold-text">Ultimate Partnership!</span></p>
                    <p>Our Gym Passport is Pakistan wide with some of the top gyms on our roster!</p>
                </div>
                <div>
                    <button type="button" class="btn btn-danger register-now-btn">Register Now</button>
                </div>
                <div><a href="#"><img src="{{ asset('become_patner/images/arrow.png')}}" alt="arrow" class="img-fluid arrow-img"></a></div>
            </div>
        </div>
    </div>
</section>

<section class="benefits-wraper">
    <h3>Why Choose Us</h3>
    <h4>Benefits of Parterning with us</h4>
    <div class="benefits-points-wraper">
        <div class="benefits-points-leftimgbx">
            <img src="{{ asset('become_patner/images/1icon.png')}}" alt="image" class="img-fluid">

        </div>
        <div class="benefits-points-leftcontentbx benefits-points-leftcontentbx1">
            <div class="point-one-content">
                <h6>It's FREE!</h6>
                <p>Joining and staying to be our partner will cost you nothing. <span class="point-three-bold"><span>EVER!</p>
            </div>
        </div>
    </div>

    <div class="benefits-points-wraper benefits-points-rightwraper">
        <div class="benefits-points-leftcontentbx benefits-points-leftcontentbx2">
            <div class="point-two-content">
                <h6>New Customers</h6>
                <!--<p> Increase your<span class="point-two-bold"> gyms visibility</span> as our platform connects you to all our Gym Passport members.</p>-->
                <p> We target people outside of your target area and bring in more users to your Gym that otherwise would not visit. Utilize your unused space and earn more!</p>
            </div>
            <!-- <img src="images/1.png" alt="image" class="img-fluid"> -->
        </div>
        <div class="benefits-points-leftimgbx">
            <img src="{{ asset('become_patner/images/2icon.png')}}" alt="image" class="img-fluid">

        </div>
    </div>

    <div class="benefits-points-wraper">
        <div class="benefits-points-leftimgbx">
            <img src="{{ asset('become_patner/images/3icon.png')}}" alt="image" class="img-fluid">

        </div>
        <div class="benefits-points-leftcontentbx benefits-points-leftcontentbx3">
            <div class="point-three-content">
                <h6>More Income</h6>
                <p>Create a <span class="point-three-bold">new revenue stream</span> with every member that visits your fitness center.</p>
            </div>

        </div>
    </div>


    <div class="five-steps-heading">
        <h3 class="light-clr-heading-how">How it Works</h3>
        <h5>5 Simple Steps</h5>
    </div>
    <ul class="five-points-ul">
        <li class="pts pt1"><span class="bold">1. </span> Register to get started</li>
        <li class="pts"><span class="bold">2. </span> A member of our team will contact you and assist you in the partnership process</li>
        <li class="pts"><span class="bold">3. </span>Your gym will be added onto our platform. We will also update our users that you are now a GYM Passport partner</li>
        <li class="pts"><span class="bold">4. </span>Allow GYM Passport members to visit your facility during staffed hours. Validate their check-in and let them enjoy their
            workout!
        </li>
        <li class="pts pt5"><span class="bold">5. </span>Receive more income with every GYM Passport member that visits.</li>
    </ul>
</section>


<section class="simple-steps-wraper">
    <div class="simple-steps-inner-wrap">
        <div class="step-row">
            <div class="step-imgbx">
                <img src="{{ asset('become_patner/images/first-point.png')}}" alt="image" class="img-fluid">
            </div>
            <div class="step-contentbx step-contentbx1">
                <p><span class="numm_neww">1. </span>Register to get started</p>
            </div>
        </div>
        <div class="step-row">
            <div class="step-imgbx alignmt">
                <img src="{{ asset('become_patner/images/mid-point.png')}}" alt="image" class="img-fluid">
            </div>
            <div class="step-contentbx">
                <p><span class="numm_neww">2. </span>A member of our team will contact you and assist you in the partnership process</p>
            </div>
        </div>
        <div class="step-row">
            <div class="step-imgbx alignmt">
                <img src="{{ asset('become_patner/images/mid-point.png')}}" alt="image" class="img-fluid">
            </div>
            <div class="step-contentbx">
                <p><span class="numm_neww">3. </span>Your gym will be added onto our platform. We will also update our users that you are now a Gym Passport partner</p>
            </div>
        </div>
        <div class="step-row">
            <div class="step-imgbx alignmt">
                <img src="{{ asset('become_patner/images/mid-point.png')}}" alt="image" class="img-fluid">
            </div>
            <div class="step-contentbx">
                <p><span class="numm_neww">4. </span>Allow Gym Passport members to visit your facility during staffed hours. Validate their check-in and let them enjoy their
                    workout!</p>
            </div>
        </div>
        <div class="step-row">
            <div class="step-imgbx alignmt">
                <img src="{{ asset('become_patner/images/last-point.png')}}" alt="image" class="img-fluid">
            </div>
            <div class="step-contentbx">
                <p><span class="numm_neww">5. </span>Receive more income with every Gym Passport member that visits.</p>
            </div>
        </div>
    </div>
</section>

<div class="for-pattern-image">


    <div class="col-12 col-sm-12 col-md-12 col-lg-12">

    </div>

    <section class="image-box">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="that-easy-img">
                <img src="{{ asset('become_patner/images/easy.png')}}" alt="photo" class="img-fluid easy-img">
            </div>
        </div>
    </section>
    <section class="interest-box" id="bottom">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h3 class="light-clr-heading-join-ufp">Join GYM Passport</h3>
                    <h5 class="interest-heading">Register Your Interest!</h5>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="successmsg-from-popup-wraper" id="myPopup" style="display: none;">
                        <div class="successmsg-popup">
                            <div class="close-btnbx">
                                <button type="button" class="close close-button close-popup" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @if(Session::has('message'))
                                <script>
                                    document.getElementById('bottom').scrollIntoView();
                                    $("#myPopup").show();
                                    //setTimeout(function() { $("#myPopup").hide(); }, 5000);
                                </script>
                                {!! session('msg') !!}
                            @endif
                        </div>
                    </div>
                    <div id="loader"></div>
                    <form action="{{ route('add_become_patner') }}" method="post" id="submit_formss">
                        {{ csrf_field() }}
                        <div class="form-group user-form">
                            <label class="lbl">First Name</label>
                            <input type="text" name="first_name" class="form-control value gry-clr" placeholder="" value="{{ old('first_name') }}">
                            @if ($errors->has('first_name'))<strong class="text">{{ $errors->first('first_name') }}</strong><br>@endif
                            <label class="lbl">Last Name</label>
                            <input type="text" name="last_name" class="form-control value gry-clr" placeholder="" value="{{ old('last_name') }}">
                            @if ($errors->has('last_name'))<strong class="text">{{ $errors->first('last_name') }}</strong><br>@endif
                            <label class="lbl">Name of your fitness facility</label>
                            <input type="text" name="fitness_facility" class="form-control value grey-clr" placeholder="" value="{{ old('fitness_facility') }}">
                            @if ($errors->has('fitness_facility'))<strong class="text">{{ $errors->first('fitness_facility') }}</strong><br>@endif
                            <label class="lbl">Email</label>
                            <input type="email" name="email" class="form-control value grey-clr" placeholder="" value="{{ old('email') }}">
                            @if ($errors->has('email'))<strong class="text">{{ $errors->first('email') }}</strong><br>@endif
                            <label class="lbl">Phone</label>
                            <input type="number" name="phone_number" class="form-control value grey-clr" placeholder="" value="{{ old('phone_number') }}">
                            @if ($errors->has('phone_number'))<strong class="text">{{ $errors->first('phone_number') }}</strong><br>@endif
                            <label class="lbl">Your clubs registration fees (Rs)</label>
                            <input type="number" step="0.01" name="registration_fees" class="form-control value grey-clr" placeholder="" value="{{ old('registration_fees') }}">
                            @if ($errors->has('registration_fees'))<strong class="text">{{ $errors->first('registration_fees') }}</strong><br>@endif
                            <label class="lbl">Your clubs monthly fees (Rs)</label>
                            <input type="number" step="0.01" name="monthly_fees" class="form-control value grey-clr" placeholder="" value="{{ old('monthly_fees') }}">
                            @if ($errors->has('monthly_fees'))<strong class="text">{{ $errors->first('monthly_fees') }}</strong><br>@endif
                            <div>
                                <label class="lbl">Country</label>
                                <div>
                                    <select class="list" name="country">
                                        <option value="">Select your Country</option>
                                        @if(!empty($country))
                                            @foreach($country as $key=>$data)
                                                <option value="{{$data->id}}" @if($data->id==old('country')) selected @endif> {{$data->nicename}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('country'))<strong class="text">{{ $errors->first('country') }}</strong><br>@endif
                                </div>
                                <label class="lbl">State</label>
                                <div>
                                    <select class="list" name="state">
                                        <option value="">Select your State</option>
                                        @if(!empty($state))
                                            @foreach($state  as  $key=>$value)
                                                <option value="{{$value->id}}" @if($value->id==old('state')) selected @endif>{{$value->state}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('state'))<strong class="text">{{ $errors->first('state') }}</strong><br>@endif
                                </div>
                            </div>
                            <div><label class="lbl grey-clr">Address</label></div>
                            <input type="text" name="address" class="form-control value" placeholder="" value="{{ old('address') }}">
                            @if ($errors->has('address'))<strong class="text">{{ $errors->first('address') }}</strong><br>@endif
                        <!--                <label class="lbl">Postcode</label>
                <input type="text" name="postal_code" class="form-control value" placeholder="" value="{{ old('postal_code') }}">
                @if ($errors->has('postal_code'))<strong class="text">{{ $errors->first('postal_code') }}</strong><br>@endif-->
                        </div>
                        <div class="register-box-btn w-100">
                            <button type="submit" class="submit btn btn-danger register-btn text-center">Register Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @if($errors->has('first_name') || $errors->has('last_name') || $errors->has('email') || $errors->has('postal_code') || $errors->has('state'))
        <script type="text/javascript">
            document.getElementById('bottom').scrollIntoView();
            //document.getElementById('formpopup').classList.add('show');
            document.getElementById("loader").style.display = "none";
        </script>
    @endif
    @if(Session::has('message'))
        <script type="text/javascript">
            document.getElementById("loader").style.display = "none";
            //document.getElementById('formpopup').classList.add('show');
        </script>
    @endif
    <script>
        document.getElementById("loader").style.display = "none";
        $('.submit').click(function () {
            var myVar;
            document.getElementById("loader").style.display = "block";
            myVar = setTimeout(showPage, 9000);
        });

        function showPage() {
            // document.getElementById("loader").style.display = "none";
        }
    </script>
    <style type="text/css">
        .error {
            color: red;
        }
    </style>
    <script>

        $(document).ready(function () {
            $(".trigger-navbar-home-page").click(function () {
                $(".custom-navbar-hm-pg").slideToggle();
            });


            $('.register-now-btn').click(function (event) {
                event.preventDefault();
                $.scrollTo($('.user-form'), 1000);
            });
        });

        $(".register-now-btn").click(function () {
            $('html, body').animate({
                scrollTop: $(".user-form").offset().top - 200
            }, 1000);
        });
        $('.close-popup').click(function () {
            $('#myPopup').hide();
        });
    </script>
</body>
</html>
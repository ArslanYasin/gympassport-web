@extends('web_user_dash.design')
@section('content')
<section class="subsciption-section">
  <style type="text/css">
.link-venues a {
    background-color: #f1f1f100 !important;
}
</style>

   <div class="container-fluid">
      <div class="subscription-plan-page-box">
      @if(!empty($membership))
       @foreach($membership as $key=>$value)
         <div class="box-one-subscription zoom">
          @if($value['plan_cat_id'] == '1')
            <div class="box-one-for-bg-img blue">
          @elseif(($value['plan_cat_id'] == '2'))
            <div class="box-one-for-bg-img green">
          @elseif(($value['plan_cat_id'] == '3'))
            <div class="box-one-for-bg-img yellow">
          @elseif(($value['plan_cat_id'] == '4'))
            <div class="box-one-for-bg-img orange">
          @elseif(($value['plan_cat_id'] == '5'))
            <div class="box-one-for-bg-img pink pink-new">
          @endif
               <h4 class="saver-sub">{{$value['plan_cat_name']}}</h4>
               <p class="access-to">Access to</p>
               <span class="twenty">{{$value['total_gym']}}</span><span class="ufp-partner"> UFP partnered clubs</span>
               <div class="link-venues">
                <a href="{{route('plan_wise_gym_list',['plan_id'=>$value['plan_cat_id']])}}" >Click here to see all venues</a>
               </div>
            </div>
            <div class="next-part-sub">
              @if(isset(Auth::user()->id))
               <h4 class="no-of-visits">Choose your No. of Visits</h4>
              @endif
               <div class="for-partition">
                @php $i=0;@endphp
                  @foreach($value['plan_detail'] as $key=>$plan)
                    @if($i%2=='0')
                      <p class="ab pass-num">{{$plan['visit_pass']}}</p>
                      <p class="ab pass-value">${{$plan['amount']}}</p>
                    @else
                      <div class="bg-shadow">
                        <p class="ab pass-num ">{{$plan['visit_pass']}}</p>
                        <p class="ab pass-value">${{$plan['amount']}}</p>
                      </div>
                    @endif
                  @php $i++;@endphp
                @endforeach
                 <!--  <p class="ab pass-num">1 Visit Pass</p>
                  <p class="ab pass-value">5$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">5 Visit Pass</p>
                     <p class="ab pass-value">10$</p>
                  </div>
                  <p class="ab pass-num">10 Visit Pass</p>
                  <p class="ab pass-value">15$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">Monthly Visit Pass</p>
                     <p class="ab pass-value">250$</p>
                  </div> -->
               </div>
            </div>
         @if(isset(Auth::user()->id))
            <div class="button-sub-box">
               <a href="{{route('sub_plan',['plan_id'=>$value['plan_cat_id']])}}">
                <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
               </a>
               
            </div>
        @endif
         </div>
        @endforeach
    @endif
       <!--   <div class="box-one-subscription zoom">
            <div class="box-one-for-bg-img">
               <h4 class="saver-sub">Lite Fit Pass</h4>
               <p class="access-to">Access to</p>
               <span class="twenty">50</span><span class="ufp-partner"> UFP partnered clubs</span>
               <div class="link-venues">
                  <a href="#">Click here to see all venues</a>
               </div>
            </div>
            <div class="next-part-sub">
               <h4 class="no-of-visits">Choose your No. of Visits</h4>
               <div class="for-partition">
                  <p class="ab pass-num">1 Visit Pass</p>
                  <p class="ab pass-value">5$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">5 Visit Pass</p>
                     <p class="ab pass-value">10$</p>
                  </div>
                  <p class="ab pass-num">10 Visit Pass</p>
                  <p class="ab pass-value">15$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">Monthly Visit Pass</p>
                     <p class="ab pass-value">250$</p>
                  </div>
               </div>
            </div>
            <div class="button-sub-box">
               <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
            </div>
         </div> -->
        <!--  <div class="box-one-subscription zoom">
            <div class="box-one-for-bg-img">
               <h4 class="saver-sub">Gym Fit Pass</h4>
               <p class="access-to">Access to</p>
               <span class="twenty">100</span><span class="ufp-partner"> UFP partnered clubs</span>
               <div class="link-venues">
                  <a href="#">Click here to see all venues</a>
               </div>
            </div>
            <div class="next-part-sub">
               <h4 class="no-of-visits">Choose your No. of Visits</h4>
               <div class="for-partition">
                  <p class="ab pass-num">1 Visit Pass</p>
                  <p class="ab pass-value">5$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">5 Visit Pass</p>
                     <p class="ab pass-value">10$</p>
                  </div>
                  <p class="ab pass-num">10 Visit Pass</p>
                  <p class="ab pass-value">15$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">Monthly Visit Pass</p>
                     <p class="ab pass-value">250$</p>
                  </div>
               </div>
            </div>
            <div class="button-sub-box">
               <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
            </div>
         </div> -->
        <!--  <div class="box-one-subscription zoom">
            <div class="box-one-for-bg-img">
               <h4 class="saver-sub">Body Fit Pass</h4>
               <p class="access-to">Access to</p>
               <span class="twenty">150</span><span class="ufp-partner"> UFP partnered clubs</span>
               <div class="link-venues">
                  <a href="#">Click here to see all venues</a>
               </div>
            </div>
            <div class="next-part-sub">
               <h4 class="no-of-visits">Choose your No. of Visits</h4>
               <div class="for-partition">
                  <p class="ab pass-num">1 Visit Pass</p>
                  <p class="ab pass-value">5$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">5 Visit Pass</p>
                     <p class="ab pass-value">10$</p>
                  </div>
                  <p class="ab pass-num">10 Visit Pass</p>
                  <p class="ab pass-value">15$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">Monthly Visit Pass</p>
                     <p class="ab pass-value">250$</p>
                  </div>
               </div>
            </div>
            <div class="button-sub-box">
               <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
            </div>
         </div> -->
        <!--  <div class="box-one-subscription zoom">
            <div class="box-one-for-bg-img">
               <h4 class="saver-sub">Ultimate Fit Pass</h4>
               <p class="access-to">Access to</p>
               <span class="twenty">230</span><span class="ufp-partner"> UFP partnered clubs</span>
               <div class="link-venues">
                  <a href="#">Click here to see all venues</a>
               </div>
            </div>
            <div class="next-part-sub">
               <h4 class="no-of-visits">Choose your No. of Visits</h4>
               <div class="for-partition">
                  <p class="ab pass-num">1 Visit Pass</p>
                  <p class="ab pass-value">5$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">5 Visit Pass</p>
                     <p class="ab pass-value">10$</p>
                  </div>
                  <p class="ab pass-num">10 Visit Pass</p>
                  <p class="ab pass-value">15$</p>
                  <div class="bg-shadow">
                     <p class="ab pass-num">Monthly Visit Pass</p>
                     <p class="ab pass-value">250$</p>
                  </div>
               </div>
            </div>
            <div class="button-sub-box">
               <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
            </div>
         </div> -->
      </div>
   </div>
     <!-- Modal -->
 <!--  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top: 127px;">
      <div class="modal-content">
        <div class="modal-body">
          <div class="popup">
            <button type="button" class="btn btn-default cross-btn" data-dismiss="modal">X</button>
            <div class="popup-box-heading">
              <h4>Ultimate Fit Pass</h4>
            </div>
            <div class="search-wrapper">
              <div class="wrap">
                <div class="search">
                  <input type="text" name="searchdata" id="searchdata" onkeyup="keyupfunction();" class="searchTerm" placeholder="Search GYM Name">
                  <button type="button" id ="search" class="searchButton" onclick="search();">
                  </button>
                </div>
              </div>
            </div>
              <div class="row-fst" id="gym_list">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->
</section>
<script type="text/javascript">
    function myfunction(plan_id){
      var search = '';
      //alert(plan_id);
       $.ajax({
            url:"",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              //alert(data);
              $('#gym_list').html(data);
              $('#myModal').modal('show');
            }
        });
    }
  </script>
  <script type="text/javascript">
      function search(){
      var search = $('#searchdata').val();
      var plan_id = '';
       $.ajax({
            url:"",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
    }
  </script>
  <script type="text/javascript">
    function keyupfunction(){
      var search = $('#searchdata').val();
      var plan_id = '';
       $.ajax({
            url:"",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
    }
  </script>
<script>
   $(document).ready(function(){
       $(".image").click(function(){
           $(".popup-paymentaa").toggle();
       });
       });
   
   $(document).ready(function() {
       $(".trigger-navbar").click(function() {
           $(".custom-navbar").slideToggle();
       });
   });
</script

@endsection
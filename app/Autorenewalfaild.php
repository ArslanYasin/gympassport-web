<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autorenewalfaild extends Model
{
    protected $table ='auto_renewal_transaction_failed';
    
    protected $fillable = ['user_id','failed_message'];
}

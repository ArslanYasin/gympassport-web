<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\Mail\SendMailabletemplate;
class Landinguser extends Model
{
    protected $table ="landinguser";

    protected $fillable = ['first_name','last_name','email','dob','address','state','postal_code','suburb','country'];

     public function savedata($request){
    	$formdata = $request->all();
    	$insert = self::create($formdata);
    	$LastId = $insert->id;
        if($LastId > 0){
                \Mail::to($formdata['email'])->send(new SendMailabletemplate($LastId));
                $msg = trans('lang_data.success');
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
        }else{
                $msg = trans('lang_data.error');
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
        }
   }
   
    
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MonthlyUser\MonthlyUserPayAmountController;

class MonthlyAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Monthly:MonthlyAmountUpdateForGym';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly plan member amount update on table based on total visit on the last month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         $this->update_monthly_amount = new MonthlyUserPayAmountController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->update_monthly_amount->get_user();
        $this->info('Monthly plan member amount update on table based on total visit on the last month!');
    }
}

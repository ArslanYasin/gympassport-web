@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Gym
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('gymlist')}}">Gym-List</a></li>
        <li class="active">Edit-gym</li>
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
  .location{
    margin-top: 10px;
  }
  .note{
    color: red;
  }
  .img{
    width: auto;
    margin-top: 7px;
    height: 150px;
  }
  .fa-trash{
    cursor: pointer;
  }
   .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc !important;
    border: 1px solid #3c8dbc !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">     
             @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif 
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('update_gym',['id' => $gym_data->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <input type="hidden" name="user_type" value="2">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">First Name<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{$gym_data->gym_owner_detail->first_name}}" placeholder="Enter First Name">
                      @if ($errors->has('first_name'))
                         <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                      @endif
                  </div>
                </div> 

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Last Name<span class="star">*</span></label>
                  <div class="col-sm-10">
                   <input type="text" class="form-control @if($errors->has('last_name')) help-block @endif" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{$gym_data->gym_owner_detail->last_name}}">
                   @if ($errors->has('last_name'))
                         <strong class="help-block"> {{ $errors->first('last_name') }}</strong>
                  @endif
                  </div>
                </div> 

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email Address<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{$gym_data->gym_owner_detail->email}}"> 
                     @if ($errors->has('email'))
                         <strong class="help-block"> {{ $errors->first('email') }}</strong>
                      @endif
                  </div>
                </div>

              <!--    <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Password<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('password')) help-block @endif" id="password" name="password" placeholder="Enter Password" value="{{ old('password') }}">
                    
                     @if ($errors->has('password'))
                         <strong class="help-block"> {{ $errors->first('password') }}</strong>
                      @endif
                  </div>
                </div>  -->

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gym Name<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('gym_name')) help-block @endif" id="gym_name" name="gym_name" placeholder="Enter Gym Name" value="{{$gym_data->gym_name}}">
                     @if ($errors->has('gym_name'))
                         <strong class="help-block"> {{ $errors->first('gym_name') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gym Price Per Visit<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('gym_price_per_visit')) help-block @endif" id="gym_price_per_visit" name="gym_price_per_visit" placeholder="Enter Gym Price Per Visit" value="{{$gym_data->gym_price_per_visit}}">
                     @if ($errors->has('gym_price_per_visit'))
                         <strong class="help-block"> {{ $errors->first('gym_price_per_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control select2 @if($errors->has('country')) help-block @endif" id="country" name="country">
                      @if(!empty($country))
                        <option value=""> Country</option>
                        @foreach($country as $key=>$value)
                          @php $get = $value->id==$gym_data->country ? "selected" :"";@endphp
                           <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('country'))
                         <strong class="help-block"> {{ $errors->first('country') }}</strong>
                    @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Phone Number<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control @if($errors->has('phone_number')) help-block @endif" id="phone_number" name="phone_number" placeholder="Enter Phone Number" value="{{$gym_data->phone_number}}">
                     @if ($errors->has('phone_number'))
                         <strong class="help-block"> {{ $errors->first('phone_number') }}</strong>
                    @endif
                  </div>
                </div>

               

                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gym Activities<span class="star"></span></label>
                  <div class="col-sm-10">
                    <textarea class="form-control @if($errors->has('gym_activities')) help-block-text @endif" name="gym_activities" id="gym_activities" placeholder="Enter Gym Activities">{{$gym_data->gym_activities}}</textarea>
                     @if ($errors->has('gym_activities'))
                         <strong class="help-block"> {{ $errors->first('gym_activities') }}</strong>
                    @endif
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tell Us about your Gym<span class="star"></span></label>
                  <div class="col-sm-10">
                    <textarea class="form-control @if($errors->has('about_gym')) help-block-text @endif" name="about_gym" id="about_gym" placeholder="About your Gym">{{$gym_data->about_gym}}</textarea>
                     @if ($errors->has('about_gym'))
                         <strong class="help-block"> {{ $errors->first('about_gym') }}</strong>
                    @endif
                  </div>
                </div> 

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Address<span class="star">*</span></label>
                  <div class="col-sm-8">
                    <textarea class="form-control @if($errors->has('gym_address')) help-block-text @endif"  name="gym_address" id="gym_address" placeholder="Enter gym address">{{$gym_data->gym_address}}</textarea>
                     @if ($errors->has('gym_address'))
                         <strong class="help-block"> {{ $errors->first('gym_address') }}</strong>
                    @endif
                     <span id="auto_get" onclick="GetGeolocation()" class="btn bg-maroon location">Current Location</span>
                  </div>
                   <div class="col-sm-2">
                     <i class="fa fa-map-marker" id="btn" aria-hidden="true" style="font-size: 32px;cursor: pointer;"></i>
                   </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Gym Postalcode<span class="star">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control @if($errors->has('gym_pin_code')) help-block @endif" id="gym_pin_code" name="gym_pin_code" placeholder="Gym postalcode" value="{{$gym_data->gym_pin_code}}">
                        @if ($errors->has('gym_pin_code'))
                        <strong class="help-block"> {{ $errors->first('gym_pin_code') }}</strong>
                        @endif
                    </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gym latitude<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('gym_latitude')) help-block @endif" id="gym_latitude" name="gym_latitude" placeholder="Gym latitude" value="{{$gym_data->gym_latitude}}">
                     @if ($errors->has('gym_latitude'))
                         <strong class="help-block"> {{ $errors->first('gym_latitude') }}</strong>
                    @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gym longitude<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('gym_longitude')) help-block @endif" id="gym_longitude" name="gym_longitude" placeholder="Gym longitude" value="{{$gym_data->gym_longitude}}">
                     @if ($errors->has('gym_longitude'))
                         <strong class="help-block"> {{ $errors->first('gym_longitude') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Upload Photos<span class="star">*</span></label>
                  <div class="col-sm-10" id="imagerefresh">
                    <input type="file" accept="image/*" name="gym_image[]" class="form-control @if($errors->has('gym_image')) help-block @endif" multiple id="gym_image">
                     @if ($errors->has('gym_image'))
                         <strong class="help-block"> {{ $errors->first('gym_image') }}</strong>
                    @endif
                    <span class="note" style="display: none;">Note: You can upload maximum 5 image.</span>

                      <div class="row" style="margin-top: 7px;" id="imgid">
                       @if(!empty($gym_data->gym_image))
                          @foreach($gym_data->gym_image as $key=>$img)
                          <div class="col-sm-4">
                            <img class="img-responsive img" src="{{ Url('/')}}/{{$img->gym_image}}" alt="Photo">
                            <i class="fa fa-trash" title ="Delete Image" onclick="deleteimage('{{$img->id}}');" ></i>
                          </div>
                          @endforeach
                        @endif
                      </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Upload Logo<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="file" name="gym_logo" class="form-control @if($errors->has('gym_logo')) help-block @endif">
                     @if ($errors->has('gym_logo'))
                         <strong class="help-block"> {{ $errors->first('gym_logo') }}</strong>
                    @endif
                     <img class="img-responsive img" src="{{Url('/')}}/{{$gym_data->gym_logo}}" alt="@if(!empty($gym_data->gym_name)){{$gym_data->gym_name}}@endif profile picture">
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Facilities<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control select2 @if($errors->has('facilities_id')) help-block @endif" id="facilities_id" name="facilities_id[]" multiple="multiple" data-placeholder="Select a Facilities">
                      @if(!empty($facities))
                        @foreach($facities as $key=>$value)
                          @if(in_array($value->id, $facility_id))
                           <option value="{{$value->id}}" selected>{{$value->facilities}}</option>
                           @else
                            <option value="{{$value->id}}">{{$value->facilities}}</option>
                           @endif
                        @endforeach
                      @else
                        <option value=""> Facilities Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('facilities_id'))
                         <strong class="help-block"> {{ $errors->first('facilities_id') }}</strong>
                    @endif
                  </div>
                </div>
              </div>


                <div class="form-group time">
                  <label for="inputEmail3" class="col-sm-2 control-label">Timings<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <div  class="col-sm-12" style=" margin-bottom: 13px;">
                      <div  class="col-sm-3">
                            <input type="checkbox" name="is_all_day_open" value="1" class="largerCheckbox" id="checkall" onClick="check_uncheck_checkbox(this.checked);" @if($gym_data->is_all_day_open == 1)) checked @endif/>  <span class="allday">Open 24*7</span></div>                  
                    </div>
                     <div class="col-sm-12" style=" margin-bottom: 13px;">
                          <div  class="col-sm-3">
                                <input type="checkbox" value="1" class="largerCheckbox" id="staff_hours" @if($gym_data->is_all_day_open != 1)) checked @endif onClick="check_uncheck_checkbox_second(this.checked);"/>  <span class="allday" >Open hours</span>
                          </div>                 
                          </div>
            @php $check = in_array("0", $days)? "checked" : "";  @endphp
            @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '0')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                    <div  class="col-sm-6">
                      <div  class="col-sm-4">
                        <input type="checkbox" name="gym_open_days[]" value="0" class="flat-red" {{$check}}> Sunday
                      </div>
                      <div  class="col-sm-4 ">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" id="" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                         <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                     </div>
                      
                    </div>
                 @php $check = in_array("1", $days)? "checked" : ""; @endphp
                 @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '1')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="gym_open_days[]" value="1" class="flat-red" {{$check}}> Monday
                   </div>
                   <div  class="col-sm-4">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
               @php $check = in_array("2", $days)? "checked" : ""; @endphp
               @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '2')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="gym_open_days[]" value="2" class="flat-red" {{$check}}> Tuesday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
              @php $check = in_array("3", $days)? "checked" : ""; @endphp
              @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '3')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-5">
                   <input type="checkbox" name="gym_open_days[]" value="3" class="flat-red" {{$check}}> Wednesday
                 </div>
                   <div  class="col-sm-3">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-3">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
              @php $check = in_array("4", $days)? "checked" : ""; @endphp
              @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '4')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="gym_open_days[]" value="4" class="flat-red" {{$check}}> Thursday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
               @php $check = in_array("5", $days)? "checked" : ""; @endphp
               @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '5')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="gym_open_days[]" value="5" class="flat-red" {{$check}}> Friday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
                @php $check = in_array("6", $days)? "checked" : ""; @endphp
                @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->gym_time as $time)
                    @if($time->gym_open_days == '6')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing));  @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                <div  class="col-sm-6">                      
                    <div class="col-sm-4">
                   <input type="checkbox" name="gym_open_days[]" value="6" class="flat-red" {{$check}}> Saturday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
                   
                </div>
                 @if ($errors->has('gym_open_days'))
                         <strong class="help-block"> {{ $errors->first('gym_open_days') }}</strong>
                    @endif
                    @if ($errors->has('gym_open_timing'))
                         <strong class="help-block"> {{ $errors->first('gym_open_timing') }}</strong>
                    @endif
                    @if ($errors->has('gym_close_time'))
                         <strong class="help-block"> {{ $errors->first('gym_close_time') }}</strong>
                    @endif
                    
            <!--staff hours start here..-->
                    <div class="col-sm-10 col-sm-offset-2">
               
                     <div class="col-sm-12" style=" margin-bottom: 13px;">
                          <div  class="col-sm-3">
                                <input type="checkbox" value="1" name="is_all_day_open_staff"  class="largerCheckbox" id="staff_hours" @if($gym_data->is_staff_hours == 1)) checked @endif onClick="check_uncheck_checkbox_staff(this.checked);"/>  <span class="allday" >Staff hours</span>
                          </div>                 
                          </div>
                     <div  class="col-sm-12">
                          <textarea name="staff_hours" id="staff_hrs" class="form-control" placeholder="Enter staff hours">{{$gym_data->staff_hours}}</textarea>
                    </div>
<!--                        
                        
            @php $check = in_array("0", $staff_days)? "checked" : "";  @endphp
            @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '0')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                    <div  class="col-sm-6">
                      <div  class="col-sm-4">
                        <input type="checkbox" name="staff_open_days[]" value="0" class="flat-red" {{$check}}> Sunday
                      </div>
                      <div  class="col-sm-4 ">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" id="" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                         <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                     </div>
                      
                    </div>
                 @php $check = in_array("1", $staff_days)? "checked" : ""; @endphp
                 @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '1')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="staff_open_days[]" value="1" class="flat-red" {{$check}}> Monday
                   </div>
                   <div  class="col-sm-4">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
               @php $check = in_array("2", $staff_days)? "checked" : ""; @endphp
               @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '2')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="staff_open_days[]" value="2" class="flat-red" {{$check}}> Tuesday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
              @php $check = in_array("3", $staff_days)? "checked" : ""; @endphp
              @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '3')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-5">
                   <input type="checkbox" name="staff_open_days[]" value="3" class="flat-red" {{$check}}> Wednesday
                 </div>
                   <div  class="col-sm-3">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-3">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
              @php $check = in_array("4", $staff_days)? "checked" : ""; @endphp
              @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '4')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="staff_open_days[]" value="4" class="flat-red" {{$check}}> Thursday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
               @php $check = in_array("5", $staff_days)? "checked" : ""; @endphp
               @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '5')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                   <div  class="col-sm-6">
                    <div class="col-sm-4">
                   <input type="checkbox" name="staff_open_days[]" value="5" class="flat-red" {{$check}}> Friday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>
                @php $check = in_array("6", $staff_days)? "checked" : ""; @endphp
                @php $start_time = ''; $end_time = ''; @endphp
                @if($check != '')
                  @foreach($gym_data->staff_timing as $time)
                    @if($time->gym_open_days == '6')
                        @php $start_time = date('h:i A', strtotime($time->gym_open_timing));  @endphp
                        @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                    @endif
                  @endforeach
                @endif
                <div  class="col-sm-6">                      
                    <div class="col-sm-4">
                   <input type="checkbox" name="staff_open_days[]" value="6" class="flat-red" {{$check}}> Saturday
                 </div>
                   <div  class="col-sm-4">
                        <input type="text" name="staff_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                      </div>
                      <div  class="col-sm-4">
                        <input type="text" name="staff_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                      </div>
                 </div>-->
                   
                </div>
                 @if ($errors->has('gym_open_days'))
                         <strong class="help-block"> {{ $errors->first('staff_open_days') }}</strong>
                    @endif
                    @if ($errors->has('gym_open_timing'))
                         <strong class="help-block"> {{ $errors->first('staff_open_timing') }}</strong>
                    @endif
                    @if ($errors->has('gym_close_time'))
                         <strong class="help-block"> {{ $errors->first('staff_close_time') }}</strong>
                    @endif
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Update</button>
                <a href="{{route('gymlist')}}"><button type="button" class="btn btn-danger">Cancel</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
   <style type="text/css">
   input.largerCheckbox { 
       width: 20px;
        height: 20px;
    } 
</style>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
 function check_uncheck_checkbox(isChecked) {
  if(isChecked) {

    $('input[name="gym_open_days[]"]').each(function() { 
      $('input[name="gym_open_days[]"]').iCheck('uncheck');
    });
  } else {
    $('input[name="gym_open_days[]"]').each(function() {
      $('input[name="gym_open_days[]"]').iCheck('check');
    });
  }
}

function check_uncheck_checkbox_second(isChecked) {
   // alert('dd');
  if(isChecked) {
       $('input[name="gym_open_days[]"]').each(function() { 
      $('input[name="gym_open_days[]"]').iCheck('check');
    });
    $('#checkall').iCheck('uncheck');
  } else {
    $('input[name="gym_open_days[]"]').each(function() {
      $('input[name="gym_open_days[]"]').iCheck('uncheck');
    });
     $('#checkall').iCheck('check');
  }
}

function check_uncheck_checkbox_staff(isChecked) {
   // alert('dd');
  if(isChecked) {
       $('#staff_hrs').removeAttr('readonly');
       $('input[name="staff_open_days[]"]').each(function() { 
      $('input[name="staff_open_days[]"]').iCheck('check');
    });
   // $('#checkall').iCheck('uncheck');
  } else {
       $('#staff_hrs').attr('readonly','true');
       $('#staff_hrs').val('');
    $('input[name="staff_open_days[]"]').each(function() {
      $('input[name="staff_open_days[]"]').iCheck('uncheck');
    });
     //$('#checkall').iCheck('check');
  }
}
</script>
<script type="text/javascript">
    function deleteimage(img_id){
      //alert(img_id);
      $.ajax({
      type: "get",
      url: "{{url('delete-image')}}/"+img_id,
      success: function(data){
         //alert("Data Save: " + data);
         location.reload();
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('#gym_image').change(function(){
   //get the input and the file list
    var input = document.getElementById('gym_image');
      if(input.files.length>5){
          $('.note').css('display','block');
      }else{
          $('.note').css('display','none');
          $("#gym_image").trigger('reset');
      }
   });
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=initMap"
  type="text/javascript"></script>

 <script type="text/javascript">
//Function to covert address to Latitude and Longitude
 $('#btn').on('click',function(){
 var address = $('#gym_address').val();
 if(address ==''){
  alert('please enter address first');
 }
 else{
  var getLocation =  function(address) {
  //alert(address);
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
      var latitude = results[0].geometry.location.lat();
      var longitude = results[0].geometry.location.lng();
    //  alert(latitude +"-----"+ longitude);
      $('#gym_latitude').val(latitude);
      $('#gym_longitude').val(longitude);
      //console.log(latitude, longitude);
      } 
  }); 


}
 }

var address = $('#gym_address').val();
getLocation(address);
    
});

</script>
<script type="text/javascript">
 /*auto detect address and lat long*/
function GetGeolocation() {

navigator.geolocation.getCurrentPosition(GetCoords, GetError);

}


function GetCoords(position){
 
  document.getElementById('gym_longitude').value = position.coords.longitude;
  document.getElementById('gym_latitude').value = position.coords.latitude;
  var geocoder= new google.maps.Geocoder();
 var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
geocoder.geocode({'latLng': latlng}, function(results, status) {
  if (status == google.maps.GeocoderStatus.OK) {
    if (results[1]) {
      var infowindow = new google.maps.InfoWindow({
        content: name
      });
       var add= results[0].formatted_address;
       document.getElementById('gym_address').value = add;
       console.log(results);
    }
  } else {
    alert("Geocoder failed due to: " + status);
  }
});

}
function GetError(){
  aler("Fail to get location.");
}
   </script>  
@endsection

<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Auth;

class CheckWebStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset(Auth::user()->user_type)){
            if(Auth::user()->user_type == 4){
                $tokenTime_db = new \DateTime(Auth::user()->user_token_time);
                $current_time = new \DateTime();
                $time = $current_time->diff($tokenTime_db);
                if ($time->i <= 3000) {
                    Auth::user()->user_token_time = $current_time;
                    //$user->update();
                    return $next($request);
                }else{
                    $msg = "Session time out, Please login again";
                    Auth::logout();
                    Session::flush();
                    Session::flash('msg',$msg);
                    Session::flash('message','alert-danger');
                    return redirect()->route('singin');
                }
            }else{
                Auth::logout();
                Session::flush();
                return redirect()->route('singin');
            }
        }else{
            return redirect()->route('singin');
        }
    }
}

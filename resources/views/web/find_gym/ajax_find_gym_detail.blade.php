<!-- Modal content-->
<div class="modal-content">
   <div class="wrap">
       <button type="button" class="closebtn cross2" id="cross2-id" onclick="hideBox();">&lt</button>
      <div class="left-part-mapview-gymdetails" id="forModalHide">
         <div class="cross-fit">
            <h5>@if(!empty($gym_detail)) {{$gym_detail['gym_name']}}  @endif</h5>
         </div>
         <div class="mapview-box-for-scroll">
            <div class="four-images">
               <div class="owl-carousel owl-theme" id="slider1">
                @if(!empty($gym_detail) && !empty($gym_detail['gym_image']))
                  @foreach($gym_detail['gym_image'] as $image)
                    <div class="item">
                       <div class="blank-photo">
                          <a href="{{$image['gym_image']}}">
                            <img src="{{$image['gym_image']}}" id="myImg" class="myImg" style="height: 190px;">
                          </a>
                       </div>
                    </div>
                  @endforeach
                @endif
               
               </div>
               <div class="hr-bar-top"><span></span>
               </div>
            </div>
            <div class="row">
               <div class="col-6 col-sm-6 class-for-direction-phone">
                  <a href="https://www.google.com/maps/?q= {{$gym_detail['gym_latitude'].','.$gym_detail['gym_longitude']}}" target="_blank"><img src="{{ asset('website_file/images/direction.png')}}" alt="get-direction-icon"class="icon-gymdetails img-fluid"></a>
                  <p class="for-info" style="font-size: 20px">Get Direction</p>
               </div>
               <div class="col-6 col-sm-6 class-for-direction-phone">
                  <a href="#"><img src="{{ asset('website_file/images/contact-us.png')}}" alt="contact-us-icon" class="icon-gymdetails img-fluid"></a>
                  <p class="for-info" style="font-size: 20px"> @if(!empty($gym_detail))
                        {{$gym_detail['phone_number']}}
                      @endif</p>
               </div>
             
            </div>
            <div class="hr-bar-top"><span></span>
            </div>
              @if(!empty($gym_detail))
            <div class="row m-0">
               <h5 class="four-a">About Us</h5>
               <div class="col-sm-12">
                  <div class="four-stmt-box">
                     <p class="four-a-stmt about-us">
                    
                        {{$gym_detail['about_gym']}}
                                        
                      </p>
                  </div>
               </div>
            </div>
             @endif     
           <div class="row m-0">
          <h5 class="four-a">Facilities</h5>
          <div class="col-sm-12">
            <div class="four-stmt-box">
              <div class="four-a-stmt-new about-us">
                @if(!empty($gym_detail['facilities']))
                @foreach($gym_detail['facilities'] as $val)
                <div class="row" style="margin-bottom: 10px;">
                  <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="four-stmt-image">
                      <img src="{{$val['facilities_image']}}" alt="image" class="img-fluid">
                    </div>
                  </div>
                  <div class="col-9 col-sm-9 col-md-9 col-lg-9 text-center">
                    <p class="four-a-stmt-new-text">{{$val['facilities_name']}}</p>
                  </div>
                </div>
                @endforeach
                @endif 
              </div>
            </div>
          </div>
        </div>
            <div class="row m-0">
               <h5 class="four-a">Address</h5>
               <div class="col-sm-12">
                  <div class="four-stmt-box">
                     <p class="four-a-stmt address">
                       @if(!empty($gym_detail))
                        {{$gym_detail['gym_address']}}
                      @endif
                     <!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim -->
                   </p>
                  </div>
               </div>
            </div>
         
            <h5 class="four-a">Opening Hours</h5>
            <div class="schedule avl-dayz-new-class">
               <div class="row m-0">
                 @if(isset($gym_detail['is_all_day_open']) && !empty($gym_detail['is_all_day_open']=='1'))

                      <div class="col-sm-12">
                        <div class="four-stmt-box">
                          <p class="four-a-stmt">
                            Open 24 * 7 gym
                          </p>
                        </div>
                      </div>
                 @endif
                @if(!empty($gym_detail['gym_time']))
                  @if(in_array("0", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='0')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Sunday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                              <!--   <div class="col-4 col-sm-3">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Sunday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-3">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("1", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='1')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Monday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-3">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Monday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                                <!-- <div class="col-4 col-sm-3">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("2", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='2')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Tuesday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-3">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Tuesday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-3">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("3", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='3')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Wednesday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Wednesday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("4", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='4')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Thursday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Wednesday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("5", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='5')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Friday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                              <!--   <div class="col-4 col-sm-4">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Friday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                              <!--   <div class="col-4 col-sm-4">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  @if(in_array("6", $gym_detail['is_gym_open']))
                      @foreach($gym_detail['gym_time'] as $key=>$time)
                        @if($time['gym_open_days'] =='6')
                          <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Saturday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_open_timing']}}</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash">-</p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">{{$time['gym_close_time']}}</p>
                                   </div>
                                </div>
                             </div>
                          </div>
                        @endif
                      @endforeach
                  @else
                    <div class="col-sm-12">
                             <div class="row">
                                <div class="col-12 col-sm-3">
                                   <p class="days">Saturday</p>
                                </div>
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="time">Close</p>
                                   </div>
                                </div>
                               <!--  <div class="col-4 col-sm-4">
                                   <p class="dash"></p>
                                </div> -->
                                <div class="col-4 col-sm-4">
                                   <div class="time-box">
                                      <p class="dash"></p>
                                   </div>
                                </div>
                             </div>
                          </div>
                  @endif
                  
               @endif
               </div>
            </div>
            
            <!--staff hours start.....-->
            @if(!empty($gym_detail['gym_staff_time']))
            <div class="row m-0">
               <h5 class="four-a">Staffed Hours</h5>
               <div class="col-sm-12">
                  <div class="four-stmt-box">
                     <p class="four-a-stmt address">
                      {{$gym_detail['gym_staff_time']}}
                     <!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim -->
                   </p>
                  </div>
               </div>
            </div>
           @endif
<!--           <h5 class="four-a"></h5>-->
            <div class="schedule avl-dayz-new-class">
<!--               <div class="row m-0">
                   <div class="col-sm-12">
                       <p class="days">{{$gym_detail['gym_staff_time']}}</p
                   </div>
               </div>

            </div>-->
            
            <!--staff hours end....-->
            
            <div class="col-sm-12">
                  <div class="four-stmt-box">
                     <p class="four-a-stmt-custome about-us">
                       @if(isset(Auth::user()->id))
                        <button class="btn btn-danger send-query-btn-custome" id="checkin" onclick='check_in({{$gym_detail["gym_id"]}})'>Check-In</button>
                        @else
                        <!-- <button type="button" class="btn btn-danger send-query-btn-custome" onclick="openmodel();">Check-In</button> -->
                        @endif
                     </p>
                  </div>
               </div>

               <div class="col-sm-12" id="display" style="display: none;">
                  <div class="four-stmt-box gym_msg">
                     <p class="four-a-stmt about-us" id="checkin_msg">
                        
                      </p>
                  </div>
               </div>
            
         </div>
      </div>
   </div>

</div>

<!-- The Modal -->

<!-- <div id="myModal9" class="modal1">
  <span class="close11" id='closeModalBtn'>&times;</span>

  <img src="{{$image['gym_image']}}" class="modal-content1" id="img01">

  <div id="caption"></div>
</div> -->
<script type="text/javascript">
  let myImg=document.querySelector('#myImg');
  let myModal9=document.querySelector('#myModal9');
  myImg.addEventListener('click',function(){
       //myModal9.style.display='block'
        $('#myModal9').modal('show');
  })
</script>

<!-- <script>
//   $("span[data-number=1]").click(function(){
//     $('modal1').modal('hide');
// });
// Get the modal
var modal2 = document.getElementById("myModal9");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
// var captionText = document.getElementById("caption");
img.onclick = function(){
  modal2.style.display = "block";
  modalImg.src = this.src;
  
}

// Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close11")[0];

// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal2.style.display = "none";
// }
</script> -->
<script type="text/javascript">
    // $(document).ready(function(){
    //     $('#checkin').keyup(function(){
    //         var gym_id = "{{$gym_detail['gym_id']}}";
    //         $.ajax({
    //         url:"{{route('check_in')}}",
    //         data: {'gym_id':gym_id,'_token': "{{ csrf_token() }}"},
    //         type: "post",
    //         success: function(data){
    //           alert(data);
    //           // $('#ajax_find_side_gym_list').html(data)
    //         }
    //        });
    //     });
    // });
</script>
<style type="text/css">
  .four-a-stmt-custome {
    border-radius: 10px;
    font-size: 19px;
    font-weight: 600;
    color: #9c9c9c;
    line-height: 23px;
    margin: 0px -104px;
    overflow: hidden;
    margin: 0 auto;
    width: 100%;
    max-width: 92%;
    display: flex;
    border: none;
}
.send-query-btn-custome{
    text-align: center;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    color: #ffffff;
    padding:11px 150px;
    background-color: #be0027;
    border: none;
    font-size: 28px;
    font-weight: 700;
    border-radius: 9px;
    -webkit-box-shadow: 0px 10px 10px #0e0e0e;
    -moz-box-shadow: 0px 10px 10px #0e0e0e;
    -ms-box-shadow: 0px 10px 10px #0e0e0e;
    -o-box-shadow: 0px 10px 10px #0e0e0e;
    box-shadow: 0px 10px 10px #0e0e0e;
    margin-top: 28px;
}
</style>
<style type="text/css">
   .alltimeopen{
  line-height: 0px;
  color: #9c9c9c;
  font-size: 19px;
  font-weight: 600;
  }
  .four-a-stmt-new {
  border-radius: 10px;
  padding: 15px 0px 25px 25px;
  font-size: 19px;
  font-weight: 600;
  color: #9c9c9c;
  background-color: #383838;
  line-height: 23px;
  margin: 0px -104px;
  overflow: hidden;
  margin: 0 auto;
  width: 100%;
  max-width: 92%;
  border: none;
  }
 
</style>
<script>
   function openNav() {
       if (window.matchMedia('(max-width: 400px)').matches)
       {
          document.getElementById("mySidenav").style.width = "300px";
          $('#mySidenav').css('display', 'block');

       }
       else{
            document.getElementById("mySidenav").style.width = "400px";
            $('#mySidenav').css('display', 'block');
       }

       $('.cross').css('display', 'block');
   }
            
   function closeNav() {
       document.getElementById("mySidenav").style.width = "0";
       $('#mySidenav').css('display', 'none');
       $('.cross').css('display', 'none');
   }
            
   $('#slider1').owlCarousel({
       loop: false,
       margin: 10,
       nav: false,
       dots: true,
       responsive: {
           0: {
               items: 3
           },
           600: {
               items: 3
           },
           1000: {
               items: 3
           }
       }
   })

   $('div.blank-photo').magnificPopup({delegate: 'a' , type: 'image', gallery:{enabled:true}});
            
   var sheet = document.createElement('style'),
       $rangeInput = $('.range input'),
       prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];
   
   document.body.appendChild(sheet);

   var getTrackStyle = function(el) {
       var curVal = el.value,
           val = (curVal - 1) * 16,
           style = '';
       // console.log(val);
   
       // Set active label
       $('.range-labels li').removeClass('active selected');
   
       var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');
   
       curLabel.addClass('active selected');
       curLabel.prevAll().addClass('selected');
   
       // Change background gradient
       for (var i = 0; i < prefs.length; i++) {
           // style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
           style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #be0027 0%, #be0027 ' + val + '%, #383838 ' + val + '%, #383838 100%)}';
       }
   
       return style;
   }
            
   $rangeInput.on('input', function() {
       sheet.textContent = getTrackStyle(this);
   });
   
   // Change input value on label click
   $('.range-labels li').on('click', function() {
       var index = $(this).index();
   
       $rangeInput.val(index + 1).trigger('input');
   
   });
            
</script>



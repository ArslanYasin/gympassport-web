@extends('web_user_dash.design')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<section class="pay-hist-section">
    <style type="text/css">
        .custommsg{
            margin-top: 15px;
            width: 85%;
            margin-left: 53px;
        }
        .fa.fa-map-marker.map_custom_cls
        { 
            font-size: 32px;
            cursor: pointer;
        }
        .fa.fa-map-marker.map_custom_cls {
            color: #9c9c9c;
            float: right;
            margin-top: -11px;
        }

        span.select2-selection.select2-selection--multiple {
            background: none;
            border: none;
            border-bottom: 2px solid #9c9c9c;
            border-radius: 0px;
            margin-left: -19px;
        }
        input.select2-search__field::placeholder {
            color: #9c9c9c;
            font-size: 19px;
            font-weight: 700;
        }
        img.img-responsive.img {
            width: 105px;
            height: 62px;
        }
        span.select2.select2-container.select2-container--default {
            width: 93% !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple {

            border: none;
        }
        .error{
            color: red;
            margin: 0px 0px 0px -17px;
        }
        .list-paym-hist-page {
            width: 43%;
        }
        .key-paym {
            font-size: 18px;
            color: #fff;
            /*line-height: 3px;
            */}
        .key-paym-heading {
            font-size: 23px;
            color: #fff;
            font-weight: 700;
            line-height: 32px;
            float: left;
        }
        .value-box-paym {
            padding: 0px 0px;
        }
        .star{
            color: red;
        }
        input[type="checkbox"] {
            margin-top: 3px;
            display: block !important;
            width: 31px;
            height: 15px;
        }
        span.allday {
            margin-left: 13px;
            color: #fff;
        }

        .select2-container--default 
        .select2-selection--multiple 
        .select2-selection__choice {
            background-color: #383838;
            color: #9c9c9c;
        }
        .box_of_btn_save {
            width: 100%;
            max-width: 100%;
            display: flex;
            justify-content: center;
        }
        #fakeInput,
        #fakeInput2,
        #fakeInput3,
        #fakeInput4 {
            color: #9c9c9c;
            background: #383838;
            font-weight: 700;
            border: none;
            padding: 2px 10px;
            border-bottom: 2px solid #9c9c9c;
            margin-left: -17px;
        }
        .buttonImage {
            float: right;
            position: absolute;
            right: 10px;
            top: 0;
            background: #9c9c9c;
            padding: 3px 9px;
            color: white;
            font-weight: bold;
            border-radius: 0px 8px 8px 0px;
            height: 30px;
        }

        #fakeDiv,#fakeDiv2,#fakeDiv3,#fakeDiv4 {
            position: relative;
        }
        #selectedFile,#selectedFile2,#selectedFile3,#selectedFile4 {
            opacity:0;
            position:absolute;
            left: 0;
            top: 0;
        }
        #selectedFile{
            opacity: 1;
            margin-top: -12px;
        }
        #loader {
            left: 49%;
            top: 50%;
        }
        #newfakeInput{
            color: #9c9c9c;
            background: #383838;
            font-weight: 700;
            border: none;
            padding: 2px 10px;
            border-bottom: 2px solid #9c9c9c;
            margin-left: 0px;
        }
        @media(max-width: 1920px){
            #newfakeInput {
                width: 100%;
                margin-left: -19px;
            }
        }
        /*   @media(max-width: 1600px){
           .box-one-paym {
           width: 40%;
           display: block;
           float: left;
           margin: 42px 0px 0 52px;
       }
       }*/

        @media(max-width: 1366px){
            #newfakeInput {
                margin-left: -16px;
            }
        }
        @media(max-width: 1200px){
            .btn.btn-primary.current_location_btn {
                font-size: 13px;
            }
            .key-paym {
                font-size: 15px;
            }
            .div_for_imgg {
                margin-right: -178px;
            }
            li.nav-item.edit_gym_heading a.nav-link.active{
                font-size: 22px;
            }
            li.select2-selection__choice {
                font-size: 12px;
            }
            .btn.btn-danger.save-btn-paym {
                font-size: 19px;
            }
            img.img-fluid.class_img_gym {
                width: 88px;
                height: 59px;
            }
            img.img-fluid.img_near_trash
            {
                width: 88px;
                height: 59px;
            }
        }
        @media(max-width: 1024px){
            #newfakeInput {
                margin-left: -19px;
            }
        }
        @media(max-width: 768px){
            /* .div_for_imgg {
             margin-right: 0px;
             width: 100%;
             margin-top: 20px;
            }*/
            .box-one-paym.facities_divv {
                margin-bottom: 12px;
            }
            img.img-fluid.class_img_gym {
                width: 82px;
                height: 54px;
            }
            img.img-fluid.img_near_trash{
                width: 100px;
                height: 54px;
            }
            .box-one-paym.class_for_mrgnn{
                margin-top: 50px;
            }
            .div_for_imgg {
                margin-right: 0px;
                width: 100%;
                margin-top: -112px;
            }

            .btn.btn-primary.current_location_btn {
                margin-top: 5px;
                margin-left: 1px;
                margin-bottom: 12px;
                font-size: 11px;
            }
            span.select2-selection.select2-selection--multiple {
                width: 100%;
                max-width: 91%;
                margin-left: 1px;
            }
            span.buttonImage.buttonImage_second{
                right: 49px;
            }
            .fa.fa-map-marker.map_custom_cls {
                float: none;
            }
            input#newfakeInput {
                width: 75%;
                margin-left: 2px;
            }
            /* .bank-det.value-box-paym.new_cls_for_boxes {
                 display: flex;
                 justify-content: center; 
                 width: 100%;
              }*/
            .btn.btn-danger.save-btn-paym {
                font-size: 16px;
                padding: 4px 26px;
                border-radius: 5px;
            }
            li.nav-item.edit_gym_heading a.nav-link.active {
                margin-top: 29px;
            }
        }
        @media(max-width: 575px){
            .key-paym {
                font-size: 12px;
            }

            .fa.fa-map-marker.map_custom_cls {
                font-size: 25px;
            }
            li.select2-selection__choice {
                font-size: 11px;
            }
            img.img-fluid.img_near_trash {
                width: 56px;
                height: 51px;
            }
            span.buttonImage.buttonImage_second {
                height: 25px;
                top: 0px;
                right: 38px;
            }
        }
        @media(max-width: 450px){
            span.buttonImage.buttonImage_second {
                right: 17px;
            }
        }
    </style>
    <style type="text/css">
  .time{
    width: 100%;
    margin-left: 24px !important;
    float: left;
    margin-top: 15px;
    color: white;
  }

  input.largerCheckbox{display: inline-block !important;}
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
  .location{
    margin-top: 10px;
  }
  .note{
    color: red;
  }
  .img{
    width: auto;
    margin-top: 7px;
    height: 150px;
  }
  .fa-trash{
    cursor: pointer;
  }
   .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc !important;
    border: 1px solid #3c8dbc !important;
  }
  .daywrp{
    /*width: 50%;*/
    float: left;
    margin-bottom: 5px;
  }

  .timesetting{
    /*width: calc(100% / 3 - 5px);*/
    display: inline-block;
  }
</style>
    <div class="pay-hist-menu-box">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item edit_gym_heading">
                <a class="nav-link active" data-toggle="pill" href="#menu-one">Edit Gym</a>
            </li>
            <!--            <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#menu-two">Payment History</a>
                        </li>-->
        </ul>
    </div>
    <div class="tab-content">
        <div id="menu-one" class="container-fluid tab-pane active">
            <div class="main-box-bank-det">
                {{-- @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible custommsg">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif --}}
                @if(Session::has('message'))
                    @if(session('message')=='alert-danger')
                        <script type="text/javascript">
                            swal({
                            title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'error'
                            });
                        </script>
                        @else
                        <script type="text/javascript">
                            swal({
                            title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'success'
                            });
                        </script>
                        @endif
                    @endif
                    <div id="loader" style="display: none;"></div> 
                    <form action="{{route('owner_update_gym',['id' => $gym_data->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}

                        <!--                    <div class="box-one-paym" style="width: 90%">
                                                <h4 class="key-paym-heading">Personal Information :</h4>
                                            </div>-->

                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Gym Name <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="text" name="gym_name" class="value-paym" placeholder="Gym Name" value="{{$gym_data->gym_name}}">
                                @if ($errors->has('gym_name'))<br><span class="error" for="">{{ $errors->first('gym_name') }}</span>@endif

                            </div>
                        </div>
                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Phone Number <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="number" name="phone_number" class="value-paym" placeholder="Phone Number" value="{{$gym_data->phone_number}}">
                                @if ($errors->has('phone_number'))<br><span class="error" for="">{{ $errors->first('phone_number') }}</span>@endif

                            </div>
                        </div>
<!--                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">About Gym <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <textarea name="" class="value-paym" placeholder="About gym" rows="2">{{$gym_data->about_gym}}</textarea>
                                @if ($errors->has('about_gym'))<br><span class="error" for="">{{ $errors->first('about_gym') }}</span>@endif

                            </div>
                        </div>-->
<!--                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Gym Address <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <textarea name="gym_address" class="value-paym" placeholder="About gym" rows="2" id="gym_address">{{$gym_data->gym_address}}</textarea>
                                <span class="map_div"><i class="fa fa-map-marker map_custom_cls" id="btn" aria-hidden="true"></i></span>
                                @if ($errors->has('gym_address'))<br><span class="error" for="">{{ $errors->first('gym_address') }}</span>@endif
                                <div id="auto_get" onclick="GetGeolocation()" class="btn btn-primary current_location_btn">Current Location</div>


                            </div>
                        </div>-->
<!--                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Gym Postalcode <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="number" name="gym_pin_code" class="value-paym" placeholder="Gym Postal Code" value="{{$gym_data->gym_pin_code}}">
                                @if ($errors->has('gym_pin_code'))<br><span class="error" for="">{{ $errors->first('gym_pin_code') }}</span>@endif
                            </div>
                        </div> -->
                       
                      
<!--                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Gym Latitude <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="text" name="gym_latitude" class="value-paym" id="gym_latitude" placeholder="Gym Latitude" value="{{$gym_data->gym_latitude}}">
                                @if ($errors->has('gym_latitude'))<br><span class="error" for="">{{ $errors->first('gym_latitude') }}</span>@endif
                            </div>
                        </div>-->
<!--                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Gym Longitude <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="text" name="gym_longitude" class="value-paym"  id="gym_longitude" placeholder="Gym Longitude" value="{{$gym_data->gym_longitude}}">
                                @if ($errors->has('gym_longitude'))<br><span class="error" for="">{{ $errors->first('gym_longitude') }}</span>@endif
                            </div>
                        </div>-->




                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Upload Logo (1.2 max size)<span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym" id="fakeDiv">
                                <input type="file" id="selectedFile" name="gym_logo" class="value-paym" placeholder="" value="">
                                <?php /*
                                <input type="text" id="newfakeInput" disabled/>

                                <span onclick="document.getElementById('selectedFile').click();" class="buttonImage buttonImage_second" >Browse</span>
                                @if ($errors->has('gym_logo'))<br><span class="error" for="">{{ $errors->first('gym_logo') }}</span>@endif */ ?>
                            </div>

                        </div>

                        <div class="box-one-paym class_for_mrgnn">
                            <div class="bank-det">
                                <h3 class="key-paym">Upload Photos (1.2 max size)<span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym" id="fakeDiv3">
                                <input type="file" name="gym_image[]" class="value-paym" placeholder="" value="" multiple id="gym_image">
                                <span class="note" style="display: none;">Note: You can upload maximum 5 image.</span>
                                @if ($errors->has('additional_document_front'))<br><span class="error" for="">{{ $errors->first('additional_document_front') }}</span>@endif
                            </div>

                        </div>
                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym"> <span class="star"></span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <div class="div_for_imgg">
                                    <img class="img-fluid class_img_gym" src="{{Url('/')}}/{{$gym_data->gym_logo}}" alt="@if(!empty($gym_data->gym_name)){{$gym_data->gym_name}}@endif profile picture">
                                </div>
                            </div>

                        </div>
                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym"><span class="star"></span></h3>
                            </div>
                            <div class="bank-det value-box-paym new_cls_for_boxes">
                                <div class="row" style="margin-top: 7px;" id="imgid">
                                    @if(!empty($gym_data->gym_image))
                                    @foreach($gym_data->gym_image as $key=>$img)
                                    <div class="col-4 col-sm-4">
                                        <img class="img-fluid img_near_trash" src="{{ Url('/')}}/{{$img->gym_image}}" alt="Photo">
                                        <i class="fa fa-trash trash_custom_cls" title ="Delete Image" onclick="deleteimage('{{$img->id}}');" ></i>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="box-one-paym facities_divv" style="margin-top: 0px;">
                            <div class="bank-det">
                                <h3 class="key-paym">Facilities <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <select id="facilities_id" name="facilities_id[]" multiple="multiple" data-placeholder="Select a Facilities" class="value-paym select2 custom_select2">
                                    @if(!empty($facities))
                                    @foreach($facities as $key=>$value)
                                    @if(in_array($value->id, $facility_id))
                                    <option value="{{$value->id}}" selected>{{$value->facilities}}</option>
                                    @else
                                    <option value="{{$value->id}}">{{$value->facilities}}</option>
                                    @endif
                                    @endforeach
                                    @else
                                    <option value=""> Facilities Unavailable</option>
                                    @endif
                                </select>
                                @if ($errors->has('gym_pin_code'))<br><span class="error" for="">{{ $errors->first('gym_pin_code') }}</span>@endif
                            </div>
                            <script>
                                $(".select2").select2({
                                maximumSelectionLength: 20
                                });
                            </script>
                        </div>
                        <div class="row col-12">
                            <div class="form-group time">
                                <div class="col-lg-2 col-md-2 col-sm-12 text-left">
                                    <label for="inputEmail3" class="col-sm-12 control-label">Timings<span class="star">*</span>
                                    </label>      
                                </div>
                                <div class="col-md-10 col-sm-12">
                                    <div  class="col-sm-12">
                                        <div  class="col-sm-3 text-left mt-2">
                                            <input type="checkbox" name="is_all_day_open" value="1" class="largerCheckbox" id="checkall" onClick="check_uncheck_checkbox(this.checked);" @if($gym_data->is_all_day_open == 1)) checked @endif/>  <span class="allday">Open 24*7</span>
                                        </div>                  
                                    </div>
                                    <div class="col-sm-12" style=" margin-bottom: 13px;">
                                        <div  class="col-sm-3 text-left mt-2">
                                            <input type="checkbox" value="1" class="largerCheckbox" id="staff_hours" @if($gym_data->is_all_day_open != 1)) checked @endif onClick="check_uncheck_checkbox_second(this.checked);"/>  <span class="allday" >Open hours</span>
                                        </div>                 
                                    </div>
                                    @php $check = in_array("0", $days)? "checked" : "";  @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '0')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div  class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="0" class="flat-red" {{$check}}> Sunday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" id="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("1", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '1')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="1" class="flat-red" {{$check}}> Monday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("2", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '2')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="2" class="flat-red" {{$check}}> Tuesday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="  Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("3", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '3')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="3" class="flat-red" {{$check}}> Wednesday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("4", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '4')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="4" class="flat-red" {{$check}}> Thursday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("5", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '5')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing)); @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="5" class="flat-red" {{$check}}> Friday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    @php $check = in_array("6", $days)? "checked" : ""; @endphp
                                    @php $start_time = ''; $end_time = ''; @endphp
                                    @if($check != '')
                                        @foreach($gym_data->gym_time as $time)
                                            @if($time->gym_open_days == '6')
                                                @php $start_time = date('h:i A', strtotime($time->gym_open_timing));  @endphp
                                                @php $end_time =  date('h:i A', strtotime($time->gym_close_time)); @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    <div  class="col-lg-6 col-md-6 col-sm-12 daywrp text-left">                      
                                        <div class="col-lg-4 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="checkbox" name="gym_open_days[]" value="6" class="flat-red" {{$check}}> Saturday
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_open_timing[]" value="{{$start_time}}" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-lg-3 col-md-4 col-sm-12 timesetting mb-1">
                                            <input type="text" name="gym_close_time[]" value="{{$end_time}}" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                       
                                </div>
                                @if ($errors->has('gym_open_days'))
                                    <strong class="help-block"> {{ $errors->first('gym_open_days') }}</strong>
                                @endif
                                @if ($errors->has('gym_open_timing'))
                                    <strong class="help-block"> {{ $errors->first('gym_open_timing') }}</strong>
                                @endif
                                @if ($errors->has('gym_close_time'))
                                    <strong class="help-block"> {{ $errors->first('gym_close_time') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="col-sm-12" style=" margin-bottom: 13px;">
                                <div  class="col-md-12 col-sm-12">
                                    <input type="checkbox" value="1" name="is_staff_hours"  class="largerCheckbox" id="staff_hours" @if($gym_data->is_staff_hours == 1)) checked @endif onClick="check_uncheck_checkbox_staff(this.checked);"/>  <span class="allday" style="margin-left: 0px;">Additional Information(e.g. Ladies timings)</span>
                                </div>                 
                            </div>
                            <div  class="col-sm-12">
                                <textarea name="staff_hours" id="staff_hrs" class="form-control" placeholder="Additional Information">{{$gym_data->staff_hours}}</textarea>
                            </div>
                        </div>
                        <!--                        <div class="box-one-paym">
                                                    <div class="bank-det">
                                                        <h3 class="key-paym">Timing <span class="star">*</span></h3>
                                                    </div>
                                                    <div class="bank-det value-box-paym">
                                                        <input type="checkbox" name="is_all_day_open" value="1" class="value-paym" id="checkall" onClick="check_uncheck_checkbox(this.checked);" @if($gym_data->is_all_day_open == 1)) checked @endif/>  <span class="allday">Open 24*7</span><br>
                                                        <input type="checkbox" value="1" class="value-paym" id="staff_hours" @if($gym_data->is_all_day_open != 1)) checked @endif onClick="check_uncheck_checkbox_second(this.checked);"/>  <span class="allday" >Staff hours</span>
                                                    </div>
                                                    
                        
                                                </div>-->
                        <!--                    <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Additional Document Back <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym" id="fakeDiv4">
                                                    <input type="file" id="selectedFile4" name="additional_document_back" class="value-paym" placeholder="ABC547865" value="{{old('additional_document_back')}}">
                                                    <input type="text" id="fakeInput4" disabled/> 
                                                    <span onclick="document.getElementById('selectedFile4').click();" class="buttonImage" >Browse</span> 
                                                    @if ($errors->has('additional_document_back'))<br><span class="error" for="">{{ $errors->first('additional_document_back') }}</span>@endif
                                                </div>
                        
                                            </div>-->
                        <!--                    <div class="box-one-paym" style="width: 90%">
                                                <h4 class="key-paym-heading">External Account :</h4>
                                            </div>
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Account Holder Name <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="text" name="account_holder_name" class="value-paym" placeholder="Account Holder Name" value="{{old('account_holder_name')}}">
                                                    @if ($errors->has('account_holder_name'))<br><span class="error" for="">{{ $errors->first('account_holder_name') }}</span>@endif
                                                </div>
                                            </div> 
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Account Holder Type <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <select class="value-paym" name="account_holder_type">
                                                        <option value="individual">Individual</option>
                                                    </select>
                                                    @if ($errors->has('account_holder_type'))<br><span class="error" for="">{{ $errors->first('account_holder_type') }}</span>@endif
                                                </div>
                                            </div>  
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Routing Number <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="number" name="routing_number" class="value-paym" placeholder="Routing Number" value="{{old('routing_number')}}">
                                                    @if ($errors->has('routing_number'))<br><span class="error" for="">{{ $errors->first('routing_number') }}</span>@endif
                                                </div>
                                            </div> 
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Account Number <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="number" name="account_number" class="value-paym" placeholder="Account Number" value="{{old('account_number')}}">
                                                    @if ($errors->has('account_number'))<br><span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
                                                </div>
                                            </div>
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Currency <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <select class="value-paym" name="currency">
                                                        <option value="aud">AUD</option>
                                                    </select>
                                                    @if ($errors->has('currency'))<br><span class="error" for="">{{ $errors->first('currency') }}</span>@endif
                                                </div>
                                            </div>  
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                </div>
                                              </div>-->

                        <!--                    <div class="box-one-paym" style="width: 90%">
                                                <h4 class="key-paym-heading">Business Profile :</h4>
                                            </div>-->
                        <!--                    <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Business Name <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="text" name="name" class="value-paym" placeholder="Business Name" value="{{old('name')}}">
                                                    @if ($errors->has('name'))<br><span class="error" for="">{{ $errors->first('name') }}</span>@endif
                                                </div>
                                            </div> 
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Business MCC <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="number" name="mcc" class="value-paym" placeholder="Business MCC" value="{{old('mcc')}}">
                                                    @if ($errors->has('mcc'))<br><span class="error" for="">{{ $errors->first('mcc') }}</span>@endif
                                                </div>
                                            </div> 
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Support Email <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="email" name="support_email" class="value-paym" placeholder="Support Email" value="{{old('support_email')}}">
                                                    @if ($errors->has('support_email'))<br><span class="error" for="">{{ $errors->first('support_email') }}</span>@endif
                                                </div>
                                            </div> 
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Support Phone <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="number" name="support_phone" class="value-paym" placeholder="Support Phone" value="{{old('support_phone')}}">
                                                    @if ($errors->has('support_phone'))<br><span class="error" for="">{{ $errors->first('support_phone') }}</span>@endif
                                                </div>
                                            </div> 
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Support Url <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="text" name="support_url" class="value-paym" placeholder="Support Url" value="{{old('support_url')}}">
                                                    @if ($errors->has('support_url'))<br><span class="error" for="">{{ $errors->first('support_url') }}</span>@endif
                                                </div>
                                            </div>
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Business Url <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <input type="text" name="url" class="value-paym" placeholder="Business Url" value="{{old('url')}}">
                                                    @if ($errors->has('url'))<br><span class="error" for="">{{ $errors->first('url') }}</span>@endif
                                                </div>
                                            </div>
                        
                                            <div class="box-one-paym">
                                                <div class="bank-det">
                                                    <h3 class="key-paym">Product Description <span class="star">*</span></h3>
                                                </div>
                                                <div class="bank-det value-box-paym">
                                                    <textarea class="value-paym" name="product_description" placeholder="Product Description"></textarea>
                                                    @if ($errors->has('product_description'))<br><span class="error" for="">{{ $errors->first('product_description') }}</span>@endif
                                                </div>
                                            </div>-->


                        <div class="box_of_btn_save">
                            <button  type="submit" class="btn btn-danger save-btn-paym">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

    <script>
        $('#selectedFile').change(function () {
        var a = $('#selectedFile').val().toString().split('\\');
        $('#fakeInput').val(a[a.length - 1]);
        });
        $('#selectedFile2').change(function () {
        var a = $('#selectedFile2').val().toString().split('\\');
        $('#fakeInput2').val(a[a.length - 1]);
        });
        $('#selectedFile3').change(function () {
        var a = $('#selectedFile3').val().toString().split('\\');
        $('#fakeInput3').val(a[a.length - 1]);
        });
        $('#selectedFile4').change(function () {
        var a = $('#selectedFile4').val().toString().split('\\');
        $('#fakeInput4').val(a[a.length - 1]);
        });
    </script>
    @if(!empty($errors->has('')))
    <script type="text/javascript">
        setTimeout(function () {
        $('.error').hide();
        }, 4000);
        $('.booking-details-wrapper').removeClass("fadace");
        document.getElementById('formpopup').classList.add('show');
        $("#loader").hide();
    </script>
    @endif
    @if(Session::has('message'))
    <script type="text/javascript">
        $('.booking-details-wrapper').removeClass("fadace");
        $("#loader").hide();
    </script>
    @endif
    <script>
        document.getElementById("loader").style.display = "none";
        $('.save-btn-paym').click(function () {
        var myVar;
        $('.booking-details-wrapper').addClass("fadace");
        $("#loader").show();
        myVar = setTimeout(showPage, 9000);
        });
    </script>
    <script type="text/javascript">
        function check_uncheck_checkbox(isChecked) {
        if (isChecked) {

        $('input[name="gym_open_days[]"]').each(function() {
        $('input[name="gym_open_days[]"]').iCheck('uncheck');
        });
        } else {
        $('input[name="gym_open_days[]"]').each(function() {
        $('input[name="gym_open_days[]"]').iCheck('check');
        });
        }
        }

        function check_uncheck_checkbox_second(isChecked) {
        // alert('dd');
        if (isChecked) {
        $('input[name="gym_open_days[]"]').each(function() {
        $('input[name="gym_open_days[]"]').iCheck('check');
        });
        $('#checkall').iCheck('uncheck');
        } else {
        $('input[name="gym_open_days[]"]').each(function() {
        $('input[name="gym_open_days[]"]').iCheck('uncheck');
        });
        $('#checkall').iCheck('check');
        }
        }
    </script>
    <script type="text/javascript">
        function deleteimage(img_id){
            // alert(img_id);
            var isdel = confirm("Are you sure to delete this image?");
            if(!isdel)
                return false;
            $.ajax({
            type: "get",
                url: "{{url('delete-gym-image')}}/" + img_id,
                success: function(data){
                    location.reload();
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
        $('#gym_image').change(function(){
        //get the input and the file list
        var input = document.getElementById('gym_image');
        if (input.files.length > 5){
        $('.note').css('display', 'block');
        } else{
        $('.note').css('display', 'none');
        $("#gym_image").trigger('reset');
        }
        });
        });</script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=initMap"
    type="text/javascript"></script>

    <script type="text/javascript">
        //Function to covert address to Latitude and Longitude
        $('#btn').on('click', function(){
        var address = $('#gym_address').val();
        if (address == ''){
        alert('please enter address first');
        }
        else{
        var getLocation = function(address) {
        //alert(address);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        //  alert(latitude +"-----"+ longitude);
        $('#gym_latitude').val(latitude);
        $('#gym_longitude').val(longitude);
        //console.log(latitude, longitude);
        }
        });
        }
        }

        var address = $('#gym_address').val();
        getLocation(address);
        });</script>
    <script type="text/javascript">
        /*auto detect address and lat long*/
        function GetGeolocation() {

        navigator.geolocation.getCurrentPosition(GetCoords, GetError);
        }


        function GetCoords(position){

        document.getElementById('gym_longitude').value = position.coords.longitude;
        document.getElementById('gym_latitude').value = position.coords.latitude;
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
        var infowindow = new google.maps.InfoWindow({
        content: name
        });
        var add = results[0].formatted_address;
        document.getElementById('gym_address').value = add;
        console.log(results);
        }
        } else {
        alert("Geocoder failed due to: " + status);
        }
        });
        }
        function GetError(){
        aler("Fail to get location.");
        }
    </script> 
    @endsection
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\API\CanclesubscriptionController;
class CancleSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Daily:CancleSubscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Cancle Subdcription which based on expire plan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         $this->cancle_subscription = new CanclesubscriptionController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->cancle_subscription->auto_cancle_subscription();
        $this->info('Auto Cancle Subscription plan after expire!');
    }
}

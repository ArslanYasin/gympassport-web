<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rates;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Country;
class RateController extends Controller
{
    function __construct(){
        $this->rate = new Rates;
        $this->country = new Country;
    }

    public function index(){
     $data['country'] = $this->country->select('id','nicename')->where('id',13)->get();
     $data['rates'] = Rates::first();
      //echo '<pre>';print_r($data['rates']);die;
     return view('admin.rates.add_rate',$data);
   }

   public function store(Request $r){
   		 $validatedData = $r->validate([
            'country_id' => 'required|integer',
            'first_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'second_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'third_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'more_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);
   		 $input = $r->all();
   		 unset($input['_token']);
   		 if(Rates::create($input)){
				$msg = "Rates added successfully";
				Session::flash('msg',$msg);
				Session::flash('message','alert-success');
   		 }else{
				$msg = trans('lang_data.error');
				Session::flash('msg',$msg);
				Session::flash('message','alert-danger');
   		 }
   		 //echo '<pre>';print_r($input);die;
   		return redirect()->back();
   }

   public function update(Request $r,$id){
   		$validatedData = $r->validate([
            'country_id' => 'required|integer',
            'first_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'second_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'third_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'more_visit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);
   		 $input = $r->all();
   		 unset($input['_token']);
   		 if(Rates::whereid($id)->update($input)){
				$msg = "Rates added updated";
				Session::flash('msg',$msg);
				Session::flash('message','alert-success');
   		 }else{
				$msg = trans('lang_data.error');
				Session::flash('msg',$msg);
				Session::flash('message','alert-danger');
   		 }
   		 //echo '<pre>';print_r($input);die;
   		return redirect()->back();
   }
}

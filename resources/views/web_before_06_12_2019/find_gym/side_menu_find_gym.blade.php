@extends('web_user_dash.design')
@section('content')
	<section class="scc">
		<div class="container-fluid">
	        <div class="row">
                <div class="first-part">
							<div class="search-container">
								<input type="text" placeholder="Search your UFP Gym" name="search" class="placeholder"><span><i class="fa fa-search search-icon"></i></span>
							</div>


							 <div class="range">
							    <input type="range" min="0" max="5" steps="1" value="0">
							  </div>

							  <ul class="range-labels">
							    <li class="active selected"><span class="speed">20</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">40</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">60</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">80</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">100</span><span class="kil-meters">Km</span></li>
							  </ul>
						    
							<div class="map-coll-box-scroll">
							<div class="box-one">
								<div class="row">
									<div class="col-7 col-sm-7 col-md-7 col-lg-7">
										<div class="left-part">
										    <h2>Cross Fit</h2>
											<h3>Sector 63, Noida</h3>
											<button type="button" class="distance-button"><p>5 Km Away</p></button>
									   </div>
									</div>
									<div class="col-5 col-sm-5 col-md-5 col-lg-5">
										<div class="image-man"><img src="images/man-img-map-col.png" alt="man-image">
										</div>
									   <div class="view-det"><a href="map-coll-page3.html">View Details</a></div>
									   
									</div>
								</div>
							</div>
							<div class="box-two">
								<div class="row">
									<div class="col-7 col-sm-7 col-md-7 col-lg-7">
										<div class="left-part">
											<h2>ABC Gym</h2>
										    <h3>Sector 63, Noida</h3>
										    <button type="button" class="distance-button"><p>5 Km Away</p></button>
									    </div>
								    </div>
									<div class="col-5 col-sm-5 col-md-5 col-lg-5">
										<div class="right-part">
											<div class="image-man"><img src="images/man-img-map-col.png" alt="man-image">
											</div>
											<div class="view-det"> <a href="map-coll-page3.html">View Details</a>
											</div>
									    </div>
								   </div>
							    </div>
						   </div>
						</div>
							<div class="bar"><span></span>
							</div>
						  <div class="row">
								<div class="col-8 col-sm-8 col-md-8 col-lg-8">
								    <div class="result"><h6>Showing results 1-10</h6></div>
								</div>
							</div>
					       </div>
					       <div class="col-sm-4">
								<div class="right-side-mapview-gymdetails">
									<a href="map-coll-page1.html"><span><div class="lt-symbol">&lt</div></span></a>
								</div>
		    				</div>
            </div>
        </div>
    </section>
    <script>
    	$(document).ready(function(){
			$(".image").click(function(){
				$(".popup-paymentaa").toggle();
			});
			});

		$(document).ready(function(){
			$(".trigger-navbar").click(function(){
				$(".custom-navbar").slideToggle();
			});
			});
    </script>
@endsection
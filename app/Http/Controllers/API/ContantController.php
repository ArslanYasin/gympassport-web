<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Contantmanagment;
use Validator;
use DB;
use App\Http\Controllers\API\ApiCommanFunctionController;

class ContantController extends Controller
{
    public $successStatus = 200;

    function __construct() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
    }
    
    public function getContant(){
        $type = $this->form->type;
        $getdata = Contantmanagment::select('id','description','type')->where('type',$type)->first();
        if(!empty($getdata)){
            $res['id'] = (string)$getdata->id;
            $res['description'] = $getdata->description;
            $res['type'] = (string)$getdata->type;
            return $this->return->sendResponse(1, $res, 'Contant Management');
        }else{
           $allData = (object)[];
           return $this->return->sendResponse(0, $allData, 'Contant not available'); 
        }
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stafftiming extends Model
{
     protected $table ="staff_timing"; 
    
    protected $fillable = ['users_gym_id','gym_open_days','gym_open_timing','gym_close_time'];
}

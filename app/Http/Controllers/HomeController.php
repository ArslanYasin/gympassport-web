<?php

namespace App\Http\Controllers;

use App\CorporateUsers;
use App\User;
use App\Usergym;
use App\Uservisitedgym;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user = Landinguser::orderBy('id','DESC')->get();
        $data['total_gym_user'] = User::where('user_type', '3')->where('status', '1')->get()->count();
        $data['total_gym_owner'] = User::where('user_type', '2')->where('status', '1')->get()->count();
        $data['total_visit'] = Uservisitedgym::get()->count();
        $data['total_gym'] = Usergym::where('status', '1')->get()->count();
        return view('home', $data);
    }

    public function show()
    {
        $id = auth()->user()->id;
//        2489
        $data['total_active_user'] = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $id)->where('c.status', '1')->get()->count();
        $data['total_inactive_user'] = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $id)->where('c.status', '0')->get()->count();
        $data['total_users'] = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $id)->get()->count();

        $data['corporate_name'] = User::join('corporate_subscription as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $id)->pluck("corporate_name")->get(0);


        $result = DB::table('users')->select(DB::raw("p.plan_cat_name,c.expired_at"))
            ->join('usersubscriptions as c', 'c.user_id', '=', 'users.id')->join('subscriptions as s', 's.id', '=', 'c.subscription_id')
            ->join('plan_category as p', 'p.id', '=', 's.plan_name')
            ->where('users.id', $id)
            ->ORDERBy('c.id', 'DESC')->limit(1)->get();
        if ($result) {
            $data['corporate_plan'] = $result[0]->plan_cat_name;
            $data['expired_at'] = date_create_from_format('Y-m-d H:i:s', $result[0]->expired_at)->format('d-m-Y H:i:s');
        } else {
            $data['corporate_plan'] = '';
            $data['expired_at'] = '';
        }


        return view('corporate-home', $data);

    }
}

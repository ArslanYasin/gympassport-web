@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gym-Owner Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gym-List</li>
      </ol>
    </section>

  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-2" style="    margin-bottom: 5px;">
          <a href="{{route('add_gym')}}"><button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i>Create Gym-Owner</button></a>
        </div> 
        <div class="col-xs-12">
    <div class="nav-tabs-custom">
       <!-- Tabs within a box -->
       <ul class="nav nav-tabs pull-left" id="myTab">
          <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
          <li class="active"><a href="#Request" data-toggle="tab">Request</a></li>
          <li><a href="#Registered" data-toggle="tab">Registered</a></li>
       </ul> 
      <div class="tab-content no-padding">
         <div class="chart tab-pane active" id="Request" >        
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                  <th>Registered Owners</th>
                  <th>Registration date</th>
                  <th>Gym Name</th>
                  <th>Email</th>
                  <th>Phone number</th>
                  <th>Country</th>
                  <th>View Details</th>
<!--                  <th>Check-in's</th>-->
                  <th>Status</th>
                </tr>
                </thead>
              </table>
            </div>
        </div>
        <div class="chart tab-pane" id="Registered" >        
            <div class="box-body">
              <table id="example_registered" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Registered Owners</th>
                  <th>Registration date</th>
                  <th>Gym Name</th>
                  <th>Email</th>
                  <th>Phone number</th>
                  <th>Country</th>
                  <th>View Details</th>
                  <th>Check-in's</th>
                  <th>Status</th>
                  <th>Edit</th>
                </tr>
                </thead>
              </table>
            </div>
        </div>
      </div>
    </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });
  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('#myTab a[href="' + activeTab + '"]').tab('show');
  }
});
</script>
<script type="text/javascript">
     $(document).ready(function() {
      $('#request_list').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bSearchable_": true,
        "ordering"    : true,
        "sAjaxSource": "{{route('request_user_list',['type'=>'2'])}}",
        "aoColumns": [
              { mData: 'inc_id' }, 
              { mData: 'reg_owner' }, 
              { mData: 'registration_date' }, 
              { mData: 'gym_name' }, 
              { mData: 'email' }, 
              { mData: 'phone_number' }, 
              { mData: 'country' }, 
              { mData: 'view_detail' }, 
              //{ mData: 'check_in' }, 
              { mData: 'status' }, 
             // { mData: 'edit' }, 
             ]
      });  
  });
  
  $(document).ready(function() {
      $('#example_registered').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bSearchable_": true,
        "sAjaxSource": "{{route('gym_owner_list')}}",
        "aoColumns": [
              { mData: 'inc_id' }, 
              { mData: 'reg_owner' }, 
              { mData: 'registration_date' }, 
              { mData: 'gym_name' }, 
              { mData: 'email' }, 
              { mData: 'phone_number' }, 
              { mData: 'country' }, 
              { mData: 'view_detail' }, 
              { mData: 'check_in' }, 
              { mData: 'status' }, 
              { mData: 'edit' }, 
            ]
      });  
  });
</script>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection


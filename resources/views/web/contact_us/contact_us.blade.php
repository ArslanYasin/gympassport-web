@extends('web_user_dash.design')
@section('content')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <style type="text/css">
        /*.error{
          margin: 0px 0px 0px 139px;
          color: red;
        }*/
        /* .msg-box1 {
           color: red;
           justify-content: center;
           margin-bottom: 4px;
           margin-top: 10px;
           width: 100%;
           max-width: 97%;
       }*/
        .msg-box1 {
            color: red;
            font-family: Raleway;
            font-size: 14px;
            font-weight: 700;
            text-align: right;
        }

        .error {
            color: red;
            font-family: Raleway;
            font-size: 14px;
            font-weight: 700;
            text-align: right;
        }


    </style>
    @if(isset(Auth::user()->id))
        @if(Auth::user()->user_type==2)
            <style type="text/css">
                @media (max-width: 768px) {
                    .profile-paymentaa-second a {
                        padding: 27px 0px 14px 11px;
                    }
                }
            </style>
        @else
            <style type="text/css">
                @media (max-width: 768px) {
                    .profile-paymentaa-second a {
                        padding: 10px 0px 14px 11px;
                    }
                }
            </style>
        @endif
    @else
        <style type="text/css">
            .left-side-form {
                padding: 7px 26px 153px;
            }
        </style>
    @endif
    @if ($errors->has('name')||$errors->has('email')||$errors->has('contact'))
        <script>
            setTimeout(function () {
                $('.error').hide();
            }, 4000);
            setTimeout(function () {
                $('.msg-box1').hide();
            }, 4000);
        </script>
    @endif

    <?php
    // echo "<pre>"; print_r($_SERVER);
    //  echo $_SERVER['REQUEST_URI']; die;
    if($_SERVER['REQUEST_URI'] == '/contact-us/1'){
    ?>

    <style>
        @media (max-width: 992px) {
            .trigger-navbar {
                display: none !important;
            }

            @media (max-width: 992px) {
                .trigger-navbar-home-page {
                    display: none !important;
                }
            }
        }

        /*@media(max-width: 992px){
        .trigger-navbar {
          display: none !important;
        }

        }*/


    </style>
    <?php }?>

    <style>
        @media (max-width: 768px) {
            .mobile-reverse {
                display: flex;
                flex-direction: column-reverse;
            }
        }
    </style>
    <section class="main-wrapper">
        <h4 class="contact-main-heading">Contact Us</h4>
        <div class="container-fluid">
            <div class="mobile-reverse">
                <div class="col-lg-6 col-md-6 col-12 col-sm-12">
                @if(Session::has('message'))
                    <!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
                            </div> -->
                        @if(session('message')=='alert-danger')
                            <script type="text/javascript">
                                // alert('ee');
                                swal({
                                    title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'error'
                                });
                            </script>
                        @else
                            <script type="text/javascript">
                                //alert('ss');
                                swal({
                                    title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'success'
                                });
                            </script>
                        @endif
                    @endif
                    <form action="{{route('submit_contactus')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group left-side-form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="key">Enter your Name</h5>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" name="name" class="value" placeholder="Enter Name"
                                           value="@if(isset(Auth::user()->first_name)){{Auth::user()->first_name}} {{Auth::user()->last_name}}@endif">

                                    @if ($errors->has('name'))<p class="error"
                                                                 for="">{{ $errors->first('name') }}</p>@endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="key">Email Address</h5>
                                </div>
                                <div class="col-sm-6">
                                    <input type="email" name="email" class="value" placeholder="Enter email"
                                           value="@if(isset(Auth::user()->first_name)){{Auth::user()->email}}@endif">

                                    @if ($errors->has('email'))<p class="error"
                                                                  for="">{{ $errors->first('email') }}</p>@endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="key">Contact Number</h5>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" name="contact" class="value"
                                           placeholder="Enter contact number"
                                           value="@if(isset(Auth::user()->first_name)){{Auth::user()->phone_number}}@endif">

                                    @if ($errors->has('contact'))<p class="error"
                                                                    for="">{{ $errors->first('contact') }}</p>@endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="key">Message</h5>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="msg-box">
                                        <textarea rows="5" cols="5" name="message" class="query-class"
                                                  placeholder="Enter your query here...">{{old('message')}}</textarea>

                                </div>
                                <div class="msg-box1">

                                    @if ($errors->has('message')){{ $errors->first('message') }}@endif
                                </div>
                            </div>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LeZNEAaAAAAAE2iHazV4XL_BsaxzZHWVzM1EKDa"></div>
                        <br/>
                        <button type="submit" class="btn btn-primary send-query-btn"
                                style="margin-top: -10px;margin-bottom: 20px">Send Query
                        </button>
                    </form>

                </div>
                <div class="col-lg-6 col-md-6 col-12 col-sm-12 mb-4">
                    <div class="right-side-form">
                        @if(isset(Auth::user()->first_name))
                            @if(Auth::user()->user_type==2)
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h5 class="key key-right">Phone</h5>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="tel:+61 401 431 864"><label class="value" style="color: white; font-size: 24px">+61 401 431
                                                864</label></a>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="key key-right">Phone</h5>
                                </div>
                                <div class="col-sm-6">
                                    <a href="tel:+61 401 431 864"><label class="value" style="color: white; font-size: 24px">03424267060</label></a>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-6">
                                <h5 class="key key-right">Contact Email</h5>
                            </div>
                            <div class="col-sm-6">
                                @if(isset(Auth::user()->first_name))
                                    @if(Auth::user()->user_type==2)
                                        <label class="value email_value" style="cursor: pointer;color: white;">hello@gympassportpk.com</label>
                                    @endif
                                    @if(Auth::user()->user_type==3)
                                        <label class="value email_value_user" style="cursor: pointer;color: white;">hello@gympassportpk.com</label>
                                    @endif
                                @else
                                    <label class="value email_value_user" style="cursor: pointer; color: white;">hello@gympassportpk.com</label>
                                @endif
                            </div>


                            <!--<div class="row">
                              <div class="col-sm-6">
                                <h5 class="key key-right">Address</h5>
                              </div>
                              <div class="col-sm-6">
                                <textarea cols="1" rows="2" class="value value-second address-textarea" disabled>ABC Street, PQR Sector, XYZ City, AB State, Country</textarea>
                              </div>
                            </div>-->
                        </div>

                        <div class="col-sm-12">
                            @if(isset(Auth::user()->first_name))
                                @if(Auth::user()->user_type==2)
                                    <div id="googleMap_old" style="width:100%;height:317px;"></div>
                                @endif
                            @endif

                            <script>
                                $(document).ready(function () {
                                    $('.email_value').on('click', function () {
                                        window.location.href = "mailto:partner@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";
                                    });

                                    $('.email_value_user').on('click', function () {
                                        window.location.href = "mailto:support@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";
                                    });
                                });

                                function myMap() {
                                    var mapProp = {
                                        center: new google.maps.LatLng(-33.868820, 151.209290),
                                        zoom: 14,
                                        styles: [
                                            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                                            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                                            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                                            {
                                                featureType: 'administrative.locality',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#d59563'}]
                                            },
                                            {
                                                featureType: 'poi',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#d59563'}]
                                            },
                                            {
                                                featureType: 'poi.park',
                                                elementType: 'geometry',
                                                stylers: [{color: '#263c3f'}]
                                            },
                                            {
                                                featureType: 'poi.park',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#6b9a76'}]
                                            },
                                            {
                                                featureType: 'road',
                                                elementType: 'geometry',
                                                stylers: [{color: '#38414e'}]
                                            },
                                            {
                                                featureType: 'road',
                                                elementType: 'geometry.stroke',
                                                stylers: [{color: '#212a37'}]
                                            },
                                            {
                                                featureType: 'road',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#9ca5b3'}]
                                            },
                                            {
                                                featureType: 'road.highway',
                                                elementType: 'geometry',
                                                stylers: [{color: '#746855'}]
                                            },
                                            {
                                                featureType: 'road.highway',
                                                elementType: 'geometry.stroke',
                                                stylers: [{color: '#1f2835'}]
                                            },
                                            {
                                                featureType: 'road.highway',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#f3d19c'}]
                                            },
                                            {
                                                featureType: 'transit',
                                                elementType: 'geometry',
                                                stylers: [{color: '#2f3948'}]
                                            },
                                            {
                                                featureType: 'transit.station',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#d59563'}]
                                            },
                                            {
                                                featureType: 'water',
                                                elementType: 'geometry',
                                                stylers: [{color: '#17263c'}]
                                            },
                                            {
                                                featureType: 'water',
                                                elementType: 'labels.text.fill',
                                                stylers: [{color: '#515c6d'}]
                                            },
                                            {
                                                featureType: 'water',
                                                elementType: 'labels.text.stroke',
                                                stylers: [{color: '#17263c'}]
                                            }
                                        ]
                                    };
                                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                }
                            </script>

                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=myMap"></script>
                            <!-- AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU -->
                        <!-- <img src="{{ asset('website_file/images/contact-pg-img.png')}}" alt="image" class="img-fluid contact-pg-img"> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--    <section class="main-wrapper">--}}
    {{--        <h4 class="contact-main-heading">Contact Us</h4>--}}
    {{--        <div class="container-fluid">--}}
    {{--            <div class="mobile-reverse">--}}

    {{--                <div class="col-12 col-sm-12 col-md-6 col-lg-6">--}}
    {{--                @if(Session::has('message'))--}}
    {{--                    <!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">--}}
    {{--                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
    {{--                {!! session('msg') !!}--}}
    {{--                            </div> -->--}}
    {{--                        @if(session('message')=='alert-danger')--}}
    {{--                            <script type="text/javascript">--}}
    {{--                                // alert('ee');--}}
    {{--                                swal({--}}
    {{--                                    title: "{!! session('msg') !!}",--}}
    {{--                                    text: "",--}}
    {{--                                    icon: 'error'--}}
    {{--                                });--}}
    {{--                            </script>--}}
    {{--                        @else--}}
    {{--                            <script type="text/javascript">--}}
    {{--                                //alert('ss');--}}
    {{--                                swal({--}}
    {{--                                    title: "{!! session('msg') !!}",--}}
    {{--                                    text: "",--}}
    {{--                                    icon: 'success'--}}
    {{--                                });--}}
    {{--                            </script>--}}
    {{--                        @endif--}}
    {{--                    @endif--}}
    {{--                    <form action="{{route('submit_contactus')}}" method="post">--}}
    {{--                        {{ csrf_field() }}--}}
    {{--                        <div class="form-group left-side-form">--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <h5 class="key">Enter your Name</h5>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <input type="text" name="name" class="value" placeholder="Enter Name" value="@if(isset(Auth::user()->first_name)){{Auth::user()->first_name}} {{Auth::user()->last_name}}@endif">--}}

    {{--                                    @if ($errors->has('name'))<p class="error" for="">{{ $errors->first('name') }}</p>@endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <h5 class="key">Email Address</h5>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <input type="email" name="email" class="value" placeholder="Enter email" value="@if(isset(Auth::user()->first_name)){{Auth::user()->email}}@endif">--}}

    {{--                                    @if ($errors->has('email'))<p class="error" for="">{{ $errors->first('email') }}</p>@endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <h5 class="key">Contact Number</h5>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <input type="number" name="contact" class="value" placeholder="Enter contact number" value="@if(isset(Auth::user()->first_name)){{Auth::user()->phone_number}}@endif">--}}

    {{--                                    @if ($errors->has('contact'))<p class="error" for="">{{ $errors->first('contact') }}</p>@endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <h5 class="key">Message</h5>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-12">--}}
    {{--                                <div class="msg-box">--}}
    {{--                                    <textarea rows="5" cols="5" name="message" class="query-class" placeholder="Enter your query here...">{{old('message')}}</textarea>--}}

    {{--                                </div>--}}
    {{--                                <div class="msg-box1">--}}

    {{--                                    @if ($errors->has('message')){{ $errors->first('message') }}@endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="g-recaptcha" data-sitekey="6LeZNEAaAAAAAE2iHazV4XL_BsaxzZHWVzM1EKDa"></div>--}}
    {{--                        <br/>--}}
    {{--                        <button type="submit" class="btn btn-primary send-query-btn" style="margin-top: -10px;margin-bottom: 20px">Send Query</button>--}}
    {{--                    </form>--}}

    {{--                </div>--}}
    {{--                <div class="col-12 col-sm-12 col-md-6 col-lg-6">--}}
    {{--                    <div class="right-side-form">--}}

    {{--                        @if(isset(Auth::user()->first_name))--}}
    {{--                            @if(Auth::user()->user_type==2)--}}
    {{--                                <div class="row">--}}
    {{--                                    <div class="col-sm-6">--}}
    {{--                                        <h5 class="key key-right">Phone</h5>--}}
    {{--                                    </div>--}}
    {{--                                    <div class="col-sm-6">--}}
    {{--                                        <a href="tel:+61 401 431 864"><label class="value" style="color: white; font-size: 24px">+61 401 431 864</label></a>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            @endif--}}
    {{--                        @else--}}
    {{--                            <div class="row">--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <h5 class="key key-right">Phone</h5>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-sm-6">--}}
    {{--                                    <a href="tel:+61 401 431 864"><label class="value" style="color: white; font-size: 24px">03424267060</label></a>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        @endif--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-sm-6">--}}
    {{--                                <h5 class="key key-right">Contact Email</h5>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-sm-6">--}}
    {{--                                @if(isset(Auth::user()->first_name))--}}
    {{--                                    @if(Auth::user()->user_type==2)--}}
    {{--                                        <label class="value email_value" style="cursor: pointer;color: white;">hello@gympassportpk.com</label>--}}
    {{--                                    @endif--}}
    {{--                                    @if(Auth::user()->user_type==3)--}}
    {{--                                        <label class="value email_value_user" style="cursor: pointer; color: white;">hello@gympassportpk.com</label>--}}
    {{--                                    @endif--}}
    {{--                                @else--}}
    {{--                                    <label class="value email_value_user" style="color: white">hello@gympassportpk.com</label>--}}
    {{--                                @endif--}}
    {{--                            </div>--}}


    {{--                            <!--<div class="row">--}}
    {{--                              <div class="col-sm-6">--}}
    {{--                                <h5 class="key key-right">Address</h5>--}}
    {{--                              </div>--}}
    {{--                              <div class="col-sm-6">--}}
    {{--                                <textarea cols="1" rows="2" class="value value-second address-textarea" disabled>ABC Street, PQR Sector, XYZ City, AB State, Country</textarea>--}}
    {{--                              </div>--}}
    {{--                            </div>-->--}}
    {{--                        </div>--}}
    {{--                        <div class="col-sm-12">--}}
    {{--                            @if(isset(Auth::user()->first_name))--}}
    {{--                                @if(Auth::user()->user_type==2)--}}
    {{--                                    <div id="googleMap_old" style="width:100%;height:317px;"></div>--}}
    {{--                                @endif--}}
    {{--                            @endif--}}

    {{--                            <script>--}}
    {{--                                $(document).ready(function () {--}}
    {{--                                    $('.email_value').on('click', function () {--}}
    {{--                                        window.location.href = "mailto:partner@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";--}}
    {{--                                    });--}}

    {{--                                    $('.email_value_user').on('click', function () {--}}
    {{--                                        window.location.href = "mailto:support@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";--}}
    {{--                                    });--}}
    {{--                                });--}}

    {{--                                function myMap() {--}}
    {{--                                    var mapProp = {--}}
    {{--                                        center: new google.maps.LatLng(-33.868820, 151.209290),--}}
    {{--                                        zoom: 14,--}}
    {{--                                        styles: [--}}
    {{--                                            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},--}}
    {{--                                            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},--}}
    {{--                                            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},--}}
    {{--                                            {--}}
    {{--                                                featureType: 'administrative.locality',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#d59563'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'poi',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#d59563'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'poi.park',--}}
    {{--                                                elementType: 'geometry',--}}
    {{--                                                stylers: [{color: '#263c3f'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'poi.park',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#6b9a76'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road',--}}
    {{--                                                elementType: 'geometry',--}}
    {{--                                                stylers: [{color: '#38414e'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road',--}}
    {{--                                                elementType: 'geometry.stroke',--}}
    {{--                                                stylers: [{color: '#212a37'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#9ca5b3'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road.highway',--}}
    {{--                                                elementType: 'geometry',--}}
    {{--                                                stylers: [{color: '#746855'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road.highway',--}}
    {{--                                                elementType: 'geometry.stroke',--}}
    {{--                                                stylers: [{color: '#1f2835'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'road.highway',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#f3d19c'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'transit',--}}
    {{--                                                elementType: 'geometry',--}}
    {{--                                                stylers: [{color: '#2f3948'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'transit.station',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#d59563'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'water',--}}
    {{--                                                elementType: 'geometry',--}}
    {{--                                                stylers: [{color: '#17263c'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'water',--}}
    {{--                                                elementType: 'labels.text.fill',--}}
    {{--                                                stylers: [{color: '#515c6d'}]--}}
    {{--                                            },--}}
    {{--                                            {--}}
    {{--                                                featureType: 'water',--}}
    {{--                                                elementType: 'labels.text.stroke',--}}
    {{--                                                stylers: [{color: '#17263c'}]--}}
    {{--                                            }--}}
    {{--                                        ]--}}
    {{--                                    };--}}
    {{--                                    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);--}}
    {{--                                }--}}
    {{--                            </script>--}}

    {{--                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=myMap"></script>--}}
    {{--                            <!-- AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU -->--}}
    {{--                        <!-- <img src="{{ asset('website_file/images/contact-pg-img.png')}}" alt="image" class="img-fluid contact-pg-img"> -->--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--    </section>--}}
@endsection
@extends('web_user_dash.design')
@section('content')
<style type="text/css">
.password{
    width: 100%;
    max-width: 52%;
    color: #fff;
    font-size: 16px;
    font-weight: 600;
    text-align: left;
    margin: 0px auto;
    color: red;
}
</style>
	<section class="change-password-wrapper h-200">
			<div class="container">
				<div class="row m-0">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					@if(Session::has('message'))
							@if(session('message')=='alert-danger')
					          <script type="text/javascript">
					            swal({
					                  title : "{!! session('msg') !!}",
					                  text  :  "",
					                  icon  : 'error'
					                });
					          </script>
					         @else
					           <script type="text/javascript">
					            swal({
					                  title : "{!! session('msg') !!}",
					                  text  :  "",
					                  icon  : 'success'
					                });
					          </script>
					         @endif
						<!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! session('msg') !!}
						</div> -->
					@endif
					<form action="{{route('UserPassword')}}"  method="post"> 
						 {{ csrf_field() }}
					   <div class="main-part text-center">
					   	<h2 class="password-heading">Current Password</h2>
					     	<input type="password" name="current_password" class="placeholder-clr" placeholder="Enter your current password" value="{{ old('current_password') }}">
					     	   @if ($errors->has('current_password'))<label class="error password" for="">{{ $errors->first('current_password') }}</label>@endif
					   	<h2 class="password-heading">New Password</h2>
					   		<input type="password" name="new_password" class="placeholder-clr" placeholder="Enter new password" value="{{ old('new_password') }}">
					   		   @if ($errors->has('new_password'))<label class="error password" for="">{{ $errors->first('new_password') }}</label>@endif
					   	<h2 class="password-heading">Confirm New Password</h2>
					   		<input type="password" name="confirm_password" class="placeholder-clr" placeholder="Enter new password" value="{{ old('confirm_password') }}">
					   		   @if ($errors->has('confirm_password'))<label class="error password" for="">{{ $errors->first('confirm_password') }}</label>@endif
					   </div>
					   <button type="submit" class="btn btn-danger change-password-save-btn">Save</button>
					</form>
				    </div>
				</div>
			</div>
			
	</section>
	<script>
	 $(document).ready(function(){
            $(".trigger-navbar").click(function(){
                $(".custom-navbar").slideToggle();
            });
            });
	</script>
@endsection
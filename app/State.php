<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table ='state';
    protected $fillable = ['country_id','state','state_code'];
}

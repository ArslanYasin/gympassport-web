@extends('web_user_dash.design')
@section('content')
<style>
    .privacy{
    padding: 109px 0;
    min-height: 600px;
    height: auto;
    background-color: #383838;
    }
    strong {
/*    color: #ffffff;*/
    font-size: 19px;
    font-family: Raleway;
    font-weight: bold;
    line-height: 72px;
}
p {
    color: #ffffff;
    font-family: Raleway;
    font-size: 17px;
    font-weight: 500;
    line-height: 21px;
    text-align: justify;
}
@media(max-width: 992px){
    strong{
      font-size: 16px;
    }
    p{
      font-size: 15px;
    }
}
@media(max-width: 575px){
    strong{
      font-size: 14px;
    }
    p{
      font-size: 12px;
    }
}
    </style>

    <div class="privacy">
           <div class="container">
       @if(!empty($h_i_w))
           {!!$h_i_w->description!!}
       @endif
       </div>
   </div>

@endsection



<!-- <!DOCTYPE html>
<html>
<head>
  <title>How-Its-Work</title>
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">
  <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
    .privacy{
    padding: 109px 0;
    min-height: 600px;
    height: auto;
    }
    </style>
<body>
    <header class="header">
         <div class="container-fluid">
            <div class="icon-hm-pg">
                <a href="{{url('/')}}">
                    <img src="{{ asset('landing_page/images/logo.png')}}" alt="">
                </a>
            </div>
            <div class="custom-navbar-hm-pg">
                <ul>
                 
                    <li><a href="{{route('findgym')}}" target="_blank">Find Gyms</a></li>
                    <li><a href="{{route('subscriptionplan')}}" target="_blank">Pricing</a></li>
                </ul>
            </div>
           </div>       
    </header>

    <div class="privacy">
           <div class="container">
       @if(!empty($h_i_w))
           {!!$h_i_w->description!!}
       @endif
       </div>
   </div>>
    <footer class="footer">
        <div class="container-fluid">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{route('terms_conditions')}}" target="_blank">Terms & Conditions</a></li>
                <li><a href="{{route('privacy_policy')}}" target="_blank">Privacy Policy</a></li>
                 <li><a href="{{route('aboutus')}}" target="_blank">About Us</a></li>
                  <li><a href="{{route('contactus')}}" target="_blank">Contact Us</a></li>
                  <li><a href="{{route('faq')}}" target="_blank">FAQ</a></li>
                    <li><a href="{{route('becomepatner')}}" target="_blank">Partnerships</a></li>
                     <li><a href="{{route('subscriptionplan')}}" target="_blank">Membership</a></li>
                      <li><a href="{{route('findgym')}}" target="_blank">Gyms Near Me</a></li>
            </ul>
        </div>
    </footer>
</body>
</html -->
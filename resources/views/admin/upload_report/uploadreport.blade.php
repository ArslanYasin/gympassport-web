@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        Upload Reports
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
       
        <li class="active"> Upload Report</li>
       
        
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-2 col-xs-12 col-sm-8 col-md-8">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('savereport')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Select Gym<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control select2 @if($errors->has('gym_name')) help-block @endif" id="gym_name" name="gym_name">
                      @if(!empty($gym))
                        <option value="">Select Gym</option>
                        @foreach($gym as $key=>$value)
                          @php $get = $value['id']==old('gym_name') ? "selected" :"";@endphp
                           <option value="{{$value['id']}}" @php echo $get;@endphp><span >{{$value['gym_name']}} </span></option>
                        @endforeach
                      @else
                        <option value=""> Gym Unavailable</option>
                      @endif
                    </select>
                       @if ($errors->has('gym_name'))
                         <strong class="help-block"> {{ $errors->first('gym_name') }}</strong>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Upload File<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="file" class="form-control @if($errors->has('check_in_report')) help-block @endif" id="check_in_report" name="check_in_report">
                     @if ($errors->has('check_in_report'))
                         <strong class="help-block"> {{ $errors->first('check_in_report') }}</strong>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Upload</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
        <div class="row">
            <div class="col-sm-offset-2 col-xs-12 col-sm-8 col-md-8">
                <span style="font-size: 18px;"> Download:<a href="{{ asset('website_file/Upload_report.csv')}}" download>Template</a></span>
            </div>
        </div>
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder();
var address = "noida";

geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    alert(latitude);
  } 
}); 
</script>
@endsection


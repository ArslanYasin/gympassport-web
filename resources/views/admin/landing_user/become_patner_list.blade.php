@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Become Patner List
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Become-Patner</li>
      </ol>
    </section>

  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-2">
        </div> 
        <div class="col-xs-12">
                   
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Fitness Facility</th>
                  <th>Phone Number</th>
                  <th>Country</th>
                  <th>State</th>
                  <th>Postal Code</th>
                  <th>Suburb</th>
                  <th>Registered date</th>
                </tr>
              @if(!empty($user))
                @foreach($user as $key=>$value)
                <tr>
                  <td>{{$value->first_name}}</td>
                  <td>{{$value->last_name}}</td>
                  <td>{{$value->email}}</td>
                  <td>{{$value->fitness_facility}}</td>
                  <td>{{$value->phone_number}}</td>
                  <td>@if(!empty($value->country)){{$value->countryname->nicename}} @endif</td>
                  <td>@if(!empty($value->state)) {{$value->state_name['state']}} @endif</td>
                  <td>{{$value->postal_code}}</td>    
                  <td>{{$value->suburb}}</td>    

                  <td>{{date('d-m-Y',strtotime($value->created_at))}}</td>    
                </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
              @if(!empty($user))
                {{ $user->links() }}
              @endif
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection


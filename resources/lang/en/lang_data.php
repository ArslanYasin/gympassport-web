<?php

return [

    'successfully' => "Successfully",
    'success' => "Thank you for registering. We got you in our system, stay tuned for more updates! If you have not received email from us in a while, check your spam folder", 
    'error' => "Oops something went wrong",
    'delete' => "Record deleted successfully",
    'update' => "Record updated successfully",
    'found' => "Record already exits",
    'product_price' => "Product Price Added successfully",
    'content' => "Content submited successfully",
     'content_update' => "Content updated successfully",
     'subscription' => "Subscription created successfully",
     'sub_update' => "Subscription updated successfully",
     'matchpass' => "Your current password does not match",
     'pass_success' => "Password changed successfully",
     'gymadd' => "Gym owner created successfully",
     //'Secret_key'=>'sk_test_51HEXrOJ9zKoDIm5k1bpkmwrvPZ5QNKH3pIOgTulKLfI5v5MBHyxKpqk2pnCAKaOgQvJqrXcUIOfWB6QLPNijfnRg003zgLPeZ0',
     //'Publish_key'=>'pk_test_51HEXrOJ9zKoDIm5kYAbxVZLsFPcdqy9P1MvD8GpNw0oq1QHkRL7RN285UM3ROR7KdhpuEiubimJWjaF8pM4knGP900SBupyO9t',
     'Secret_key'=>'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',
     'Publish_key'=>'pk_live_51HEXrOJ9zKoDIm5kYjwSEyEWuZrGWJZPzcLpfpswP53VDkziNyfiCURbl1U6Z3QJr6HnNIkWjvLXKSWYDGbEZvTQ00PGjrqDVc',
     'gst'=>'10',
    'total_visit_pass'=>'20',
    'total_trail_pass'=>'1',

];

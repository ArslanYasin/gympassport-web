@if(!empty($billing_detail)) 
   @foreach($billing_detail as $key=>$value)
     <div class="billing-history-box">
        <div class="billing-history-wrapper">
          <h2>{{$value->plan_category->plan_cat_name}}</h2>
          <span class="amt-subscription-billing">{{$value->visit_pass}} visit Pass</span>
          <div class="purchased-box-billing">
            <h4>
              <span class="purchased-payment-billing">Purchased on:</span><span class="purchased-payment-date-billing">{{date('d/m/Y',strtotime($value->purchased_at))}}</span>
            </h4>
            <h4>
              <span class="purchased-payment-billing">Expired on:</span>
              <span class="purchased-payment-date-billing">{{date('d/m/Y',strtotime($value->expired_at))}}</span>
            </h4>
          </div>
        </div>
      </div>
  @endforeach
  <div id="page-selection"></div>
@else
   <div class="billing-history-box">
    <div class="billing-history-wrapper">
      <h2>Billing detail not available</h2>
    </div>
  </div>
@endif

<script src="{{ asset('website_file/jquery.bootpag.min.js')}}"></script>
<?php  $total_pages = ceil($total_rows / $limit); if($total_pages > 1): ?>
<?php endif; ?>
<script>         
  var numval = localStorage.getItem("bill_val");
  if(numval != null){
    var nval = numval;
  }else{
    var nval = 1;
  } 
  $('#page-selection').bootpag({
    total: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    page: nval,
    maxVisible: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    firstLastUse: true,
    // first: '←',
    // last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    // nextClass: 'next',
    // prevClass: 'prev',
    // lastClass: 'last',
    // firstClass: 'first'
  }).on("page", function(event, bill_val){
      localStorage.setItem("bill_val", bill_val);
      $.ajax({
      type: 'get',
      url: "{{route('billing_histroy')}}",
      data: {"bill_val" : bill_val},
      beforeSend:function(){
      },
      complete:function(){
      },
      }).done(function(data) {
      $("#billing_detail").html(data);
         localStorage.setItem("bill_val",1);

      });
  });
</script>
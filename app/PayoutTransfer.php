<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutTransfer extends Model
{
    protected $table = 'payout_transfer';
    protected $fillable = ['gym_partner_id','transfer_id','balance_transaction_id','amount_transfer','transfer_destination'
,'destination_payment','reversals_url','transfer_group'];
}

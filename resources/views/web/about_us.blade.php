<!DOCTYPE html>
<html>
<head>
  <title>About-us</title>
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">
  <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<style>
    .privacy{
    padding: 109px 0;
    min-height: 600px;
    height: auto;
    background-color: #000000;
    }
    .privacy p{
    text-align: center;
    color: #fff;
}
.privacy p strong {
    font-size: 31px;
    font-weight: bold;
    color: #fff;
  }
  .privacy p {
    font-size: 19px;
  }
    </style>
<body>
    <header class="header">
         <div class="container-fluid">
            <div class="icon-hm-pg">
                <!-- <img src="images/logo.png" alt="icon" class="logo-image img-fluid"> -->
                <a href="{{url('/')}}">
                    <img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="">
                </a>
            </div>
            <div class="custom-navbar-hm-pg">
                <ul>
                 
{{--                    <li><a href="{{route('findgym')}}" >Find Gyms</a></li>--}}
<!--                    <li><a href="{{route('h_i_w')}}">How It Works</a></li>-->
<!--                    <li><a href="{{route('subscriptionplan')}}" >Pricing</a></li>-->
                    <li><a href="{{route('becomepatner')}}">Become a Partner</a></li>
{{--                     <li><a href="{{route('singup')}}">Sign Up</a></li> --}}
                 <li><a href="{{route('singin')}}">Login</a></li>
                </ul>
            </div>
           </div>       
    </header>

    <div class="privacy">
           <div class="container">
       @if(!empty($about_us))
           <?php echo $about_us->description?>
       @endif
       </div>
   </div>


    <!-- Footer started here -->
    <footer class="footer">
        <div class="container-fluid">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{route('terms_conditions')}}" >Terms & Conditions</a></li>
                <li><a href="{{route('privacy_policy')}}" >Privacy Policy</a></li>
                <li><a href="{{route('refund_policy')}}" >Refund Policy</a></li>
                <li><a href="{{route('aboutus')}}" >About us</a></li>
                <li><a href="{{route('contactus')}}" >Contact us</a></li>
                <li><a href="{{route('faq')}}" >FAQ</a></li>
                <li><a href="{{route('becomepatner')}}" >Partnerships</a></li>
            <!--                    <li><a href="{{route('subscriptionplan')}}" >Membership</a></li>-->
{{--                <li><a href="{{route('findgym')}}" >Gyms Near Me</a></li>--}}
            </ul>
        </div>
    </footer>
   
</body>
</html>
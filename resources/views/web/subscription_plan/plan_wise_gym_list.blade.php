@extends('web_user_dash.design')
@section('content')
<section class="popup-section">
  
 <div class="container-fluid">
 <div class="popup">
  <div class="cross-btn"><p><!-- X --></p></div>
  <div class="popup-box-heading">
    <h4>{{$plan_name}}</h4>

  </div>
  <!-- <i class="fa fa-close close-btn"></i> -->

  <div class="search-wrapper">
       <div class="wrap">
        <div class="search">
        <input type="text" name="searchdata" id="searchdata" onkeyup="keyupfunction();" class="searchTerm" placeholder="Search GYM Name">
        <button type="button" id ="search" class="searchButton" onclick="search();">
                  </button>
        </div>
       </div>
    </div>
   
  <div class="row row-fst" id="gym_list">
    
   <!--  <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div>
    <div class="card-box">
      <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
      <h3 class="gym-name">Cross Fit Gym</h3>
      <div class="address-box">
        <p>PQR Sector,</p>
        <p>ABC Street,</p>
        <p>Noida, UP, India</p>
      </div>
    </div> -->
  </div>
 </div>
</div>
</section>
<style type="text/css">
  .msgcolor{
      margin-top: 29px;
      margin-bottom: 11px;
      font-size: 23px;
      font-weight: bold;
      color: red;
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    var plan_id = "{{$plan_id}}";
    var search  = '';
         $.ajax({
            url:"{{route('ajax_plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              //alert(data);
              $('#gym_list').html(data);
              //$('#myModal').modal('show');
            }
        });
    });
  </script>
  <script type="text/javascript">
      function search(){
      var search = $('#searchdata').val();
       var plan_id = "{{$plan_id}}";
       $.ajax({
            url:"{{route('ajax_plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
    }
  </script>
  <script type="text/javascript">
    function keyupfunction(){
      var search = $('#searchdata').val();
      var plan_id = "{{$plan_id}}";
       $.ajax({
            url:"{{route('ajax_plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
    }
  </script>
  <script type="text/javascript">
    function open_find_gym(gym_id){
      if(gym_id){
        localStorage.setItem("gym_id", gym_id);
        //alert(gym_id);
         window.location.href = '{{route("findgym")}}';
      }
      

    }
  </script>
<script>
   $(document).ready(function(){
       $(".image").click(function(){
           $(".popup-paymentaa").toggle();
       });
       });
   
   $(document).ready(function() {
       $(".trigger-navbar").click(function() {
           $(".custom-navbar").slideToggle();
       });
   });
</script>
@endsection
@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User Listing</a></li>
            <li class="active">User profile</li>
        </ol>
    </section>
    <style type="text/css">
        .timeline:before {
            display: none !important;
        }
        .timeline>li>.timeline-item {
            margin-left: 15px !important;
        }
        .first-box{
            width: 28%;
            padding: 0px 2px 11px 10px;
        }
        .second-box{
            width: 72%;
        }
        a.pull-right.value {
            margin: -1px 4px;
        }
        b.key {
            width: 100%;
        }
        @media(max-width: 768px){
            .first-box{
                width: 100%;
            }
            .second-box{
                width: 100%;
            }
        }
    </style>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            @if(Session::has('message'))
            <div class="alert msg_show @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible" style="width: 50%; margin-left: 7px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif 
            <div class="col-md-3 first-box">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{$url}}{{$user_details['user_image']}}" alt="@if(!empty($user_details['first_name'])){{$user_details['first_name']}} {{$user_details['last_name']}}@endif">
                        <i class="fa fa-pencil-square fontsize" data-toggle="modal" data-target="#myModal" aria-hidden="true"></i> 
                        <h3 class="profile-username text-center">
                            @if(!empty($user_details['first_name']))
                            {{$user_details['first_name']}}
                            @endif
                            @if($user_details['last_name'])
                            {{$user_details['last_name']}}
                            @endif
                        </h3>
                        @if($user_details['status']==1)
                        <p class="text-muted text-center"> <span class="label label-success">Active User</span></p>
                        @endif
                        @if($user_details['status']==0)
                        <p class="text-muted text-center"> <span class="label label-danger">Inactive User</span></p>
                        @endif
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b class="key">User Id</b> <a class="pull-right value">{{$user_details['user_ufp_id']}}</a>
                            </li>
                            <li class="list-group-item">
                                <b class="key">First Name </b> <a class="pull-right value">@if(!empty($user_details['first_name'])){{$user_details['first_name']}}@endif</a>
                            </li>
                            <li class="list-group-item">
                                <b class="key">Last Name</b> <a class="pull-right value"> @if($user_details['last_name']){{$user_details['last_name']}} @endif</a>
                            </li> 
                            <li class="list-group-item">
                                <b class="key">Email</b> <a class="pull-right value"> @if($user_details['email']){{$user_details['email']}} @endif</a>
                            </li>
                            <li class="list-group-item">
                                <b class="key">Date of birth</b> <a class="pull-right value"> @if($user_details['d_o_b'])
                                    {{date('d/m/Y',strtotime($user_details['d_o_b']))}} @endif</a>
                            </li> 
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">About Me</h3>
                    </div>
                    <div class="box-body">
                        <strong><i class="fa fa-globe margin-r-5"></i> Country</strong>

                        <p class="text-muted">
                            @if($user_details['countryname']['nicename'])
                            {{$user_details['countryname']['nicename']}}
                            @endif
                        </p>

                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i>State</strong>

                        <p class="text-muted">
                            @if($user_details['state_name']['state'])
                            {{$user_details['state_name']['state']}} 
                            @endif
                        </p>
                        <hr>

                        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                        <p class="text-muted">@if($user_details['city'])
                            {{$user_details['city']}}
                            @endif,
                            @if($user_details['address'])
                            {{$user_details['address']}} @endif,
                            @if($user_details['postal_code'])
                            {{$user_details['postal_code']}}
                            @endif</p>
                        <!-- 
                                      <hr>
                        
                                      <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                        
                                      <p>
                                        <span class="label label-danger">UI Design</span>
                                        <span class="label label-success">Coding</span>
                                        <span class="label label-info">Javascript</span>
                                        <span class="label label-warning">PHP</span>
                                        <span class="label label-primary">Node.js</span>
                                      </p>
                        
                                      <hr>
                        
                                      <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                        
                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p> -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9 second-box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Subscription" data-toggle="tab">Subscription details</a></li>
                        <li><a href="#checkin" data-toggle="tab">Check in details</a></li>
                        <li><a href="#download_report" data-toggle="tab">Download Report</a></li> 
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="Subscription">
                            <ul class="timeline timeline-inverse">
                                @if(!empty($subscription_details))
                                @foreach($subscription_details as $data)
                                <li>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('d/m/Y h:m a',strtotime($data->purchased_at))}}</span>
                                        @if($data->status=='1')
                                        <h3 class="timeline-header"><a href="#"><!-- Active subscription plan --> {{$data->plan_category->plan_cat_name}} </a>  <span class="label label-success">Active Plan</span></h3>
                                        @elseif($data->status=='2')
                                        <h3 class="timeline-header"><a href="#">Upcoming subscription plan {{$data->plan_category->plan_cat_name}} </a>  <span class="label label-warning">Upcoming Plan</span></h3>
                                        @else
                                        <h3 class="timeline-header"><a href="#">{{$data->plan_category->plan_cat_name}} <!-- {{$data->plan_time}} Pass. --></a>  <span class="label label-danger">Expired Plan</span></h3>
                                        @endif
                                        <div class="timeline-body">
                                            <b>Amount paid for subscription</b> : {{$data->amount}} $ <br>
                                            <b>Purchased on</b> : {{date('d/m/Y h:m a',strtotime($data->purchased_at))}}<br>
                                            <b>Visit Pass</b> : {{$data->visit_pass}} Visit<br>
                                            <b>Plan duration </b>: {{$data->plan_duration}} {{$data->plan_time}}
                                            @if($data->status=='0')
                                            <br>
                                            <b>Plan expired </b>: {{date('d/m/Y h:m a',strtotime($data->expired_at))}}
                                            @endif
                                        </div>

                                        <div class="timeline-body">
                                            <b>Plan description</b> : {{$data->description}}
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="checkin">
                            <ul class="timeline timeline-inverse">
                                @if(!empty($check_in_details))
                                @foreach($check_in_details as $data)
                                <li>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{date('d/m/Y h:m a',strtotime($data->user_check_in))}}</span>
                                        <h3 class="timeline-header"><a href="#"> {{$data->gym_name->gym_name}} </a> </h3>
                                        <div class="timeline-body">
                                            <b>Address</b> : {{$data->gym_name->gym_address}} Visit<br>
                                            <b>Check-In </b>: {{date('d/m/Y h:m a',strtotime($data->user_check_in))}}<br>
                                            @php
                                            if($data->plan_detail->visit_pass=='month'){
                                            $pass = 'Monthly infinity pass';
                                            }else{
                                            $pass = $data->plan_detail->visit_pass.' Visited Pass';
                                            }

                                            @endphp
                                            <b>Plan </b>:  {{$data->plan_detail->plan_category->plan_cat_name}} ({{$pass}})
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                @endif
                            </ul>

                        </div>

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="download_report">
                            <ul class="timeline timeline-inverse">

                                <li>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select name="user_report" class="form-control" id="user_report">
                                                <option value="">Choose options</option>
                                                <option value="{{trans('constants.last_week')}}">Last Week</option>
                                                <option value="{{trans('constants.last_month')}}">Last Month</option>
                                                <option value="{{trans('constants.last_six_month')}}">Last 6 Month</option>
                                                <option value="{{trans('constants.last_year')}}">Last Year</option>
                                            </select>
                                            <span>Note: <span style="color:red;">Last week,month,year calculate is from current date.</span></span>
                                        </div>
                                        <h4 class="timeline-header" id="check_option">
                                            <a href="{{route('download_user_report')}}"> Download Report</a> 
                                        </h4>
                                       
                                    </div>
                                </li>

                            </ul>

                        </div>
                        <!-- /.tab-pane -->

                        <!--  <div class="tab-pane" id="settings">
                 
                         </div> -->
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Profile Image</h4>
            </div>
            <div class="modal-body">



                <form action="{{route('update_image',['id'=>$user_details['id']])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Profile-Image<span class="star">*</span></label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" id="image" name="image" value="" placeholder="" required>
                                @if ($errors->has('image'))
                                <strong class="star"> {{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div style="text-align: center;">
                        <button type="submit" class="btn btn-info">Update</button>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>  <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<style type="text/css">
    .fontsize{
        font-size: 23px;
        text-align: center;
        display: block;
        margin-right: -42px;
        margin-top: -26px;
        cursor: pointer;
    }
    .star{
        color: red;
    }
</style>
<script>
    $(document).ready(function(){
       $('#check_option').click(function(){
           if($('#user_report').val() !=''){
               return true;
           }else{
               alert('Please chooes option to download report');
               return false;
           }
       }) 
       
       $('#user_report').change(function(){
           var report_val = $('#user_report').val();
           var user_id = "{{$user_id}}";
           if(report_val !=''){
                    $.ajax({
                     url:"{{route('set_user_report')}}",
                     data: {'report_val':report_val, 'user_id':user_id, '_token': "{{ csrf_token() }}"},
                     type: "post",
                     success: function(data){
                           // alert(data);
                     }
                    });
           }else{
               
           }           
       });
    });
</script>
@endsection


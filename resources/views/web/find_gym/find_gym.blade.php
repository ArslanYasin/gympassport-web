@extends('web_user_dash.design')
@section('content')
<style type="text/css">
   .list-view {
    position: fixed;
    top: 30px;
}
.modal-backdrop {   
      display: none;    
}
.image-man img {
    margin-top: -14px;
    margin-left: -5px;
    height: 74px;
    width: 91px;
}
.modal-content.content-of-modal-custom {
    margin: 0 auto;
    margin-top: 172px;
}

</style>

         <section class="section-map-coll-gymdet" >
            <div id="ajax_find_gym_data">

            </div>

             <div class="container-fluid">

             </div>
             

            <div class="list-view">
               <button class="btn-list-view" onclick="openNav()">List View<span class="gt-symbol">&gt</span></button>
            </div>
            
{{--             <div class="list-view" style="right:0px;width: 300px;height: 80px">--}}
{{--                 <input type="checkbox" id="lite" name="lite" value="1" style="display: inline-block">--}}
{{--                 <label for="lite"> Gym Passport - Lite </label><br>--}}
{{--                 <input type="checkbox" id="pro" name="pro" value="2" style="display: inline-block">--}}
{{--                 <label for="pro"> Gym Passport - Pro </label>--}}
{{--             </div>--}}
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn cross" onclick="closeNav()">&lt <span class="map_class">map</span></a> 
               <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                  <div class="first-part">
                     <div class="search-container">
                        <input type="text" placeholder="Please enter Gym name" name="search" class="placeholder" onkeyup="serchdata(this.value);" id="search"><span><i class="fa fa-search search-icon"></i></span>
                     </div>
                     <!-- <div class="range">
                        <input type="range" min="1" max="7" steps="1" value="1">
                     </div>
                     <ul class="range-labels">
                        <li class="active selected"><span class="speed-num">20</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">40</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">60</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">80</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">100</span><span class="kms"> km</span></li>
                     </ul> -->
                     <div class="map-coll-box-scroll" id="ajax_find_side_gym_list">
                        @if(!empty($side_gym_data))
                           @foreach($side_gym_data as $key=>$value)
                              @if($value['gym_cat_id'] == '1')
                            <div class="box-one class_blue">
                            @elseif($value['gym_cat_id'] == '2')
                            <div class="box-one class_green">
                            @elseif($value['gym_cat_id'] == '3')
                            <div class="box-one class_yellow">
                            @elseif($value['gym_cat_id'] == '4')
                            <div class="box-one class_orange">
                            @elseif($value['gym_cat_id'] == '5')
                            <div class="box-one class_red">
                            @else
                              <div class="box-one">
                            @endif
                                 <div class="row">
                                    <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                                       <div class="left-part">
                                          <h2>{{$value['gym_name']}}</h2>
                                          <h3>{{substr($value['gym_address'],0,30)}}</h3>
                                          <button type="button" class="distance-button">
                                             <p>{{number_format((float) $value['distance'], 1, '.', '')}} Km Away</p>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="col-5 col-sm-5 col-md-5 col-lg-5">
                                       <div class="image-man"><img src="{{trans('constants.image_url')}}{{$value['gym_logo']}}" alt="man-image">
                                       </div>
                                       <div class="view-det"><a href="#" onclick="getGymdetail('{{$value["gym_id"]}}')">View Details</a>
                                       </div>
                                      
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                        @else
                           <div class="box-one">
                              <div class="row">
                                  <h2>Unavailable</h2>
                              </div>
                           </div>
                        @endif
                       <!--  <div class="box-one">
                           <div class="row">
                              <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                                 <div class="left-part">
                                    <h2>ABC Gym</h2>
                                    <h3>Sector 63, Noida</h3>
                                    <button type="button" class="distance-button">
                                       <p>5 Km Away</p>
                                    </button>
                                 </div>
                              </div>
                              <div class="col-5 col-sm-5 col-md-5 col-lg-5">
                                 <div class="image-man"><img src="{{ asset('website_file/images/man-img-map-col.png')}}" alt="man-image">
                                 </div>
                                 <div class="view-det" data-toggle="modal" data-target="#myModal"><a href="#">View Details</a>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                        <div class="bar">
                           <span></span>
                        </div>
                     </div>
                     <div class="row">
                     <!--    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                           <div class="result">
                              <span>
                                 <h6>Showing results 1-10</h6>
                              </span>
                              <span>
                                 <div class="two-arrow">
                                    <span><a href="#">&lt</a></span>
                                    <span><a href="#">&gt</a></span>
                                 </div>
                              </span>
                           </div>
                        </div> -->
                        <div class="map-photo">
                           <img src="{{ asset('website_file/images/contact-pg-img.png')}}" class="img-fluid">
                        </div>
                       
                        <div class="modal fade" id="myModal" role="dialog">
                           <div class="modal-dialog" id="id01">
                              <!-- Modal content-->
                           </div>
                        </div>
                       
                     </div>
                  </div>
               </div>
            </div>
             
             <!-- Modal -->
    <div class="container">
        <div id="myModalfirst" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content content-of-modal-custom">
                    <div class="modal-header header-of-modal-custom">
                        <button type="button" class="close modal-close-btn-custom" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body body-of-modal-custom">
                          <h4 class="login-act-heading">Please login account</h4>
                        <div class="content">
                            <div class="form loginBox">
                                 <p id="msg_error" class="error_gym"></p>
                                <form method="post" action="" accept-charset="UTF-8">
                                  <div class="custom-input-fields-box">
                                    <input id="email" class="form-control modal-custom-input-fields" type="text" placeholder="Email" name="email" required>
                                    <p id="email_error" class="error_gym"></p>
                                    <input id="password" class="form-control modal-custom-input-fields" type="password" placeholder="Password" name="password" required>
                                    <p id="password_error" class="error_gym"></p><br>
                                </div>
                                    <div class="submit-btn-login-box">
                                    <input class="btn btn-default btn-login-modal" type="button" id="submit_form" value="Login">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
         </section>
<!-- <script>
let myImg=document.querySelector('#myImg');
 let myModal9=document.querySelector('#myModal9');
 let closeModalBtn=document.querySelector('#closeModalBtn');
 let forModalHide=document.querySelector('#forModalHide');
 

 myImg.addEventListener('click',function(){
    myModal9.style.display='block'
})

 forModalHide.addEventListener('click',function(){
   console.log('forModalHide')
})


</script> -->
  <style type="text/css">
    .map_image{
      width: 104px;height: 72px;border-radius: 9px;
    }
    .map_button{

    }
    .main_gym {
    width: 285px;
}
    .gym_unv{
      color: #fff;
    text-align: center;
    margin: 0 auto;
    font-family: Raleway;
    font-size: 23px;
    }
    .error_msg{
      color: red;
      font-weight: bold;
    }
    .success_msg{
      color: green;
      font-weight: bold;
    }
  </style>
  <style>
    .loginBox input{
        margin-bottom: 12px;
    }
    .error_gym{
        color:red;
        display: block !important;
       
    }
</style>
<script type="text/javascript">
  if (navigator.geolocation) {
    var search_data = localStorage.getItem("search_gym");
    var gym_id = localStorage.getItem("gym_id");
    if(search_data == null && gym_id == null){
      navigator.geolocation.getCurrentPosition(showPosition);
    }
    
  }
   function showPosition(position) {
       lat = position.coords.latitude
       long =  position.coords.longitude;
       //console.log(lat + ', ' + long);
       localStorage.setItem("local_lat", lat);
       localStorage.setItem("local_long", long);
       $.ajax({
            url:"{{route('get_lat_long')}}",
            data: {'latitude':lat, 'longitude':long, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              if (window.location.href.indexOf('reload')==-1) {
                         window.location.replace(window.location.href+'?reload');
                    }                   
            }
        });
   }
 
</script>
<script type="text/javascript">
$( document ).ready(function() {
	var search_data = localStorage.getItem("search_gym");
	if(search_data == null || search_data ==''){
    return ;
  }else{
    //alert(search_data);
		openNav();
		serchdata(search_data);
		$('#search').val(search_data);
		  var search = $('#search').val();
            $.ajax({
            url:"{{route('ajax_find_side_gym_list')}}",
            data: {'search':search,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
               $('#ajax_find_side_gym_list').html(data);
                localStorage.removeItem('search_gym');
            }
           });
	}

});
</script>
<script type="text/javascript">
$( document ).ready(function() {
  var gym_id = localStorage.getItem("gym_id");
  if(gym_id !=''){
      getGymdetail(gym_id);
      localStorage.removeItem('gym_id');
  }
});
</script>
  <script>
      function openmodel(){
          //alert('hello');
            $(".error_gym").attr("style", "display: none !important");
            $('#myModalfirst').modal('show');
      }
  </script>
  <script>
     
  </script>
         <script>
            $(document).ready(function() {
                $(".image-map-coll-page").click(function() {
                    $(".popup-paymentaa-second").toggle();
                });
            });
            
             $(document).ready(function() {
                $(".trigger-navbar").click(function() {
                    $(".custom-navbar-map-coll-page").slideToggle();
                });
            });
            
            //24/01/2020 
            function hideBox(){
                //alert('dfkjhk');
                $('#myModal').modal('toggle');
                openNav(); 
            }
          //24/01/2020 end
            function openNav() {
                if (window.matchMedia('(max-width: 400px)').matches)
                {
                   document.getElementById("mySidenav").style.width = "300px";
                   $('#mySidenav').css('display', 'block');
                   
            
                }
                else{
                     document.getElementById("mySidenav").style.width = "400px";
                     $('#mySidenav').css('display', 'block');
                }
            
                $('.cross').css('display', 'block');
            }
            
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
                $('#mySidenav').css('display', 'none');
                $('.cross').css('display', 'none');
            }
            $('#mySidenav').css('display', 'none');

            
            
            $('#slider1').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                dots: true,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                }
            })

            
            var sheet = document.createElement('style'),
                $rangeInput = $('.range input'),
                prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];
            
            document.body.appendChild(sheet);
            
            var getTrackStyle = function(el) {
                var curVal = el.value,
                    val = (curVal - 1) * 16,
                    style = '';
                // console.log(val);
            
                // Set active label
                $('.range-labels li').removeClass('active selected');
            
                var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');
            
                curLabel.addClass('active selected');
                curLabel.prevAll().addClass('selected');
            
                // Change background gradient
                for (var i = 0; i < prefs.length; i++) {
                    // style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
                    style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #be0027 0%, #be0027 ' + val + '%, #383838 ' + val + '%, #383838 100%)}';
                }
            
                return style;
            }
            
            $rangeInput.on('input', function() {
                sheet.textContent = getTrackStyle(this);
            });
            
            // Change input value on label click
            $('.range-labels li').on('click', function() {
                var index = $(this).index();
            
                $rangeInput.val(index + 1).trigger('input');
            
            });
         </script>

<script type="text/javascript">
   $(document).ready(function(){
         var search = '';
         $.ajax({
            url:"{{route('ajax_find_gym')}}",
            data: {'search':search,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
               // console.log(data)
               $('#ajax_find_gym_data').html(data)
            }
        });

        $("#lite,#pro,#standard").on("click",function(){
          var search = $(this).val();
          if(search == 1){
            $("#standard").prop("checked", false);
            $("#pro").prop("checked", false);
          }else if(search == 2){
            $("#lite").prop("checked", false);
            $("#pro").prop("checked", false);
          }else{
              $("#lite").prop("checked", false);
              $("#standard").prop("checked", false);
          }
          if(search == 1 || search == 2 || search == 3){
            $.ajax({
                url:"{{route('ajax_find_gym')}}",
                data: {'search':search,'plan':1,'_token': "{{ csrf_token() }}"},
                type: "post",
                success: function(data){
                   // console.log(data)
                   $('#ajax_find_gym_data').html(data)
                }
            });
          }
        });

   });
</script>

<script type="text/javascript">
   function serchdata(search){
       var search = search;
        $("#pro").prop("checked", false);
        $("#lite").prop("checked", false);
       $("#standard").prop("checked", false);
       //alert(search);
         $.ajax({
            url:"{{route('ajax_find_gym')}}",
            data: {'search':search,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
               
               $('#ajax_find_gym_data').html(data)
            }
        });
   }

    function getGymdetail(gym_id){

        $.ajax({
            url:"{{route('ajax_find_gym_detail')}}",
            data: {'gym_id':gym_id,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
               $('#id01').html(data);
               $("#myModal").modal('show');
               $('.btn-list-view').trigger('click');
               $('.cross').hide();
            }
        });
     }
  function check_in(gym_id){
    //alert(gym_id);
    // if (!confirm('Are you sure to visit this gym ?')) return false;
    swal({
      'title' : "Are you sure!",
      'text'  : "Do you want to check in this gym?",
      'icon'  : "warning",
      buttons: [true, "Yes, Check In"],
      dangerMode : true,
    }).then(function(isConfirm){
      if (!isConfirm) {
        return false;
      } else {
     $.ajax({
            url:"{{route('check_in')}}",
            data: {'gym_id':gym_id,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              console.log(data);
              if (data.flag) {
                swal({
                  title : "Successfully Check In",
                  text  : data.message,
                  icon  : 'success'
                }).then(function(){
                    window.location.reload();
                });
              } else {
                swal({
                  title : "Check In Error!",
                  text  : data.message,
                  icon  : 'error'
                });
              }
              // $('#display').css("display", "block");
              // $('#checkin_msg').html('');
              // $('#checkin_msg').html(data);
              // $(".gym_msg").fadeIn();
              // setTimeout(function() { $(".gym_msg").fadeOut(); }, 5000);
            }
           });
      }
    });
  }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#search').keyup(function(){
            var search = $('#search').val();
            //alert(search);
            $.ajax({
            url:"{{route('ajax_find_side_gym_list')}}",
            data: {'search':search,'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
               $('#ajax_find_side_gym_list').html(data)
            }
           });
        });
  });
</script>


<script>

    $(document).ready(function () {
       $('#submit_form').click(function(){
          //$('.error').hide();
          var email =  $('#email').val();
          var password =  $('#password').val();
          if(email=='' && password==''){
              $('#email_error').html('Email is required');
              $('#password_error').html('Password is required');
              $('#email_error').attr("style", "display: block !important");
              $('#password_error').attr("style", "display: block !important");
          }else if(password==''){
               $('#password_error').html('Password is required');
               $('#password_error').attr("style", "display: block !important");
          }else if(email==''){
               $('#email_error').html('Email is required');
                $('#email_error').attr("style", "display: block !important");
          }else{
              $.ajax({
                url: "{{ route('signin.submit')}}",
                data: {'email': email, 'password': password,'_token': "{{ csrf_token() }}"},
                type: "post",
                success: function (data) {
                    $('#myModalfirst').modal('toggle');
                    swal({
                        'title':"Log In Success",
                        'text':"You have logged in successfully",
                        'icon':"success"
                    }).then(function(){
                        location.reload();
                    });
                }
            });
            
                $('<queue/>')
               .delay(700 /*ms*/)
               .queue( (next) => { $('#msg_error').html('These credentials do not match our records.'); $('#msg_error').attr("style", "display: block !important");} );
          }
         // $(".error_gym").attr("style", "display: block !important");
          setTimeout(function() { $(".error_gym").attr("style", "display: none !important");}, 5000);
       });
    });

</script>
<script type="text/javascript">
  navigator.geolocation.getCurrentPosition(function(position) {
     latitute = position.coords.latitude
     longitute =  position.coords.longitude;
      // var LocalLat = localStorage.getItem("local_lat");
      // var LocalLong = localStorage.getItem("local_long");
      // if(latitude == LocalLat && longitute == LocalLong){
      //     location.reload();
      // }
}, function() {
   
});
</script>



@endsection
<?php

namespace App\Http\Controllers\WebGymUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use App\API\Subscription;
use App\API\Usersubscription;
use App\API\Plancategory;
use App\Usercard;
use App\Userusecoupon;
use Stripe;
use Auth;
use Session;

class PaymentController extends Controller {

    function __construct() {
        $this->plan_category = new Plancategory;
        $this->user_sub = new Usersubscription;
    }

    public function make_payment(Request $r) { 
       
        $validatedData = $r->validate([
           // 'plan_id' => 'required',
            'payable_amount' => 'required',
           // 'base_amount' => 'required',
            //'gst' => 'required',
            'card_number' => 'required|min:16',
            'exp_month' => 'required|min:2|max:2',
            'exp_year' => 'required|min:2|max:4',
            'cvv' => 'required|min:3|max:3',
        ]);
        $input = $r->all();
       // dd($input); 
        $form_card_number = trim($input['card_number']);
        $card_number = preg_replace('/\s+/', '', $form_card_number);
        $description = '';
        $plan_description = Plancategory::select('description')->whereid($input['plan_id'])->first();
        if ($plan_description) {
            $description = $plan_description->description;
        }
        $GymUser = Auth::User();
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        //generate token start here......
        try {
            $token = \Stripe\Token::create([
                        'card' => [
                            'number' => $card_number,
                            'exp_month' => (int) trim($input['exp_month']),
                            'exp_year' => (int) trim($input['exp_year']),
                            'cvc' => trim($input['cvv'])
                        ]
            ]);
        } catch (\Stripe\Exception\CardException $e) {
            Session::flash('msg', $e->getError()->message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\RateLimitException $e) {
            $Message = 'Too many requests made to the API too quickly';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $Message = 'Invalid parameters were supplied to Stripes API';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $Message = 'Authentication with Stripes API failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $Message = 'Network communication with Stripe failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $Message = 'Display a very generic error to the user, and maybe send yourself an email';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (Exception $e) {
            $Message = 'Something else happened, completely unrelated to Stripe';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        
       // echo'<pre>';print_r($token);die;

        // create customer id start here .....
        $get_customer_id = $this->user_sub->get_customer_id($GymUser->id);
        try {
            if ($get_customer_id == '') {
                $user_email = $GymUser->email;
                $token_id = $token->id;
                $customer = \Stripe\Customer::create([
                            'source' => $token_id,
                            'email' => $user_email,
                ]);
                $customer_id = $customer->id;
            } else {
                $customer_id = $get_customer_id;
            }
        } catch (\Stripe\Exception\CardException $e) {
            Session::flash('msg', $e->getError()->message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\RateLimitException $e) {
            $Message = 'Too many requests made to the API too quickly';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $Message = 'Invalid parameters were supplied to Stripes API';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $Message = 'Authentication with Stripes API failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $Message = 'Network communication with Stripe failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $Message = 'Display a very generic error to the user, and maybe send yourself an email';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (Exception $e) {
            $Message = 'Something else happened, completely unrelated to Stripe';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        
//            $amount = trim($input['payable_amount'] * 100);
//            $curr = 'aud'; //'usd';
//            //$token = $token->id;
//            $customer_id = trim($customer_id);
//            $charge = \Stripe\Charge::create([
//                        'amount' => $amount,
//                        'currency' => $curr,
//                        'customer' => $customer_id,
////                        'description' => $description,
////                        'source' => $token,
//            ]);
//            echo'<pre>';print_r($charge);die;
        //payment start here....
        try {
            $amount = trim($input['payable_amount'] * 100);
            $curr = 'PKR'; //'usd';
            //$token = $token->id;
            $customer_id = trim($customer_id);
            $charge = \Stripe\Charge::create([
                        'amount' => $amount,
                        'currency' => $curr,
                        'customer' => $customer_id,
//                        'description' => $description,
//                        'source' => $token,
            ]);
            if ($charge) {

                //echo $charge->payment_method_details->card->brand.'<br>';die;
                //echo '<pre>';print_r($charge);die;
                $charge_id = $charge->id ? $charge->id : '';
                $charge_object = $charge->object ? $charge->object : '';
                $charge_amount = $charge->amount ? $charge->amount : '';
                $balance_transaction_id = $charge->balance_transaction ? $charge->balance_transaction : '';
                $balance_created = $charge->created ? $charge->created : '';
                $balance_currency = $charge->currency ? $charge->currency : '';
                $balance_description = $charge->description ? $charge->description : '';
                $payment_method = $charge->payment_method ? $charge->payment_method : '';
                $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
//User Subscription Data
                $insert['user_id'] = $GymUser->id;
                $insert['subscription_id'] = $input['plan_id'];
                $insert['customer_id'] = $customer_id; //customer id
                $insert['transaction_id'] = $balance_transaction_id;
                $insert['purchased_at'] = date('Y-m-d H:i:s', $balance_created);
                $insert['plan_name'] = $get_subscription_data->plan_name;
                $insert['visit_pass'] = $get_subscription_data->visit_pass;
                $insert['plan_duration'] = $get_subscription_data->plan_duration;
                $insert['plan_time'] = $get_subscription_data->plan_time;
                $insert['amount'] = $get_subscription_data->amount;
                $insert['description'] = $description; //$get_subscription_data->description;
                $insert['payment_method'] = $payment_method;
                $insert['status'] = ($charge->status == 'succeeded') ? 1 : 0;
                $insert['currency'] = $balance_currency;
                $insert['charge_id'] = $charge_id;
              //  $insert['integrated_gst'] = $input['gst'];
                $insert['total_payable_amount'] = $input['payable_amount'];
                $plan_duration = $insert['plan_duration'];
                $plan_time = $insert['plan_time'];
                $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
                //echo '<pre>';print_r($insert);die;
// User Card Data
                $card['card_brand'] = $charge->payment_method_details->card->brand;
                //echo '<pre>';print_r($card);die;
                $card['card_country'] = $charge->payment_method_details->card->country;
                $card['card_number'] = $charge->payment_method_details->card->last4;
                $card['card_exp_month'] = $charge->payment_method_details->card->exp_month;
                $card['card_exp_year'] = $charge->payment_method_details->card->exp_year;
                $card['user_card_brand'] = $card['card_brand'];
                $card['user_card_number'] = $card_number;
                $card['user_card_exp_month'] = $input['exp_month'];
                $card['user_card_exp_year'] = $input['exp_year'];
                $card['status'] = 1;
                $card['user_id'] = $GymUser->id;
                $status['status'] = '0';
                $update = Usersubscription::where('user_id', $GymUser->id)->update($status);
                $add_payment = Usersubscription::create($insert);
                if (isset($add_payment->id) && $add_payment->id > 0) {
                    $card['usersubscription_id'] = $add_payment->id;
                    $add_card = Usercard::create($card);
                    //28-11-2019 for coupon code
                    if (!empty($input['coupon_id']) && $input['coupon_code']) {
                        $coupon['usersubscription_id'] = $add_payment->id;
                        $coupon['coupon_id'] = $input['coupon_id'];
                        $coupon['coupon_code'] = $input['coupon_code'];
                        $coupon['counpon_discount'] = $input['counpon_discount'];
                        $coupon['coupon_discount_price'] = $input['coupon_discount_amount'];
                        $coupon['user_id'] = $GymUser->id;
                        Userusecoupon::create($coupon);
                    }
                    //end coupon code
                    if ($charge->status != 'succeeded') {
                        $msg = $charge->failure_message;
                        Session::flash('msg', $msg);
                        Session::flash('message', 'alert-danger');
                        return redirect()->back();
                    } else {
                        $res['user_id'] = $GymUser->id;
                        $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
                        $is_sub_val = (boolean) 1;
                        $msg = 'Your payment for ' . $get_subscription_data->visit_pass . ' visit pass has been received. You are now subscribed to ' . $get_subscription_data->plan_category->plan_cat_name . ' plan !';
                        Session::flash('msg', $msg);
                        Session::flash('message', 'alert-success');
                        return redirect()->route('userhome');
                    }
                } else {
                    $msg = "Something went wrong payment saved failed";
                    Session::flash('msg', $msg);
                    Session::flash('message', 'alert-danger');
                    return redirect()->back();
                }
            }
         } 
         
        catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            echo 'Status is:' . $e->getHttpStatus() . '\n';
            echo 'Type is:' . $e->getError()->type . '\n';
            echo 'Code is:' . $e->getError()->code . '\n'; // param is '' in this case
            echo 'Param is:' . $e->getError()->param . '\n';
            echo 'Message is:' . $e->getError()->message . '\n';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $msg = "Too many requests made to the API too quickly";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $msg = "Invalid card parameters";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $msg = "Authentication with Stripe's API failed";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $msg = "Network communication with Stripe failed";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $msg = "Something went wrong please try again";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        } catch (Exception $e) {
            $msg = "Something went wrong please try again";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
    }

    public function generate_token() {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $token = \Stripe\Token::create([
                    'card' => [
                        'number' => '4242424242424242',
                        'exp_month' => 10,
                        'exp_year' => 2020,
                        'cvc' => '123'
                    ]
        ]);
        echo '<pre>';
        print_r($token);
        die;
    }

    public function check_cc($cc, $extra_check = false) {
        $cards = array(
            "visa" => "(4\d{12}(?:\d{3})?)",
            "amex" => "(3[47]\d{13})",
            "jcb" => "(35[2-8][89]\d\d\d{10})",
            "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
            "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
            "mastercard" => "(5[1-5]\d{14})",
            "switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
        );
        $names = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch");
        $matches = array();
        $pattern = "#^(?:" . implode("|", $cards) . ")$#";
        $result = preg_match($pattern, str_replace(" ", "", $cc), $matches);
        if ($extra_check && $result > 0) {
            $result = (validatecard($cc)) ? 1 : 0;
        }
        return ($result > 0) ? $names[sizeof($matches) - 2] : false;
    }

}

@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reset Password
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class=""><a href="#">Gym-List</a></li>
            <li class="active">Reset password</li>
        </ol>
    </section>
    <style type="text/css">
        .time{
            width: 99%;
            margin-left: 1px !important;
        }
        .star{
            color: red;
        }
        .help-block{
            color: red;
            border-color: red;
            line-height: 0px;
        }
        .help-block-text{
            color: red;
            border-color: red;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .location{
            margin-top: 10px;
        }
        .note{
            color: red;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border: 1px solid #3c8dbc !important;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-offset-2 col-xs-12 col-sm-8 col-md-8">     
                @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif 
                <div class="box">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{route('change_owner_password')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                          <!--  <input type="hidden" name="user_type" value="2"> -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Gym Owner<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2 @if($errors->has('gym_owner')) help-block @endif" id="gym_owner" name="gym_owner" required>
                                            @if(!empty($gym_owner))
                                            <option value="">Select Gym Owner Name</option>
                                            @foreach($gym_owner as $key=>$value)
                                            @php $get = $value->id==old('gym_owner') ? "selected" :"";@endphp
                                            <option value="{{$value->id}}" @php echo $get;@endphp><span >{{$value->first_name.' '.$value->last_name}} </span>( {{$value->email}} )</option>
                                            @endforeach
                                            @else
                                            <option value=""> Country Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('gym_owner'))
                                        <strong class="help-block"> {{ $errors->first('gym_owner') }}</strong>
                                        @endif
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">New Password<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control @if($errors->has('new_password')) help-block @endif" id="password" name="new_password" placeholder="New password" value="{{ old('new_password') }}" required>

                                        @if ($errors->has('new_password'))
                                        <strong class="help-block"> {{ $errors->first('new_password') }}</strong>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Confirm Password<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control @if($errors->has('confirm_password')) help-block @endif" id="confirm_password" name="confirm_password" placeholder="Confirm password" value="{{ old('confirm_password') }}" required>

                                        @if ($errors->has('confirm_password'))
                                        <strong class="help-block"> {{ $errors->first('confirm_password') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                <div class="col-sm-offset-1 col-sm-4"></div>
                                <button type="submit" class="btn btn-info">Change password</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div> 
@endsection

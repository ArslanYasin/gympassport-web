<?php

namespace App\Exports;

use App\Usergym;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Uservisitedgym;
use Session;
class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;
    function __construct($data=NULL) {
        
        $this->gym = new Usergym;
        $this->uvg = new Uservisitedgym;
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    // public function fromArray($data){
    //     return $data;
    // }

    public function headings(): array
    {
        return [
            'Sr No',
            'name',
            'Check Date',
            'Check In',
            'Purchase Plan',
            'Total Amount'
        ];
    }

}
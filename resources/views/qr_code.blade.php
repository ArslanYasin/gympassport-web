<!DOCTYPE html>
<html>
<head>
    <title>GymPassport </title>
    <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}

{{--    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">--}}
{{--    <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>--}}
{{--    <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>--}}

{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>--}}
    <style>
        body{
            background-color: black;
            font-family: "Arial Black", arial-black;
        }
        * {
            margin: 0px;
            padding: 0px;
        }
        #main_div_pdf{
            text-align: center;
            /*background-color: black;*/
            color: white;
            font-size: 25pt;
            font-weight: bold;
            /*padding: 30px;*/
        }
        #logo_image{
            margin-bottom: 150px;
            margin-top: 15px;
            width: 60%;
        }
        #qr_image{
            background-color: white;
            width: 60%;
            padding-top: 120px;
            margin: auto;
            border-radius: 10px;
            margin-bottom: 0px;
            margin-top: 70px;
        }
    </style>
</head>
<body>
    <div id="main_div_pdf">
        <img id="logo_image" src="{{ asset('landing_page/images/Website_Logo2.jpg') }}" >
        <p>
            Scan the QR Code to Check-in
        </p>

        <div id="qr_image">
            <img width="300px" src="{{ asset('image/qr_codes/'.$qr_image) }}" >

            <p style="color: black; padding-bottom: 40px">
                {{$gym_name}}
            </p>
        </div>
    </div>

</body>
</html>
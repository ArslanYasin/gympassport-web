@extends('web_user_dash.design')
@section('content')
<style type="text/css">
  .error{
    margin: 0px 0px 0px 94px;
    color: red;
  }
  .msg-box1 {
    color: red;
    justify-content: center;
    margin-bottom: 4px;
    margin-top: 10px;
    width: 100%;
    max-width: 97%;
}
</style>

 <section class="main-wrapper">
      <h4 class="contact-main-heading">Contact Us</h4>
      <div class="container-fluid">
        <div class="row">
     
          <div class="col-12 col-sm-12 col-md-6 col-lg-6">
          @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
         @endif
        <form action="{{route('submit_contactus')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group left-side-form">
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key">Enter your Name</h5>
                </div>
                <div class="col-sm-6">
                  <input type="text" name="name" class="value" placeholder="Rahul Kumar" value="{{old('name')}}">
                   @if ($errors->has('name'))<label class="error" for="">{{ $errors->first('name') }}</label>@endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key">Email Address</h5>
                </div>
                <div class="col-sm-6">
                  <input type="email" name="email" class="value" placeholder="abc@gmail.com" value="{{old('email')}}">
                  @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key">Contact Number</h5>
                </div>
                <div class="col-sm-6">
                  <input type="number" name="contact" class="value" placeholder="9876542130" value="{{old('contact')}}">
                  @if ($errors->has('contact'))<label class="error" for="">{{ $errors->first('contact') }}</label>@endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key">Message</h5>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="msg-box">
                  <textarea rows="5" cols="5" name="message" placeholder="Enter your query here..." >{{old('message')}}</textarea>
                  
                </div>
                <div class="msg-box1">
                   @if ($errors->has('message')){{ $errors->first('message') }}@endif
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary send-query-btn">Send Query</button>
          </form>
            
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6">
            <div class="right-side-form">
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key key-right">Enter Email</h5>
                </div>
                <div class="col-sm-6">
                  <input type="email" name="" class="value" placeholder="ufp@gmail.com" disabled>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key key-right">Phone</h5>
                </div>
                <div class="col-sm-6">
                  <input type="number" name="" class="value" placeholder="9876542130" disabled>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="key key-right">Address</h5>
                </div>
                <div class="col-sm-6">
                  <textarea cols="1" rows="2" class="value value-second address-textarea" disabled>ABC Street, PQR Sector, XYZ City, AB State, Country</textarea>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div id="googleMap" style="width:100%;height:317px;"></div>

              <script>
              function myMap() {
              var mapProp= {
                center:new google.maps.LatLng(-33.868820,151.209290),
                zoom:14,
                styles: [
                          {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                          {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                          {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                          {
                            featureType: 'administrative.locality',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}]
                          },
                          {
                            featureType: 'poi',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}]
                          },
                          {
                            featureType: 'poi.park',
                            elementType: 'geometry',
                            stylers: [{color: '#263c3f'}]
                          },
                          {
                            featureType: 'poi.park',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#6b9a76'}]
                          },
                          {
                            featureType: 'road',
                            elementType: 'geometry',
                            stylers: [{color: '#38414e'}]
                          },
                          {
                            featureType: 'road',
                            elementType: 'geometry.stroke',
                            stylers: [{color: '#212a37'}]
                          },
                          {
                            featureType: 'road',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#9ca5b3'}]
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'geometry',
                            stylers: [{color: '#746855'}]
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'geometry.stroke',
                            stylers: [{color: '#1f2835'}]
                          },
                          {
                            featureType: 'road.highway',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#f3d19c'}]
                          },
                          {
                            featureType: 'transit',
                            elementType: 'geometry',
                            stylers: [{color: '#2f3948'}]
                          },
                          {
                            featureType: 'transit.station',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#d59563'}]
                          },
                          {
                            featureType: 'water',
                            elementType: 'geometry',
                            stylers: [{color: '#17263c'}]
                          },
                          {
                            featureType: 'water',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#515c6d'}]
                          },
                          {
                            featureType: 'water',
                            elementType: 'labels.text.stroke',
                            stylers: [{color: '#17263c'}]
                          }
                        ]
              };
            var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
            }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=myMap"></script>
              <!-- AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU -->
              <!-- <img src="{{ asset('website_file/images/contact-pg-img.png')}}" alt="image" class="img-fluid contact-pg-img"> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <script>
      $(document).ready(function() {
          $(".trigger-navbar").click(function() {
              $(".custom-navbar").slideToggle();
          });
      });
    </script>
@endsection
@extends('web_user_dash.design')
@section('content')
<style type="text/css">
  .booking-details-wrapper {
    background-color: #383838;
     height: 100vh; 
}
  .make-payment-btn {
    -webkit-box-shadow: 0 15px 15px black;
    -moz-box-shadow: 0 15px 15px black;
    -ms-box-shadow: 0 15px 15px black;
    -o-box-shadow: 0 15px 15px black;
    box-shadow: 0 15px 15px black;
    color: white;
    background-color: #be0027;
    border: none;
    padding: 10px 68px;
    border-radius: 10px;
    font-size: 32px;
    font-weight: 700;
    margin-top: 0px;
    margin-bottom: 65px;
}
p.error {
    color: red;
    line-height: 0px;
    margin-left: 68px;
}
.alert-success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    margin: 130px 0px -52px 0px;
}
.alert-danger {
    margin: 130px 0px -52px 0px;
}

</style>
@if(!Session::has('message'))
<style type="text/css">
  .card-box1 {
    margin-top: 201px;
}
@endif
</style>
<section class="booking-details-wrapper ">
  <div class="container-fluid">
    @if(Session::has('message'))
      <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          {!! session('msg') !!}
      </div>
    @endif 
    <div id="loader"></div> 
    <form action="{{route('make_payment')}}" method="post">
         {{ csrf_field() }}
         <input type="hidden" name="plan_id" value="{{$plan_id}}">
         <input type="hidden" name="base_amount" value="{{$base_amount}}">
         <input type="hidden" name="gst" value="{{$gst}}">
         <input type="hidden" name="payable_amount" value="{{$total_amount}}">
        
        
      <div class="card-box1">
        <div class="card-fst-data">
         <!--  <h4 class="same-h4-card">Card Type</h4> -->
          <h4 class="same-h4-card">Card Number</h4>
          <!-- <h4 class="same-h4-card">Name on Card</h4> -->
          <h4 class="same-h4-card">Expiry Date</h4>
          <h4 class="same-h4-card">Expiry Year</h4>
          <h4 class="same-h4-card">Enter CVV</h4>
        </div>
        <div class="card-second-value">
          <div class="placeholder-box">
           <!--  <span>
              <select class="list">
                <option value="Master-card" class="master-card">Master Card</option>
                <option value="card-2">A</option>
                <option value="card-3">B</option>
                <option value="card-4">C</option>
              </select>
            </span> -->
            <!-- <span><i class="fa down-icon">&#xf107;</i></span> -->
            <input type="number" name="card_number" id="card_number" class="placeholder-clr-card" placeholder="7203 *** *** 1542" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "16" value="{{ old('card_number') }}">
             @if ($errors->has('card_number'))<p class="error" for="">{{ $errors->first('card_number') }}</p>@endif

            <input type="number" name="exp_month" id="exp_month" class="placeholder-clr-card" placeholder="01" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "2"  value="{{ old('exp_month') }}">
             @if ($errors->has('exp_month'))<p class="error" for="">{{ $errors->first('exp_month') }}</p>@endif

            <input type="number" name="exp_year" id="exp_year" class="placeholder-clr-card" placeholder="2020" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "4" value="{{ old('exp_year') }}">
             @if ($errors->has('exp_year'))<p class="error" for="">{{ $errors->first('exp_year') }}</p>@endif

            <input type="number" name="cvv" id="cvv" class="placeholder-clr-card" placeholder="_ _ _" oninput="javascript: if (this.value.length > this.maxLength)alert('Please enter valid card number');this.value = this.value.slice(0, this.maxLength);"   maxlength = "3" value="{{ old('cvv') }}">
             @if ($errors->has('cvv'))<p class="error" for="">{{ $errors->first('cvv') }}</p>@endif
          </div>
        </div>
      </div>
      <div class="make-payment-btn-box">
        <button type="submit" class="btn btn-danger make-payment-btn submit">Make Payment</button>
      </div>
    </form>
  </div>
</section>
@if($errors->has('card_number') || $errors->has('exp_month') || $errors->has('exp_year') || $errors->has('cvv'))
<script type="text/javascript">
    $('.booking-details-wrapper').removeClass("fadace");
    document.getElementById('formpopup').classList.add('show');
    document.getElementById("loader").style.display = "none";
</script>
@endif
 @if(Session::has('message'))
    <script type="text/javascript">
        $('.booking-details-wrapper').removeClass("fadace");
        document.getElementById("loader").style.display = "none";
   //document.getElementById('formpopup').classList.add('show');
   </script>
 @endif
 <script>
document.getElementById("loader").style.display = "none";
$('.submit').click(function(){
    var myVar;
    $('.booking-details-wrapper').addClass("fadace");
    document.getElementById("loader").style.display = "block";
    myVar = setTimeout(showPage, 9000);
});

function showPage() {
 // document.getElementById("loader").style.display = "none";
}
</script>
<script>
  $(document).ready(function(){
    $(".trigger-navbar").click(function(){
      $(".custom-navbar").slideToggle();
    });
    });
  
  // $(document).ready(function(){
  //  $(".down-icon").click(function(){
  //    $(".card-list").slideToggle();
  //  });
  //  });
</script>
@endsection
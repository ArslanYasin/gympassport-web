
<!DOCTYPE html>
<html>
<head>
  <title>signup</title>
 <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=devic e-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="head-signup">
    <div class="container-fluid">
      <div class="icon-signup">
        <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="img-fluid signup-icon">
      </div>
      </div>
  </header>
  <section class="otp-left-part">
  <img src="{{ asset('website_file/images/otp-image.png')}}" alt="otp-page-image" class="img-fluid signup-left-img">
  </section>
  <section class="otp-right-part">
      @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
      @endif 
    <h5 class="otp-heading">Verify Code</h5>
    <label class="otp-labels">Please enter the OTP sent to your registered Email Id</label>

<form action="{{route('otp_check')}}" method="post">   
   {{ csrf_field() }}
     <input type="hidden" name="user_id" value="{{session()->get('user_id')}}">
      <input type="hidden" name="pin" value="{{session()->get('pin')}}">
      <input type="hidden" name="email" value="{{session()->get('email')}}">
    <div class="kettles-box">
      <div class="box-1">
      <div class="otp-wrapper">
        <img src="{{ asset('website_file/images/ketvtle_bell.png')}}" alt="image" class="img-fluid image-kettle">
        <div class="centered-text"><input type="number" name="number1" class="otp-placeholder" placeholder="0" required></div>
      </div>
  </div>
  <div class="box-2">
      <div class="otp-wrapper">
        <img src="{{ asset('website_file/images/ketvtle_bell.png')}}" alt="image" class="img-fluid image-kettle">
        <div class="centered-text"><input type="number" name="number2" class="otp-placeholder" placeholder="7" required></div>
      </div>
  </div>
  <div class="box-3">
      <div class="otp-wrapper">
        <img src="{{ asset('website_file/images/ketvtle_bell.png')}}" alt="image" class="img-fluid image-kettle">
        <div class="centered-text"><input type="number" name="number3" class="otp-placeholder" placeholder="0" required></div>
      </div>
  </div>
  <div class="box-4">
      <div class="otp-wrapper">
        <img src="{{ asset('website_file/images/ketvtle_bell.png')}}" alt="image" class="img-fluid image-kettle">
        <div class="centered-text"><input type="number" name="number4" class="otp-placeholder" placeholder="9" required></div>
      </div>
  </div>

  </div>
    <div class="kettle-box-small">
      <input type="number" name="" class="form-control kettle-small" placeholder="0">
      <input type="number" name="" class="form-control kettle-small" placeholder="7">
      <input type="number" name="" class="form-control kettle-small" placeholder="0">
      <input type="number" name="" class="form-control kettle-small" placeholder="9">
    </div>
    <div class="otp-btn-box">
            <button type="submit" class="btn btn-danger otp-verify-btn">Verify</button>
 </div>
</form>
        <h4 class="otp-last-line">The Entered Code is incorrect. Please try Again Later.</h4>
  </section>
</body>
</html>
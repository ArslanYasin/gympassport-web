<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
    protected $table='rates';
    protected $fillable = ['country_id','first_visit','second_visit','third_visit','more_visit'];
}

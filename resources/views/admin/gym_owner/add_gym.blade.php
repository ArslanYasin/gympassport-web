@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Gym-Owner Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class=""><a href="#">Gym-List</a></li>
            <li class="active">add-gym</li>
        </ol>
    </section>
    <style type="text/css">
        .time{
            width: 99%;
            margin-left: 1px !important;
        }
        .star{
            color: red;
        }
        .help-block{
            color: red;
            border-color: red;
            line-height: 0px;
        }
        .help-block-text{
            color: red;
            border-color: red;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .location{
            margin-top: 10px;
        }
        .note{
            color: red;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border: 1px solid #3c8dbc !important;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">     
                @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif
                <div class="box">
                    <div class="box box-info">
                      <div class="box-header with-border">
                        <h3 class="box-title">Upload Gym Owner CSV File.</h3>
                      </div>
                      <form action="{{route('upload_gym_owner')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="box-body">  
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">CSV File<span class="star">*</span></label>
                            <div class="col-sm-9">
                              <input type="file" class="form-control @if($errors->has('upload_gym_owner')) help-block @endif" id="upload_gym_owner" name="upload_gym_owner" placeholder="Pay on 2nd visit" value="">
                               @if ($errors->has('upload_gym_owner'))
                                   <strong class="help-block"> {{ $errors->first('upload_gym_owner') }}</strong>
                              @endif
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="col-sm-offset-1 col-sm-4"></div>
                            <button type="submit" name="submit" class="btn btn-info">Submit</button>
                            <input type="hidden" name="submit" value="import">
                          </div>
                        </div>
                      </form>
                    </div>
                </div> 
                <div class="box">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{route('store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                          <!--  <input type="hidden" name="user_type" value="2"> -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym Owner<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2 @if($errors->has('gym_owner')) help-block @endif" id="gym_owner" name="gym_owner">
                                            @if(!empty($gym_owner))
                                            <option value="">Select Gym Owner Name</option>
                                            @foreach($gym_owner as $key=>$value)
                                            @php $get = $value->id==old('gym_owner') ? "selected" :"";@endphp
                                            <option value="{{$value->id}}" @php echo $get;@endphp><span >{{$value->first_name.' '.$value->last_name}} </span>( {{$value->email}} )</option>
                                            @endforeach
                                            @else
                                            <option value=""> Country Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('gym_owner'))
                                        <strong class="help-block"> {{ $errors->first('gym_owner') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <!--  <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-2 control-label">First Name<span class="star">*</span></label>
                                   <div class="col-sm-10">
                                     <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{ old('first_name') }}" placeholder="Enter First Name">
                                       @if ($errors->has('first_name'))
                                          <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                                       @endif
                                   </div>
                                 </div>  -->

                                <!--  <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-2 control-label">Last Name<span class="star">*</span></label>
                                   <div class="col-sm-10">
                                    <input type="text" class="form-control @if($errors->has('last_name')) help-block @endif" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                          <strong class="help-block"> {{ $errors->first('last_name') }}</strong>
                                   @endif
                                   </div>
                                 </div>  -->

                                <!--   <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email Address<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                      <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}"> 
                                       @if ($errors->has('email'))
                                           <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                        @endif
                                    </div>
                                  </div> -->

                                   <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-2 control-label">Owner Password<span class="star"></span></label>
                                   <div class="col-sm-10">
                                     <input type="text" class="form-control @if($errors->has('password')) help-block @endif" id="password" name="password" placeholder="Enter gym owner password" value="{{ old('password') }}">
                                     
                                      @if ($errors->has('password'))
                                          <strong class="help-block"> {{ $errors->first('password') }}</strong>
                                       @endif
                                   </div>
                                 </div> 
                                 
                                 <div class="form-group">
                                   <label for="inputEmail3" class="col-sm-2 control-label">Gym Price Per Visit<span class="star">*</span></label>
                                   <div class="col-sm-10">
                                     <input type="text" class="form-control @if($errors->has('gym_price_per_visit')) help-block @endif" id="gym_price_per_visit" name="gym_price_per_visit" placeholder="Enter gym price per visit" value="{{ old('gym_price_per_visit') }}">
                                     
                                      @if ($errors->has('gym_price_per_visit'))
                                          <strong class="help-block"> {{ $errors->first('gym_price_per_visit') }}</strong>
                                       @endif
                                   </div>
                                 </div> 

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym Name<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control @if($errors->has('gym_name')) help-block @endif" id="gym_name" name="gym_name" placeholder="Enter Gym Name" value="{{ old('gym_name') }}">
                                        @if ($errors->has('gym_name'))
                                        <strong class="help-block"> {{ $errors->first('gym_name') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Country<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2 @if($errors->has('country')) help-block @endif" id="country" name="country">
                                            @if(!empty($country))
                                            <option value=""> Country</option>
                                            @foreach($country as $key=>$value)
                                            @php $get = $value->id==old('country') ? "selected" :"";@endphp
                                            <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                                            @endforeach
                                            @else
                                            <option value=""> Country Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('country'))
                                        <strong class="help-block"> {{ $errors->first('country') }}</strong>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">City<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2 @if($errors->has('city')) help-block @endif" id="city" name="city">
                                            @if(!empty($city))
                                              <option value=""> City</option>
                                              @foreach($city as $key=>$value)
                                                @php $get = $value->name==old('city') ? "selected" :"";@endphp
                                                <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->name}}</option>
                                              @endforeach
                                            @else
                                                <option value=""> City Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('city'))
                                        <strong class="help-block"> {{ $errors->first('city') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Phone Number<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control @if($errors->has('phone_number')) help-block @endif" id="phone_number" name="phone_number" placeholder="Enter Phone Number" value="{{ old('phone_number') }}">
                                        @if ($errors->has('phone_number'))
                                        <strong class="help-block"> {{ $errors->first('phone_number') }}</strong>
                                        @endif
                                    </div>
                                </div>



                               <!--  <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym Activities<span class="star"></span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control @if($errors->has('gym_activities')) help-block-text @endif" name="gym_activities" id="gym_activities" placeholder="Enter Gym Activities">{{ old('gym_activities') }}</textarea>
                                        @if ($errors->has('gym_activities'))
                                        <strong class="help-block"> {{ $errors->first('gym_activities') }}</strong>
                                        @endif
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Tell Us about your Gym<span class="star"></span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control @if($errors->has('about_gym')) help-block-text @endif" name="about_gym" id="about_gym" placeholder="About your Gym">{{ old('about_gym') }}</textarea>
                                        @if ($errors->has('about_gym'))
                                        <strong class="help-block"> {{ $errors->first('about_gym') }}</strong>
                                        @endif
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Address<span class="star">*</span></label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control @if($errors->has('gym_address')) help-block-text @endif"  name="gym_address" id="gym_address" placeholder="Enter gym address">{{ old('gym_address') }}</textarea>
                                        @if ($errors->has('gym_address'))
                                        <strong class="help-block"> {{ $errors->first('gym_address') }}</strong>
                                        @endif
                                        <span id="auto_get" onclick="GetGeolocation()" class="btn bg-maroon location">Current Location</span>
                                    </div>
                                    <div class="col-sm-2">
                                        <i class="fa fa-map-marker" id="btn" aria-hidden="true" style="font-size: 32px;cursor: pointer;"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym Postalcode<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control @if($errors->has('gym_pin_code')) help-block @endif" id="gym_pin_code" name="gym_pin_code" placeholder="Gym postalcode" value="{{ old('gym_pin_code') }}">
                                        @if ($errors->has('gym_pin_code'))
                                        <strong class="help-block"> {{ $errors->first('gym_pin_code') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym latitude<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control @if($errors->has('gym_latitude')) help-block @endif" id="gym_latitude" name="gym_latitude" placeholder="Gym latitude" value="{{ old('gym_latitude') }}">
                                        @if ($errors->has('gym_latitude'))
                                        <strong class="help-block"> {{ $errors->first('gym_latitude') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Gym longitude<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control @if($errors->has('gym_longitude')) help-block @endif" id="gym_longitude" name="gym_longitude" placeholder="Gym longitude" value="{{ old('gym_longitude') }}">
                                        @if ($errors->has('gym_longitude'))
                                        <strong class="help-block"> {{ $errors->first('gym_longitude') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Upload Photos<span class="star">*</span></label>
                                    <div class="col-sm-10" id="imagerefresh">
                                        <input type="file" accept="image/*" name="gym_image[]" class="form-control @if($errors->has('gym_image')) help-block @endif" multiple id="gym_image">
                                        @if ($errors->has('gym_image'))
                                        <strong class="help-block"> {{ $errors->first('gym_image') }}</strong>
                                        @endif
                                        <span class="note" style="display: none;">Note: You can upload maximum 5 image.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Upload Logo<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="file" name="gym_logo" class="form-control @if($errors->has('gym_logo')) help-block @endif">
                                        @if ($errors->has('gym_logo'))
                                        <strong class="help-block"> {{ $errors->first('gym_logo') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Facilities<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2 @if($errors->has('facilities_id')) help-block @endif" id="facilities_id" name="facilities_id[]" multiple="multiple" data-placeholder="Select a Facilities">
                                            @if(!empty($facities))
                                            @foreach($facities as $key=>$value)
                                            @php $get = $value->id==old('facilities_id') ? "selected" :"";@endphp
                                            <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->facilities}}</option>
                                            @endforeach
                                            @else
                                            <option value=""> Facilities Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('facilities_id'))
                                        <strong class="help-block"> {{ $errors->first('facilities_id') }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group time">
                                <label for="inputEmail3" class="col-sm-2 control-label">Timings<span class="star">*</span></label>
                                <div class="col-sm-10">
                                     <div  class="col-sm-12" style=" margin-bottom: 13px;">
                                        <div  class="col-sm-3">
                                              <input type="checkbox" name="is_all_day_open" value="1" class="largerCheckbox" id="checkall" onClick="check_uncheck_checkbox(this.checked);" />  <span class="allday">Open 24*7</span>
                                        </div>                 
                                      </div>
                                      <div  class="col-sm-12" style=" margin-bottom: 13px;">
                                        <div  class="col-sm-3">
                                              <input type="checkbox" value="1" class="largerCheckbox" id="staff_hours" checked onClick="check_uncheck_checkbox_second(this.checked);"/>  <span class="allday" >Open hours</span>
                                        </div>                 
                                      </div> 
                                    <div  class="col-sm-12">
                                        <div  class="col-sm-3">

                                            <input type="checkbox" name="gym_open_days[]" value="0" class="flat-red" checked> Sunday
                                        </div>
                                        <div  class="col-sm-2 ">
                                            <input type="text" name="gym_open_timing[]" value="" id="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>

                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="1" class="flat-red" checked> Monday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="2" class="flat-red" checked> Tuesday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="3" class="flat-red" checked> Wednesday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="4" class="flat-red" checked> Thursday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="5" class="flat-red" checked> Friday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="gym_open_days[]" value="6" class="flat-red" checked> Saturday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="gym_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>

                                    </div>

                                </div>
                                @if ($errors->has('gym_open_days'))
                                <strong class="help-block"> {{ $errors->first('gym_open_days') }}</strong>
                                @endif
                                @if ($errors->has('gym_open_timing'))
                                <strong class="help-block"> {{ $errors->first('gym_open_timing') }}</strong>
                                @endif
                                @if ($errors->has('gym_close_time'))
                                <strong class="help-block"> {{ $errors->first('gym_close_time') }}</strong>
                                @endif
                                
<!--                                staff hours-->
                                <div class="col-sm-10 col-sm-offset-2">
                                      <div  class="col-sm-12" style=" margin-bottom: 13px;">
                                        <div  class="col-sm-3">
                                              <input type="checkbox" value="1" class="largerCheckbox" id="staff_hours" checked onClick="check_uncheck_checkbox_staff(this.checked);"/>  <span class="allday" >Staff hours</span>
                                        </div>                 
                                      </div> 
                                   <div  class="col-sm-12">
                                       <textarea name="staff_hours" id="staff_hrs" class="form-control" placeholder="Enter staff hours"></textarea>
                                   </div>
<!--                                    <div  class="col-sm-12">
                                        <div  class="col-sm-3">

                                            <input type="checkbox" name="staff_open_days[]" value="0" class="flat-red" checked> Sunday
                                        </div>
                                        <div  class="col-sm-2 ">
                                            <input type="text" name="staff_open_timing[]" value="" id="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>

                                    </div>-->
<!--                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="1" class="flat-red" checked> Monday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="2" class="flat-red" checked> Tuesday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="3" class="flat-red" checked> Wednesday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="4" class="flat-red" checked> Thursday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="5" class="flat-red" checked> Friday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>
                                    </div>
                                    <div  class="col-sm-12">
                                        <div class="col-sm-3">
                                            <input type="checkbox" name="staff_open_days[]" value="6" class="flat-red" checked> Saturday
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_open_timing[]" value="" class="form-control timepicker" placeholder="Open">
                                        </div>
                                        <div  class="col-sm-2">
                                            <input type="text" name="staff_close_time[]" value="" class="form-control timepicker" placeholder="Close">
                                        </div>

                                    </div>-->

                                </div>
<!--                                @if ($errors->has('gym_open_days'))
                                <strong class="help-block"> {{ $errors->first('staff_open_days') }}</strong>
                                @endif
                                @if ($errors->has('gym_open_timing'))
                                <strong class="help-block"> {{ $errors->first('staff_open_timing') }}</strong>
                                @endif
                                @if ($errors->has('gym_close_time'))
                                <strong class="help-block"> {{ $errors->first('staff_close_time') }}</strong>
                                @endif-->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                <div class="col-sm-offset-1 col-sm-4"></div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<style type="text/css">
   input.largerCheckbox { 
       width: 20px;
        height: 20px;
    } 
</style>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
 function check_uncheck_checkbox(isChecked) {
  if(isChecked) {

    $('input[name="gym_open_days[]"]').each(function() { 
      $('input[name="gym_open_days[]"]').iCheck('uncheck');
    });
    $('#staff_hours').iCheck('uncheck');
  } else {
    $('input[name="gym_open_days[]"]').each(function() {
      $('input[name="gym_open_days[]"]').iCheck('check');
    });
     $('#staff_hours').iCheck('check');
  }
}

function check_uncheck_checkbox_second(isChecked) {
   // alert('dd');
  if(isChecked) {
       $('input[name="gym_open_days[]"]').each(function() { 
      $('input[name="gym_open_days[]"]').iCheck('check');
    });
    $('#checkall').iCheck('uncheck');
  } else {
    $('input[name="gym_open_days[]"]').each(function() {
      $('input[name="gym_open_days[]"]').iCheck('uncheck');
    });
     $('#checkall').iCheck('check');
  }
}


function check_uncheck_checkbox_staff(isChecked) {
   // alert('dd');
  if(isChecked) {
      $('#staff_hrs').removeAttr('readonly');
       $('input[name="staff_open_days[]"]').each(function() { 
      $('input[name="staff_open_days[]"]').iCheck('check');
    });
   // $('#checkall').iCheck('uncheck');
  } else {
     $('#staff_hrs').attr('readonly','true');
    $('input[name="staff_open_days[]"]').each(function() {
      $('input[name="staff_open_days[]"]').iCheck('uncheck');
    });
     //$('#checkall').iCheck('check');
  }
}
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#gym_image').change(function () {
        //get the input and the file list
        var input = document.getElementById('gym_image');
        if (input.files.length > 5) {
            $('.note').css('display', 'block');
        } else {
            $('.note').css('display', 'none');
            $("#gym_image").trigger('reset');
        }
    });
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=initMap"
type="text/javascript"></script>

<script type="text/javascript">
//Function to covert address to Latitude and Longitude
$('#btn').on('click', function () {
    var address = $('#gym_address').val();
    if (address == '') {
        alert('please enter address first');
    } else {
        var getLocation = function (address) {
            //alert(address);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    //  alert(latitude +"-----"+ longitude);
                    $('#gym_latitude').val(latitude);
                    $('#gym_longitude').val(longitude);
                    //console.log(latitude, longitude);
                }
            });


        }
    }

    var address = $('#gym_address').val();
    getLocation(address);

});

</script>
<script type="text/javascript">
    /*auto detect address and lat long*/
    function GetGeolocation() {

        navigator.geolocation.getCurrentPosition(GetCoords, GetError);

    }


    function GetCoords(position) {

        document.getElementById('gym_longitude').value = position.coords.longitude;
        document.getElementById('gym_latitude').value = position.coords.latitude;
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var infowindow = new google.maps.InfoWindow({
                        content: name
                    });
                    var add = results[0].formatted_address;
                    document.getElementById('gym_address').value = add;
                    console.log(results);
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        });

    }
    function GetError() {
        aler("Fail to get location.");
    }
</script>  
@endsection

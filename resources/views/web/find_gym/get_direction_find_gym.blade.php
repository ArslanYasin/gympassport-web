@extends('web_user_dash.design')
@section('content')
<style type="text/css">
   .list-view {
    position: fixed;
    top: 0px;
}
.modal-backdrop {   
      display: none;    
}
</style>
         <section class="section-map-coll-gymdet">
             {!! $map['js'] !!}
            <div id="" style="height: 100vh;width: 100%">{!! $map['html'] !!}</div>
           <!--  <div class="mapouter googlemap"><div class="gmap_canvas"><iframe width="1073" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:1073px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:1073px;}</style></div>  -->
            <div class="list-view">
               <button class="btn-list-view" onclick="openNav()">List View<span class="gt-symbol">&gt</span></button>
            </div>
            <div id="mySidenav" class="sidenav">
               <a href="javascript:void(0)" class="closebtn cross" onclick="closeNav()">&lt</a>
               <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                  <div class="first-part">
                     <div class="search-container">
                        <input type="text" placeholder="Search Your UFP Gym" name="search" class="placeholder"><span><i class="fa fa-search search-icon"></i></span>
                     </div>
                    <!--  <div class="range">
                        <input type="range" min="1" max="7" steps="1" value="1">
                     </div>
                     <ul class="range-labels">
                        <li class="active selected"><span class="speed-num">20</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">40</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">60</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">80</span><span class="kms"> km</span></li>
                        <li><span class="speed-num">100</span><span class="kms"> km</span></li>
                     </ul> -->
                     <div class="map-coll-box-scroll">
                        <div class="box-one">
                           <div class="row">
                              <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                                 <div class="left-part">
                                    <h2>Cross Fit</h2>
                                    <h3>Sector 63, Noida</h3>
                                    <button type="button" class="distance-button">
                                       <p>5 Km Away</p>
                                    </button>
                                 </div>
                              </div>
                              <div class="col-5 col-sm-5 col-md-5 col-lg-5">
                                 <div class="image-man"><img src="{{ asset('website_file/images/man-img-map-col.png')}}" alt="man-image">
                                 </div>
                                 <div class="view-det" data-toggle="modal" data-target="#myModal"><a href="#">View Details</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="box-one">
                           <div class="row">
                              <div class="col-7 col-sm-7 col-md-7 col-lg-7">
                                 <div class="left-part">
                                    <h2>ABC Gym</h2>
                                    <h3>Sector 63, Noida</h3>
                                    <button type="button" class="distance-button">
                                       <p>5 Km Away</p>
                                    </button>
                                 </div>
                              </div>
                              <div class="col-5 col-sm-5 col-md-5 col-lg-5">
                                 <div class="image-man"><img src="{{ asset('website_file/images/man-img-map-col.png')}}" alt="man-image">
                                 </div>
                                 <div class="view-det" data-toggle="modal" data-target="#myModal"><a href="#">View Details</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="bar">
                           <span></span>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                           <div class="result">
                              <span>
                                 <h6>Showing results 1-10</h6>
                              </span>
                              <span>
                                 <div class="two-arrow">
                                    <span><a href="#">&lt</a></span>
                                    <span><a href="#">&gt</a></span>
                                 </div>
                              </span>
                           </div>
                        </div>
                        <div class="map-photo">
                           <img src="{{ asset('website_file/images/contact-pg-img.png')}}" class="img-fluid">
                        </div>
                        <div class="modal fade" id="myModal" role="dialog">
                           <div class="modal-dialog" id="id01">
                              <!-- Modal content-->
                              <div class="modal-content">
                                 <div class="wrap">
                                    <button type="button" class="closebtn cross2" data-dismiss="modal">&lt</button>
                                    <div class="left-part-mapview-gymdetails">
                                       <div class="cross-fit">
                                          <h5>Cross Fit</h5>
                                       </div>
                                       <div class="mapview-box-for-scroll">
                                          <div class="four-images">
                                             <div class="owl-carousel owl-theme" id="slider1">
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                                <div class="item">
                                                   <div class="blank-photo">
                                                      <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="hr-bar-top"><span></span>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-4 col-sm-4">
                                                <a href="#"><img src="{{ asset('website_file/images/direction.png')}}" alt="get-direction-icon"class="icon-gymdetails img-fluid"></a>
                                                <p class="for-info">Get Direction</p>
                                             </div>
                                             <div class="col-4 col-sm-4">
                                                <a href="#"><img src="{{ asset('website_file/images/contact-us.png')}}" alt="contact-us-icon" class="icon-gymdetails img-fluid"></a>
                                                <p class="for-info">Contact Us</p>
                                             </div>
                                             <div class="col-4 col-sm-4">
                                                <div class="check-in-wrapper">
                                                   <a href="#"><img src="{{ asset('website_file/images/check-ins.png')}}" alt="check-ins-icon" class="icon-gymdetails img-fluid"></a>
                                                   <p class="for-info">Total Check-In's</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="hr-bar-top"><span></span>
                                          </div>
                                          <div class="row m-0">
                                             <h5 class="four-a">About Us</h5>
                                             <div class="col-sm-12">
                                                <div class="four-stmt-box">
                                                   <p class="four-a-stmt about-us">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row m-0">
                                             <h5 class="four-a">Address</h5>
                                             <div class="col-sm-12">
                                                <div class="four-stmt-box">
                                                   <p class="four-a-stmt address">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row m-0">
                                             <h5 class="four-a">Activities</h5>
                                             <div class="col-sm-12">
                                                <div class="four-stmt-box">
                                                   <p class="four-a-stmt activities">Weight Loosing Exercise, Body Fit</p>
                                                </div>
                                             </div>
                                          </div>
                                          <h5 class="four-a">Available Days</h5>
                                          <div class="schedule">
                                             <div class="row m-0">
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Sunday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Monday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Tuesday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Wednesday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Thursday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Friday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-sm-12">
                                                   <div class="row">
                                                      <div class="col-12 col-sm-3">
                                                         <p class="days">Saturday</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 AM</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <p class="dash">-</p>
                                                      </div>
                                                      <div class="col-4 col-sm-3">
                                                         <div class="time-box">
                                                            <p class="time">10:00 PM</p>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
<!-- <p id="demo"></p> -->
<!-- <form action="" method="post">
<input type="text" name="demo" id="lat" value="">
<input type="text" name="demob" id="long" value="">
</form> -->

         </section>
         <script>
            $(document).ready(function() {
                $(".image-map-coll-page").click(function() {
                    $(".popup-paymentaa-second").toggle();
                });
            });
            
             $(document).ready(function() {
                $(".trigger-navbar").click(function() {
                    $(".custom-navbar-map-coll-page").slideToggle();
                });
            });
            
            
            function openNav() {
                if (window.matchMedia('(max-width: 400px)').matches)
                {
                   document.getElementById("mySidenav").style.width = "300px";
                   $('#mySidenav').css('display', 'block');
            
                }
                else{
                     document.getElementById("mySidenav").style.width = "400px";
                     $('#mySidenav').css('display', 'block');
                }
            
                $('.cross').css('display', 'block');
            }
            
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
                $('#mySidenav').css('display', 'none');
                $('.cross').css('display', 'none');
            }
            $('#mySidenav').css('display', 'none');

            
            
            $('#slider1').owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                dots: true,
                responsive: {
                    0: {
                        items: 3
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 3
                    }
                }
            })
            
            var sheet = document.createElement('style'),
                $rangeInput = $('.range input'),
                prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];
            
            document.body.appendChild(sheet);
            
            var getTrackStyle = function(el) {
                var curVal = el.value,
                    val = (curVal - 1) * 16,
                    style = '';
                // console.log(val);
            
                // Set active label
                $('.range-labels li').removeClass('active selected');
            
                var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');
            
                curLabel.addClass('active selected');
                curLabel.prevAll().addClass('selected');
            
                // Change background gradient
                for (var i = 0; i < prefs.length; i++) {
                    // style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
                    style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #be0027 0%, #be0027 ' + val + '%, #383838 ' + val + '%, #383838 100%)}';
                }
            
                return style;
            }
            
            $rangeInput.on('input', function() {
                sheet.textContent = getTrackStyle(this);
            });
            
            // Change input value on label click
            $('.range-labels li').on('click', function() {
                var index = $(this).index();
            
                $rangeInput.val(index + 1).trigger('input');
            
            });
         </script>
<script type="text/javascript">
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  }
   function showPosition(position) {
       lat = position.coords.latitude
       long =  position.coords.longitude;

       $.ajax({
            url:"{{route('get_lat_long')}}",
            data: {'latitude':lat, 'longitude':long, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
            }
        });
   }

</script>
@endsection
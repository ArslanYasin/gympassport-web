@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
<!--    <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css"> -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Listing
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User List</li>
      </ol>
    </section>
<style type="text/css">
  i.fa {
    font-size: 19px;
}
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
                   
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <table id="dataTable-example" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>Username</th>
                  <th>Registration date</th>
                  <th>View info</th>
                  <!-- <th>Check-in details</th>
                  <th>Subscription details</th> -->
                  <th>User status</th>
                  

                  </tr>
                  </thead>
                  </table>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $('#dataTable-example').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bSearchable_": true,
        "sAjaxSource": "{{route('cus_man_list')}}",
        "aoColumns": [
              { mData: 'username' } ,
              { mData: 'registerd_date' },
              { mData: 'view_info' },
              /*{ mData: 'check_in_details' },
              { mData: 'subscription_details' },*/
              { mData: 'user_status' }
            ]
      });  
  });
</script>
    </section>
    <!-- /.content -->
  </div>
@endsection


@extends('web_user_dash.design')
@section('content')
	<section class="section m-0">
		<div class="row m-0">
			<div class="col-12 col-md-8 col-sm-12 w-100 p-0">
				<div class="left-part-mapview-gymdetails">
				<div class="cross-fit"><h5>Cross Fit</h5></div>
				<div class="mapview-box-for-scroll">
				<div class="four-images">

			<div class="owl-carousel owl-theme" id="slider1">
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                  <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                  <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-1.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div>
            </div>
			<div class="hr-bar-top"><span></span>
							</div>
				</div>
				<div class="row">
					<div class="col-4 col-sm-4">
						<img src="images/direction.png" alt="get-direction-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Get Direction</p>
					</div>
					<div class="col-4 col-sm-4">
						<img src="images/contact-us.png" alt="contact-us-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Contact Us</p>
					</div>
					<div class="col-4 col-sm-4">
						<div class="check-in-wrapper">
						<img src="images/check-ins.png" alt="check-ins-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Total Check-In's</p>
					     </div>
					</div>
				</div>
				<div class="hr-bar-top"><span></span>
				</div>

				<div class="row m-0">
					<h5 class="four-a">About Us</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
					</div>
				</div>
			    <div class="row m-0">
			    	<h5 class="four-a">Address</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
					</div>
				</div>
				<div class="row m-0">
					<h5 class="four-a">Activities</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Weight Loosing Exercise, Body Fit</p>
					</div>
				</div>
				    <h5 class="four-a">Available Days</h5>
					<div class="schedule">
				<div class="row m-0">
                     <div class="col-sm-12">
                        <div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Sunday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								   <p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Monday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								   <p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Tuesday</p>
							</div>
							<div class="col-4 col-sm-3">
							<div class="time-box">
								   <p class="time">10:00 AM</p>
							</div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Wednesday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Thursday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
						     	</div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Friday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
				    </div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Saturday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
							    <p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>
			
			</div>
			
				<div class="col-sm-4">
					<div class="right-part-third-page">
						<a href="map-coll-page2.html"><span><div class="lt-symbol">&lt</div></span></a>
					</div>
		    	</div>
			
		</div>
		</div>
	</section>
         <script>
    	$(document).ready(function(){
			$(".image").click(function(){
				$(".popup-paymentaa").toggle();
			});
			});

		$(document).ready(function(){
			$(".trigger-navbar").click(function(){
				$(".custom-navbar").slideToggle();
			});
			});

		$('#slider1').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    responsive:{
        0:{
           items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
})

    </script>
@endsection
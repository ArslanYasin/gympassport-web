@extends('corporate_dash.design')
@section('title','Corporate | Employees')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <style>
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting {
            padding-right: 0px !important;
        }

        img.user_image {
            height: auto;
            width: 79px;
            border-radius: 15px;
        }
    </style>


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Employee Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('corporate-home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Employee-List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                @endif

                <div class="col-xs-2" style="margin-bottom: 5px;">
                    <a href="{{route('add_employee','status=1')}}">
                        <button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i> Uplaod CSV</button>
                    </a>
                </div>

                <div class="col-xs-2" style="    margin-bottom: 5px;">
                    <a href="{{route('add_employee','status=2')}}">
                        <button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Add Employee</button>
                    </a>
                </div>

                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->

                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <!-- <div class="col-sm-4"></div> -->
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <!--                                                <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'>-->
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                            <div class="col-sm-2">
                                                <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                            <div class="col-sm-2">
                                                <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary" id="submit">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <table id="example_registered" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>

                                            {{--                                            <th>User Image</th>--}}
                                                                                        <th>Username</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                            <th>Status</th>
                                            <th>Check Ins</th>

                                            <th>Edit</th>
                                            <th>Delete</th>
                                            <th>Send Email</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                    });
                    var activeTab = localStorage.getItem('activeTab');
                    if (activeTab) {
                        $('#myTab a[href="' + activeTab + '"]').tab('show');
                    }
                });
            </script>


            <script type="text/javascript">


                var urls = document.URL;
                var urlsplit = urls.split("/");
                var url = urlsplit[urlsplit.length - 2];
                var url2 = urlsplit[urlsplit.length - 1];


                $(document).ready(function () {

                    var dataTable = $('#example_registered').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('employee_list_name',['',''])}}" + '/' + url + '/' + url2,
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                data.searchdata = searchdata;
                            }
                        },
                        'columns': [

                            // {data: 'user_image'},
                            {data: 'username'},
                            {data: 'email'},
                            {data: 'plain_password'},
                            {data: 'status'},
                            {data: 'view_info'},
                            {data: 'edit'},
                            {data: 'delete'},
                            {data: 'send_email'}
                        ],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf', 'pageLength'
                        ],
                    });

                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                });
            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                    $('#startdate_first').attr('max', maxDate);
                    $('#enddate_first').attr('max', maxDate);
                });
            </script>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection


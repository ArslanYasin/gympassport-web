<?php

namespace App\Http\Controllers\Admin;

use App\API\Plancategory;
use App\API\Subscription;
use App\API\Usersubscription;
use App\Autorenewalfaild;
use App\CorporateSubscription;
use App\CorporateUsers;
use App\Country;
use App\Http\Controllers\Controller;
use App\User;
use App\Usercard;
use App\Userusecoupon;
use App\Uservisitedgym;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class SubscriptionController extends Controller
{

    function __construct()
    {
        $this->subscription = new Subscription;
        $this->plan_cat = new Plancategory;
        $this->U_Sub = new Usersubscription;
        $this->country = new Country;
        // $this->usercard = new Usercard;
    }

    public function index()
    {
        $data['plan_list'] = $this->subscription->selectdata();
        //$data = $data['plan_list'] ? $data['plan_list']:array();
        //echo '<pre>';print_r($data['plan_list']);die();
        return view('admin.subscription_management.subscription_list', $data);
    }

    public function create()
    {
        $data['country'] = $this->country->select('id', 'nicename')->get();
        $data['plan_cat'] = $this->plan_cat->get_plan_category();
        return view('admin.subscription_management.add_subscription', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'plan_name' => 'required|string|max:255',
            //'plan_duration' => 'required',
            'visit_pass' => 'required',
            //'plan_time' => 'required|string',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'country_id' => 'required|integer',
            'description' => 'required|string',
        ]);
        $this->subscription->savedata($request);
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 13)->get();
        $data['plan_id'] = $id;
        $data['plan_cat'] = $this->plan_cat->get_plan_category();
        $data['plan_list'] = $this->subscription->for_edit_select($id);
        return view('admin.subscription_management.add_subscription', $data);
    }

    public function delete_customer(Request $request, $id)
    {

        $userCard = Usercard::where('user_id', $id)->delete();
        $corporateUsers = CorporateUsers::where('user_id', $id);
        $userVisitedGym = Uservisitedgym::where('user_id', $id)->delete();
        $autoRenewalFaild = Autorenewalfaild::where('user_id', $id)->delete();
        $userUseCoupon = Userusecoupon::where('user_id', $id)->delete();
        if (DB::table('user_bank_detail')->where('user_id', $id)->get()) {
            DB::table('user_bank_detail')->where('user_id', $id)->delete();
        }
        $userSubscription = Usersubscription::where('user_id', $id)->delete();
        $user = User::where('id', $id)->delete();
        if ($user) {

            Session::flash('msg', 'User Deleted Successfully');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot delete this user right now!');
            Session::flash('message', 'alert-success');
        }
        return view('admin.customer_management.customer_list');
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            // 'plan_cat_id' => 'required|string|max:255',
            //'plan_duration' => 'required',
            'plan_name' => 'required',
            'visit_pass' => 'required',
            // 'plan_time' => 'required|string',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'country_id' => 'required|integer',
            'description' => 'required|string',
        ]);
        $this->subscription->updatedata($request, $id);
        return redirect()->back();
    }

    public function subscriberlist()
    {

        // $status1 = 1;
        // $status2 = 2;
        // $status3 = 3;
        // $status4 = 4;
        // $data['one_month'] =  $this->U_Sub->select_month_year($status1);
        // $data['three_month'] =  $this->U_Sub->select_month_year($status2);
        // $data['six_month'] =  $this->U_Sub->select_month_year($status3);
        // $data['one_year'] =   $this->U_Sub->select_month_year($status4);
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 13)->get();
        // echo '<pre>';print_r($data['one_month']);die;
        return view('admin.subscriber_list.subscriber_list', $data);
    }

    //14-10-2019
    public function ajaxsubscriber()
    {
        $plan_cat = $_GET['plan_cat'];
        // echo'<pre>';print_r($_GET);die();
        $plan_cat = $plan_cat ? $plan_cat : '';
        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $country = '';
        $visit_pass = $_GET['visit_pass'] ? $_GET['visit_pass'] : "";

        $FetchSubscriberData = $this->U_Sub->select_subscriber_list($plan_cat, $startdate, $enddate, $search, $offset, $limit_t, $country, $visit_pass);


        $totalFetchUser = $this->U_Sub->count_subscriber_list($plan_cat, $startdate, $enddate, $search, $country, $visit_pass);
        $data = $this->createfetchuserdata($FetchSubscriberData);

        $results = ["draw" => intval($draw),
            "iTotalRecords" => $totalFetchUser,
            "iTotalDisplayRecords" => $totalFetchUser,
            "aaData" => $data];
        echo json_encode($results);
    }

    public function createfetchuserdata($FetchSubscriberData)
    {
        $data = array();
        $i = 0;
        if (!empty($FetchSubscriberData)) {
            foreach ($FetchSubscriberData as $key => $value) {

                $coupon = Userusecoupon::where('usersubscription_id', $value->id)->first();
                if ($coupon) {
                    $data[$i]['coupon_code'] = $coupon->coupon_code;
                } else {
                    $data[$i]['coupon_code'] = '';
                }
                $data[$i]['total_checkin'] = $value->total_checkin;
                $data[$i]['total_earning'] = $value->total_earning;
                $data[$i]['subscriber_name'] = $value->username->first_name . ' ' . $value->username->last_name;
                $data[$i]['subscriber_email'] = $value->username->email;
                $data[$i]['amount'] = $value->total_payable_amount;
                $data[$i]['created_at'] = date('d M Y',strtotime($value->created_at));
                $active_inactive = "<a href='#' onclick='userview(" . $value->id . ")' class='btn btn-success btn-xs' id='active' >View</a>";
                $data[$i]['details'] = $active_inactive;
                $delete = " <a href='" . route('delete_user_subscription', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' onclick='return confirm(\"Are you sure to delete this subscription\")'>Delete</a>";
                $data[$i]['delete'] = $delete;
                $i++;
            }
        }
        return $data;
    }

    /*24-03-2022*/
    public function corporate_list(Request $r, $type = null)
    {

        $where = ($type) ? [0, 1] : [2]; // based on tab request 2 for pending user and 0,1 for active user
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $search = $_GET['search']['value'];

        $select = array('id', 'first_name', 'email', 'phone_number', 'status', 'created_at');
        $get_data = $this->select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate);
        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data];
        echo json_encode($results);


    }

    public function select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName)
    {

        $query = User::query($select);
        $query->whereNotIn('status', $where);
        $query->where(['is_delete' => '1', 'is_corporate' => '1']);

        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('created_at', '>=', $startdate);
            $query->whereDate('created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);

        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($user_data as $key => $value) {

            $employees = CorporateUsers::where(['corporate_id' => $value->id, 'is_delete' => 1])->count();
            $employeeIds = CorporateUsers::where(['corporate_id' => $value->id, 'is_delete' => 1])->pluck('user_id');
            $corporate = CorporateSubscription::where('corporate_id', $value->id)->first();
            $plan = Plancategory::select('plan_cat_name')->join('subscriptions', 'subscriptions.plan_name', '=', 'plan_category.id')->where('subscriptions.id', $corporate->subscription_id)->first();

            $total_check_in = Uservisitedgym::whereIn('user_id', $employeeIds)->count();
//dd($total_check_in);
            $date = date('d/m/Y', strtotime($value->created_at));

            $data[$i]['id'] = $value->id;
            $data[$i]['corporate_name'] = $corporate->corporate_name;
            $data[$i]['no_of_employee'] = $employees;
            $data[$i]['plan_name'] = $plan ? $plan->plan_cat_name : '';
            $data[$i]['total_cehck_ins'] = $total_check_in;
            $data[$i]['detail'] =" <a href='" . route('corporateEmployeeCheckInDetail' ).'?corporateId='. $value->id . "' class='btn btn-primary btn-xs'>View</a>";

            $i++;
            // }
        }
        return $data;
    }

    public function selectdata_count($search, $where, $startdate, $enddate)
    {

        $query = User::query();
        $query->whereNotIn('status', $where);
        $query->where('is_delete', '=', '1');
        $query->where('is_corporate', '=', '1');
        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('created_at', '>=', $startdate);
            $query->whereDate('created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('created_at', 'DESC');
        $gym_data = $query->get();
        return $gym_data->count();
    }

    public function getsubscribe_user($id)
    {
        $data['data'] = $this->U_Sub->user_current_active_plan($id);
        // return json_encode($data);
        return view('admin.subscriber_list.get_user_subscribe_model', $data);
    }


    public function addSubscriber()
    {
        $users = User::where('user_type', 3)->where('is_delete', 1)->where('status', 1)->get();
        $plan_category = Plancategory::all();

        return view('admin.subscriber_list.add_subscriber', compact('users', 'plan_category'));
    }

    public function getSubscriptions($id)
    {
        return $this->subscription->where('plan_name', $id)->get();
    }

    public function addUserSubscription(Request $request)
    {

        $validatedData = $request->validate([
            'user_id' => 'required',
            'subscription_id' => 'required',
            'subscription_plan' => 'required'
        ]);


        $input = $request->all();

        $userId = $input['user_id'];
        $oldSubscription = Usersubscription::where(['user_id' => $input['user_id'], 'status' => 1])->get();

        if ($oldSubscription) {
            $status = DB::statement("UPDATE usersubscriptions SET status = 0  where status =1 and user_id = $userId");
        }

        $subscribed['user_id'] = $input['user_id'];

        User::updateOrCreate(
            [
                'id' => $input['user_id']
            ],
            [
                'partner_first_login' => 1
            ]
        );

        $subscribed['customer_id'] = $input['user_id'];
        $subscribed['subscription_id'] = $input['subscription_id'];


        $get_subscription_data = Subscription::whereid($input['subscription_id'])->first();

        $subscribed['transaction_id'] = 12345;
        $subscribed['purchased_at'] = Carbon::now();

        $subscribed['plan_name'] = $get_subscription_data->plan_name;
        $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
        $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
        $subscribed['plan_time'] = $get_subscription_data->plan_time;
        $subscribed['amount'] = $get_subscription_data->amount;
        $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
        $plan_duration = $subscribed['plan_duration'];
        $plan_time = $subscribed['plan_time'];
        $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


        $subscribed['payment_method'] = 'Foree';
        $subscribed['currency'] = 'PKR';
        $subscribed['charge_id'] = 12345;
        $subscribed['total_payable_amount'] = 0;

        $corporateSubscriptionAdded = Usersubscription::create($subscribed);

        if ($corporateSubscriptionAdded) {
            $msg = "Subscription added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        }
        return redirect()->back();
    }


///// Cancle user subscription 03-11-2021
    public function CancelSubscription($uId)
    {
        $response = array();
        $user = User::find($uId);

        $date = date('Y-m-d H:i:s');
        $getdata = Usersubscription::where('user_id', $user->id)->where('status', 1)->update(['status' => 0, 'expired_at' => $date]);

        if ($getdata) {
            $msg = "Successfully cancelled subscription plan";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
            Session::flash('flag', true);

        } else {
            $msg = "Something went wrong to cancel subscription";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            Session::flash('flag', false);
//            $response['message'] = 'Something went wrong to cancel subscription';
        }

        return redirect()->back();
    }
    ////////Delete user subscription   02-02-2022/////
    public function DeleteUserSubscription(Request $request, $id)
    {
        $userSubscription = Usersubscription::find($id)->delete();

        if ($userSubscription) {
            Session::flash('msg', 'User Subscription Deleted Successfully');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot delete this subscription right now!');
            Session::flash('message', 'alert-success');
        }
        return redirect()->back();
    }
}

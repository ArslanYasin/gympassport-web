<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Resetpassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Mail\SendMailable;
use App\Mail\SendMailabletemplate;
use App\Http\Controllers\API\UserController as ApiUserController;
use App\Http\Controllers\UserAuth\LoginController;
class UserController extends Controller
{
    function __construct() {
        $this->api_user_controller = new ApiUserController;
        $this->login = new LoginController;
    }

    public function index(){
    	return view('web.login');
    }

    public function create(){
        $data['country'] = DB::table('country')->select('id', 'nicename')->where('id',13)->orderBy('nicename', 'ASC')->get();
        $country_id = 13;
        $data['state'] = DB::table('state')->select('id', 'state')->where('country_id',$country_id)->orderBy('state', 'ASC')->get();
    	return view('web.registration',$data);
    }

    public function store(Request $r){
    	//echo '<pre>';print_r($r->all());die;
         $validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'state' => 'required|string|max:255',
            'password' => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:8',
            'postal_code' => 'required|string|max:255',
            'country' => 'required',
            'city' => 'required',
            'address' => 'required',
            'checked' => 'required',
            // 'user_image' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $input = $r->all();
        unset($input['confirm_password']);
        unset($input['password']);
        unset($input['checked']);
        $input['user_ufp_id'] = mt_rand(10000, 99999);
        $input['user_token'] = $this->api_user_controller->generateRandomString();
        $input['user_token_time'] = new \DateTime();
        $input['password'] = Hash::make($r->password);
        // if ($r->hasFile('user_image')){
        //     $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
        //     request()->user_image->move(public_path('image/user_image'), $imageName);
        // }
        // $input['user_image'] = '/public/image/user_image/' . $imageName;
        $insert = User::create($input);
        $lastID = $insert->id;
        if ($lastID > 0) {
            \Mail::to($input['email'])->send(new SendMailabletemplate($lastID));
            return $this->login->login($r);//here user login after registartion
            // $msg = "User Registration successfully";
            // Session::flash('msg',$msg);
            // Session::flash('message','alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }
        return redirect()->back();
    }

    public function login(Request $r){
        $validatedData = $r->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:8',
        ]);
    }

    public function forgate_password(){
    	return view('web.forgate_password');
    }

    public function forgate_pass(Request $r){
        $validatedData = $r->validate([
            'email' => 'required|string|email',
            'g-recaptcha-response' => 'required'
        ]);
        $message = [
            'g-recaptcha-response.required' => 'Captcha is required',
        ];
        // $validator = Validator::make($r->all(), $validatedData, $message);
        // if ($validator->fails()) {
        //     $msg = "Captacha verification failed";
        //     Session::flash('message', 'alert-danger');
        //     return redirect()->back();
        // }
        $email_id = $r->email;
        $user = User::where('email',$r->email)->first();
        if($user){
            $user_id = $user->id;
            $pin = $this->api_user_controller->generatePIN();
            $email['token'] = $pin;
            $email['email'] = $email_id;
            $get_email_otp = Resetpassword::where('email', $r->email)->first(); // For resend OTP  condition
            if ($get_email_otp) {
                Resetpassword::where('email', $r->email)->update(['token' => $pin]);
            } else {
                Resetpassword::create($email);
            }
            $tempArr = array('email' => $email['email'], 'otp' => (string) $pin);
            \Mail::to($email_id)->send(new SendMailable($pin)); 
            $data['user_id'] = $user_id;
            $data['pin'] = $pin;
            $data['email'] = $email_id;
            session()->put('email',$email_id);
            session()->put('user_id',$user_id);
            session()->put('pin',$pin);
            $msg = "A one time pin code has been sent to your email address.Please check your spam folder incase you have not received it.";
            Session::flash('msg',$msg);
            Session::flash('message','alert-success');
            //return view('web.forgate_otp',$data);
            return redirect()->route('otp');

        }else{
            $msg = "Email does not exits in our record";
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
            return redirect()->back();
         }
    }

    public function new_password(){
    	return view('web.new_password');
    }

    public function otp(){
       return view('web.forgate_otp');
    }

    public function otp_check(Request $r){
         $validatedData = $r->validate([
            'number1' => 'required',
            'number2' => 'required',
            'number3' => 'required',
            'number4' => 'required',
            'email'=>'required',
        ]);
        $input= $r->all();
        $get_db_otp = Resetpassword::where('email',$input['email'])->first();
        if($get_db_otp){
            $request_otp =  $input['number1'].$input['number2'].$input['number3'].$input['number4'];
            $db_otp = $get_db_otp->token;
            if($request_otp == $db_otp){
                session()->put('email',$input['email']);
                session()->put('user_id',$input['user_id']);
                $msg = "OTP verified please enter new password";
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
                return redirect()->route('new_password');
                //return view('web.new_password',$data);
            }else{
                $msg = "Incorrect OTP please add valid otp";
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
                return redirect()->back();
            }
        }
    }
    //public
    public function update_password(Request $r){
        //echo '<pre>';print_r($r->all());die;
         $validatedData = $r->validate([
            'email' => 'required|string|email',
            'password' => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:8',
        ]);
        $input = $r->all();
        $update['password'] = Hash::make($input['password']);
        $password_data = User::where('email',$input['email'])->update($update);
        if($password_data){
                Session::forget('email');
                Session::forget('user_id');
                Session::forget('pin');
                $msg = "Your new password has been updated.Please sing-in to your account";
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
                return redirect()->route('singin');
        }else{
            $msg = "Something went wrong please try again";
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
            return redirect()->back();
        }
    }
}

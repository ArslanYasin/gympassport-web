<!DOCTYPE html>
<html>
<head>
  <title>UltimateFitnessPass</title>
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">
  <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <header class="header">
        <div class="container-fluid">
            <div class="logo">
                <a href="{{url('/')}}">
                    <img src="{{ asset('landing_page/images/logo.png')}}" alt="">
                </a>
            </div>
        </div>
        <style type="text/css">
            .text{
                color: red;
               border-color: red;
            }
        </style>
         <style type="text/css">
            #loader {
                  position: absolute;
                  left: 50%;
                  top: 50%;
                  z-index: 1;
                  width: 150px;
                  height: 150px;
                  margin: -75px 0 0 -75px;
                  border: 16px solid #f3f3f3;
                  border-radius: 50%;
                  border-top: 16px solid #3498db;
                  width: 120px;
                  height: 120px;
                  -webkit-animation: spin 2s linear infinite;
                  animation: spin 2s linear infinite;
                }

                @-webkit-keyframes spin {
                  0% { -webkit-transform: rotate(0deg); }
                  100% { -webkit-transform: rotate(360deg); }
                }

                @keyframes spin {
                  0% { transform: rotate(0deg); }
                  100% { transform: rotate(360deg); }
                }

                /* Add animation to "page content" */
                .animate-bottom {
                  position: relative;
                  -webkit-animation-name: animatebottom;
                  -webkit-animation-duration: 1s;
                  animation-name: animatebottom;
                  animation-duration: 1s
                }

                @-webkit-keyframes animatebottom {
                  from { bottom:-100px; opacity:0 } 
                  to { bottom:0px; opacity:1 }
                }

                @keyframes animatebottom { 
                  from{ bottom:-100px; opacity:0 } 
                  to{ bottom:0; opacity:1 }
                }

                #myDiv {
                  display: none;
                  text-align: center;
}
        </style>
    </header>
<div class="terms_condition">
  <div class="container-fluid">
    @if(!empty($terms_conditions))
        {!!$terms_conditions->description!!}
    @endif
  </div>
</div>
    <!-- Footer started here -->
    <footer class="footer">
        <div class="container-fluid">
            <ul>
                <li><a href="{{url('/')}}">Home</a></li>
                  <li><a href="{{route('terms_conditions')}}">Terms & Conditions</a></li>
                <li><a href="{{route('privacy_policy')}}">Privacy Policy</a></li>
            </ul>
        </div>
    </footer>
    <!-- Footer Ended Here -->



   
    
@if($errors->has('first_name') || $errors->has('last_name') || $errors->has('email') || $errors->has('postal_code') || $errors->has('state'))
<script type="text/javascript">
    document.getElementById('formpopup').classList.add('show');
    document.getElementById("loader").style.display = "none";
</script>
@endif
 @if(Session::has('message'))
    <script type="text/javascript">
        document.getElementById("loader").style.display = "none";
   //document.getElementById('formpopup').classList.add('show');
   </script>
 @endif
 <script>
document.getElementById("loader").style.display = "none";
$('.submit').click(function(){
    var myVar;
    document.getElementById("loader").style.display = "block";
    myVar = setTimeout(showPage, 9000);
});

function showPage() {
 // document.getElementById("loader").style.display = "none";
}
</script>
    <script type="text/javascript">
        $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
           });

        function showpopup(){
            document.getElementById('formpopup').classList.add('show');
        }  

        $(function() {
  $('.scrollMouse').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});


$(document).ready(function(){
    $(".custom-btn").click(function(){
    $('#formpopup').addClass("show");
});

$(document).on("click", function(event){
    // var trigger = $('.custom-btn, #formpopup');
    // console.log(trigger);
     if (!$(event.target).closest(".custom-btn, #formpopup").length) {
        $('#formpopup').removeClass('show');
     }

});



});


    </script>
</body>
</html>
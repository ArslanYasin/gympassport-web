@extends('web_user_dash.design')
@section('content')
<style type="text/css">
    .booking-details-wrapper {
        height: 100vh;
    }
</style>
<style>
    .loginBox input{
        margin-bottom: 12px;
    }
    .error{
        color:red;
    }
</style> 
<script type="text/javascript">
    $(document).ready(function () {
        $('#coupon_code').val('');
    });
</script>
<section class="booking-details-wrapper">
    <div class="container-fluid">
        <div class="booking-details-box1">
            <div class="first-wrapper">
                <!-- <div id="demo" class="collapse"> -->
                @php
                $plan_id ='';
                $base_amount ='';
                $gst ='';
                $total_payable ='';
                $plan_cat_id ='';
                if(!empty($plan_detail)){
                $plan_id = $plan_detail['plan_id'];
                $base_amount = $plan_detail['base_amount'];
                $gst = $plan_detail['gst'];
                $total_payable = $plan_detail['total_payable'];
                $plan_cat_id = $plan_detail['plan_cat_id'];
                }

                @endphp
                <div class="key-value-div">
                    <h4 class="same-h4">Base Amount</h4>
                </div>
                <div class="key-value-div">
                    <input type="text" name="" class="payable-value" placeholder="$50" value="${{$base_amount}}" disabled>
                    <!-- <h2 class="payable-value">$50</h2> -->
                </div>
                <!--  <div class="key-value-div">
                    <h4 class="same-h4">Internet handling fees</h4>
                 </div>
                 <div class="key-value-div">
                    <span class="qqq"><input type="text" name="" class="payable-value" placeholder="$30"></span>
                 </div>
                 <div class="key-value-div">
                    <h4 class="same-h4">Base Amount</h4>
                 </div>
                 <div class="key-value-div">
                    <input type="text" name="" class="payable-value" placeholder="$10">
                 </div>-->
                <div class="key-value-div">
                    <h4 class="same-h4">GST (10%)</h4>
                </div>
                <div class="key-value-div">
                    <input type="text" name="" class="payable-value" placeholder="$10" value="${{$gst}}" disabled>
                </div>
                <div class="key-value-div">
                    <h4 class="same-h4">Total Amount</h4>
                </div>
                <div class="key-value-div">
                    <input type="text" name="" class="payable-value" placeholder="$10" value="${{$total_payable}}" disabled>
                </div>

                <div class="key-value-div">
                    <h4 class="same-h4">Promotional discount</h4>
                </div>
                <div class="key-value-div">
                    <input type="text" name="" class="payable-value" placeholder="$10" id="coupon_value" value="$0" disabled>
                </div>
                <!--   <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><span class="span-icon"><i class="fa">&#xf106;</i></span></button> -->
                <!-- </div> -->
                <!--   <span class="span-icon"><i class="fa" data-toggle="collapse" data-target="#demo">&#xf106;</i></span> -->
                <!--            <div class="pol-tag"> <a href="#" class="cancellation-pol">Cancellation Policy</a></div>-->
            </div>
            <div class="payable-amt">
                <p class="payable-stmt">Total Payable Amount</p>
                <input type="text" name="" class="payable-value one-fifty" placeholder="$150" id="payable_amount" value="${{$total_payable}}" disabled>
            </div>
        </div>
        <div class="booking-details-box2">
            <h3 id="coupon_title">Apply Promo Code</h3>
            <div class="second-box">
                <input type="text" name="coupon_code" id="coupon_code" class="enter-code-box" placeholder="Enter code" value="">
                <span id="apply">
                    @if(isset(Auth::user()->id))
                    <button type="button" id="submit" class="apply-box">Apply</button>
                    @else
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                    <button type="button" data-toggle="modal" data-target="#myModal" class="apply-box">Apply</button>
                    @endif
                </span>
                <span id="cancle" style="display: none;">
                    <button type="button" id="cancle_coupon" class="apply-box">Remove</button>
                </span>

            </div>
            <span id="msg"></span>
        </div>
        <div class="pay-btn-box">
            <div id="before_apply_coupon">
                @if(isset(Auth::user()->id))
                <a href="{{route('card_detail',['plan_id'=>encrypt($plan_id),'base_amount'=>encrypt($base_amount),'gst'=>encrypt($gst),'total_amount'=>encrypt($total_payable),'total_payable_amount'=>encrypt($total_payable)])}}"><button type="button" class="btn btn-danger pay-btn">Pay <span id="amt">$ {{$total_payable}}</span></button></a>
                @else
                <button type="button" class="btn btn-danger pay-btn" data-toggle="modal" data-target="#myModal" >Pay <span id="amt">$ {{$total_payable}}</span></button>
                @endif
            </div>
            <div id="after_apply_coupon">

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="container">
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content content-of-modal-custom">
                    <div class="modal-header header-of-modal-custom">
                        <button type="button" class="close modal-close-btn-custom" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body body-of-modal-custom">
                        <h4 class="login-act-heading">Please login account</h4>
                        <div class="content">
                            <div class="form loginBox">
                                 <span id="msg_error" class="error"></span>
                                <form method="post" action="" accept-charset="UTF-8">
                                   <div class="custom-input-fields-box">
                                    <input id="email" class="form-control modal-custom-input-fields" type="text" placeholder="Email" name="email" required>
                                    <span id="email_error" class="error"></span>
                                    <input id="password" class="form-control modal-custom-input-fields" type="password" placeholder="Password" name="password" required>
                                    <span id="password_error" class="error"></span><br>
                                   </div>

                                    <div class="submit-btn-login-box">
                                    <input class="btn btn-default btn-login-modal" type="button" id="submit_form" value="Login">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>

            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .alert{
        color: red;
        font-size: 17px;
        font-weight: bold;
    }
</style>
<script>
    setTimeout(function() {
 $('.error').fadeOut();
}, 9000 );
    $(document).ready(function () {
       $('#submit_form').click(function(){
          //$('.error').hide();
          var email =  $('#email').val();
          var password =  $('#password').val();
          if(email=='' && password==''){
              $('#email_error').html('Email is required');
              $('#password_error').html('Password is required');
               $('#email_error').attr("style", "display: block !important");
              $('#password_error').attr("style", "display: block !important");
          }else if(password==''){
               $('#password_error').html('Password is required');
              $('#password_error').attr("style", "display: block !important");
          }else if(email==''){
               $('#email_error').html('Email is required');
                $('#email_error').attr("style", "display: block !important");
          }else{
              $.ajax({
                url: "{{ route('signin.submit')}}",
                data: {'email': email, 'password': password,'_token': "{{ csrf_token() }}"},
                type: "post",
                success: function (data) {
                    $('#myModal').modal('toggle');
                    swal({
                        'title':"Log In Success",
                        'text':"You have logged in successfully",
                        'icon':"success"
                    }).then(function(){
                        location.reload();
                    });
                }
            });
            
                $('<queue/>')
               .delay(700 /*ms*/)
               .queue( (next) => { $('#msg_error').html('These credentials do not match our records.'); $('#msg_error').attr("style", "display: block !important");} );
          }
          setTimeout(function() { $(".error").attr("style", "display: none !important");}, 5000);
       });
    });

</script>
<script>
    $(document).ready(function () {
        $(".trigger-navbar").click(function () {
            $(".custom-navbar-book-det").slideToggle();
        });
    });

</script>
<script type="text/javascript">
    $('#before_login_button').click(function () {
        swal({
            'title': "Login account!",
            'text': "Please login your account first ?",
            'icon': "warning",
            buttons: [true, "Yes, Login"],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (!isConfirm) {
                return false;
            } else {
                location.reload();
            }
        });
    });
    $('#before_login_submit').click(function () {
        swal({
            'title': "Login account!",
            'text': "Please login your account first ?",
            'icon': "warning",
            buttons: [true, "Yes, Login"],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (!isConfirm) {
                return false;
            } else {
                location.reload();
            }
        });
    });
    $('#submit').click(function () {
        var plan_id = "{{$plan_id}}";
        var base_amount = "{{$base_amount}}";
        var gst = "{{$gst}}";
        var payable_amount = "{{$total_payable}}";
        var plan_cat_id = "{{$plan_cat_id}}";
        var coupon_code = $('#coupon_code').val();
        if (coupon_code != '' && payable_amount != '') {
            $.ajax({
                url: "{{route('apply_promo_code')}}",
                data: {'plan_id': plan_id, 'base_amount': base_amount, 'gst': gst, 'payable_amount': payable_amount, 'plan_cat_id': plan_cat_id, 'coupon_code': coupon_code, '_token': "{{ csrf_token() }}"},
                type: "post",
                success: function (data) {
                    //console.log(data);
                    if (data.flag) {
                        swal({
                            title: 'Success',
                            text: data.message,
                            icon: 'success'
                        });
                        var discount_percent = '$' + data.result.discount;
                        var payable_amt = '$ ' + data.result.final_amt;
                        var coupon_code = data.result.coupon_code;
                        var discount_amount = '-$' + data.result.discount_amount;
                        var link = data.submit_button;
                        $('#coupon_value').val(discount_amount);
                        $('#payable_amount').val(payable_amt);
                        $('#coupon_code').val(coupon_code);
                        $('#after_apply_coupon').html(link);
                        $('#apply').hide();
                        $('#before_apply_coupon').hide();
                        $('#cancle').show();
                        $('#coupon_title').html('Promo Code Applied');
                        $('#coupon_title').css('color', 'green');
                        //$('#ddd').hide();
                    } else {
                        swal({
                            title: 'Coupon Error',
                            text: data.message,
                            icon: 'error'
                        });
                    }

                }
            });
        } else {
            swal({
                title: 'Coupon Error',
                text: 'Please enter coupon code',
                icon: 'error'
            }).then({
                //$("#submit").select();
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cancle_coupon').click(function () {
        swal({
            'title': "Are you sure!",
            'text': "To do cancel coupon code ?",
            'icon': "warning",
            buttons: [true, "Yes, Cancel"],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (!isConfirm) {
                return false;
            } else {
                location.reload();
            }
        });

    });
</script>
@endsection
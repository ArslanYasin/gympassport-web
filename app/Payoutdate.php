<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payoutdate extends Model
{
    protected $table = 'payout_date';
    
    protected $fillable = ['payout_date','period_from','period_to'];
}

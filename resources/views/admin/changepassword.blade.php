@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Change Password
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
</style>
    <!-- Main content -->
    <section class="content">
  <div class="col-sm-offset-3 col-sm-6">
@if(Session::has('message'))
      <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          {!! session('msg') !!}
      </div>
      @endif 
</div>
      <div class="register-box">
  <div class="register-box-body">
    <b>Change Password</b>
    <form action="{{route('change_password')}}" method="post">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="password" class="form-control @if($errors->has('current_password')) help-block @endif" name="current_password" id="current_password" placeholder="Enter Current  Password" value="{{ old('current_password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         @if ($errors->has('current_password'))
            <strong class="help-block"> {{ $errors->first('current_password') }}</strong>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="new_password" id="new_password" class="form-control " placeholder="Enter New Password" value="{{ old('new_password') }}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('new_password'))
            <strong class="help-block"> {{ $errors->first('new_password') }}</strong>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password" class="form-control  @if($errors->has('password')) help-block @endif" placeholder="Retype password" value="{{ old('password') }}">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
         @if ($errors->has('password'))
            <strong class="help-block"> {{ $errors->first('password') }}</strong>
        @endif
      </div>
      <input type="submit" class="btn btn-success" value="Update">
    </form>
  </div>
  <!-- /.form-box -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection


<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContantmanagmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contantmanagments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->tinyInteger('type')->comment = '1 =>about_us , 2=>privacy_policy ,3=>how_its_work , 4=>FAQ';
            $table->boolean('status')->default(1)->comment = '1=>active ,0=>inactive';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contantmanagments');
    }
}

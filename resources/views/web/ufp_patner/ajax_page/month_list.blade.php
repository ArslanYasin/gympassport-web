<!-- @if(!empty($month_data))
  @foreach($month_data as $key=>$data)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
       <div class="month-name">
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
  @endforeach
@else
  <div class="item">
       <div class="month-name">
          <h4 class="march">No data</h4>
          <p class="earnings-dollar">available</p>
       </div>
     </div>
@endif -->
@if(in_array("1", $static_month))  <!-- jan data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '1')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        
    @else
    <div class="item">
    @endif
    <div class="month-name month-color">
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='1')
      <div class="item">
       <div class="month-name">
          <h4 class="march">January' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">January' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("2", $static_month))  <!-- feb data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '2')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
       
    @else
    <div class="item">
       
    @endif
        <div class="month-name month-color">
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='2')
  <div class="item">
       <div class="month-name">
          <h4 class="march">February' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">February' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("3", $static_month))  <!-- march data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '3')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
    @else
    <div class="item">
    @endif
         <div class="month-name month-color">
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='3')
  <div class="item">
       <div class="month-name">
          <h4 class="march">March' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">March' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif


@if(in_array("4", $static_month))  <!-- april data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '4')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='4')
  <div class="item">
       <div class="month-name">
          <h4 class="march"> April' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
   @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">April' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("5", $static_month))  <!-- may data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '5')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
     <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='5')
  <div class="item">
       <div class="month-name">
          <h4 class="march">May' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">May' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("6", $static_month))  <!-- june data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '6')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='6')
  <div class="item">
       <div class="month-name ">
          <h4 class="march">June' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">June' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("7", $static_month))  <!-- july data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '7')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='7')
  <div class="item">
       <div class="month-name">
          <h4 class="march">July' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">July' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("8", $static_month))  <!-- August data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '8')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='8')
  <div class="item">
       <div class="month-name">
          <h4 class="march">August' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">August' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("9", $static_month))  <!-- september data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '9')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='9')
  <div class="item">
       <div class="month-name">
          <h4 class="march">September' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">September' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("10", $static_month))  <!-- october data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '10')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='10')
  <div class="item">
       <div class="month-name">
          <h4 class="march">October' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">October' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("11", $static_month))  <!-- november data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '11')
     @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif
      
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='11')
  <div class="item">
       <div class="month-name">
          <h4 class="march">November' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">November' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

@if(in_array("12", $static_month))  <!-- november data -->
  @foreach($month_data as $key=>$data)
    @if($data['month'] == '12')
    @if($data['total_earn_amount'] > 1)
    <div class="item" onclick="get_month_wise_data('{{$data['month']}}','{{$gym_id}}');">
        <div class="month-name month-color">
    @else
    <div class="item">
        <div class="month-name month-color">
    @endif 
          <h4 class="march">{{$data['month_name']}}' {{$data['year']}}</h4>
          <p class="earnings-dollar">Earnings - ${{$data['total_earn_amount']}}</p>
       </div>
     </div>
     @endif
  @endforeach
@else
  @if($current_month <='12')
  <div class="item">
       <div class="month-name">
          <h4 class="march">December' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @else
     <div class="item">
       <div class="month-name month-color">
          <h4 class="march">December' {{$current_year}}</h4>
          <p class="earnings-dollar">Earnings - $00.00</p>
       </div>
     </div>
  @endif
@endif

<script>
      $( '.month-color' ).on( 'click', function() {
            $( '.month-color' ).removeClass( 'active' );
            $( this ).addClass( 'active' );
      });
    </script>
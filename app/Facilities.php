<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilities extends Model
{
    protected $table = 'facilities';
    
    protected $fillable = ['facilities','imag_url']; 
    
    public function get_facility(){
        self::select('id','facilities','imag_url')->orderBy('id','DESC')->get();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\API\Subscription;
use App\API\Usersubscription;
use App\Autorenewalfaild;
use App\City;
use App\CorporateSubscription;
use App\CorporateUsers;
use App\Country;
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use App\User;
use App\Usercard;
use App\Userusecoupon;
use App\Uservisitedgym;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Session;

class CorporateEmployeeController extends Controller
{

    function __construct()
    {
        $this->country = new Country;

    }

    public function employeelist()
    {
        return view('admin.corporate_users.employeelist');
    }

    public function create()
    {

//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
//        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();
        $data['subscription'] = Subscription::select('id', 'description')->get();

        return view('admin.corporate_users.add_employee', $data);
    }

    public function getSubscription()
    {
        $v = $_REQUEST['plan'];
        $subscription = Subscription::select('id', 'visit_pass', 'plan_duration', 'plan_time', 'description')->where('plan_name', $v)->get();

        return $subscription;
    }

    public function store(Request $request)
    {
        $r = $request->all();

        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
//            'phone_number' => 'required',
//            'country' => 'required|integer',
//            'password' => 'required|string|min:8',
//            'user_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $already_employeed = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $r['corporate_id'])->get()->count();
        $CorporateSubscription = CorporateSubscription::where('corporate_id', $r['corporate_id'])->first();
        $no_of_employees = $CorporateSubscription->no_of_employees;
        if ($no_of_employees == 0) {
            Session::flash('msg', 'You cannot add employees.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        if ($no_of_employees <= $already_employeed) {
            Session::flash('msg', 'Max no of employees already added.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        $password = str_random(8);
        $r['plain_password'] = $password;
        $r['password'] = Hash::make($password);
        $message = ['user_name' => $r['first_name'] . ' ' . $r['last_name'], 'email' => $r['email'], 'password' => $password];
        \Mail::to($r['email'])->send(new SendMailable($message, 2));

        $r['user_type'] = 3;
        $r['partner_first_login'] = 1;


        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;

        $r['user_ufp_id'] = mt_rand(1000, 9999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();

        $user = User::Create($r);

        if ($user) {

            $subscription['user_id'] = $user->id;
            $subscription['corporate_id'] = $request->corporate_id;
            $corporateUser = CorporateUsers::create($subscription);

            $subscription_id = CorporateSubscription::select('subscription_id')->where('corporate_id', $request->corporate_id)->orderBy('id', 'desc')->first();

            $subscribed['user_id'] = $user->id;
            $subscribed['customer_id'] = $user->id;
            $subscribed['subscription_id'] = $subscription_id->subscription_id;

            $get_subscription_data = Subscription::whereid($subscription_id->subscription_id)->first();

            $subscribed['transaction_id'] = 12345;
            $subscribed['purchased_at'] = Carbon::now();

            $subscribed['plan_name'] = $get_subscription_data->plan_name;
            $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
            $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
            $subscribed['plan_time'] = $get_subscription_data->plan_time;
            $subscribed['amount'] = $get_subscription_data->amount;
            $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
            $plan_duration = $subscribed['plan_duration'];
            $plan_time = $subscribed['plan_time'];
            $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


            $subscribed['payment_method'] = 'Foree';
            $subscribed['currency'] = 'PKR';
            $subscribed['charge_id'] = 12345;
            $subscribed['total_payable_amount'] = 0;

            $corporateSubscriptionAdded = Usersubscription::create($subscribed);
            $msg = "Corporate employee added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function corporate_employee_list(Request $r, $id = null, $corporate_name = null)
    {

        $where = ($id) ? $id : ''; // based on tab request 2 for pending user and 0,1 for active user
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'users.created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $search = $_GET['search']['value'];

        $select = array('users.id', 'first_name', 'email', 'phone_number', 'status', 'users.created_at');
        $get_data = $this->select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate);

        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data];
        echo json_encode($results);


    }

    public function select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName)
    {

        $query = User::query($select);
        $query->where('corporate_users.corporate_id', $where);
        $query->where(['users.is_delete' => '1']);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');
        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('users.created_at', '>=', $startdate);
            $query->whereDate('users.created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);

        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($user_data as $key => $value) {

            $image = trans('constants.image_url') . $value->user_image;
            $data[$i]['user_image'] = '<img src="' . $image . '" class="user_image" alt="' . $value->first_name . '">';

            if ($value->status == 1) {
                $status = "<a href='" . route('corporate_employee_status', ['id' => $value->user_id, 'status' => $value->status]) . "' class='btn btn-success btn-xs' id='active' onclick='return confirm(\"Are you sure to inactive this recode\")'>Active</a>";
            } else {
                $status = " <a href='" . route('corporate_employee_status', ['id' => $value->user_id, 'status' => $value->status]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to active this recode\")'>Inactive</a>";
            }

            $delete = " <a href='" . route('delete_corporate_employee', ['id' => $value->user_id, 'cId' => $id, 'cName' => $corporate_name]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to delete this employee\")'>Delete</a>";
            $edit = "<a href='" . route('edit_corporation_employee', ['id' => $value->user_id]) . "' class='btn btn-xs bg-orange'>Edit</a>";
            $sendEmail = "<a href='" . route('send_email', ['id' => $value->user_id]) . "' class='btn btn-xs bg-black'>Send</a>";

//            $data[$i]['reg_owner'] = $value->first_name . ' ' . $value->last_name;

            $date = date('d/m/Y', strtotime($value->created_at));

            $data[$i]['username'] = $value->first_name . ' ' . $value->last_name;
            $data[$i]['email'] = $value->email;
            $data[$i]['address'] = $value->address;
            $data[$i]['plain_password'] = $value->plain_password;
            $data[$i]['country'] = ($value->countryname) ? $value->countryname->nicename : '';
            $data[$i]['city'] = ($value->cityname) ? $value->cityname->name : '';
            $data[$i]['status'] = $status;
            $data[$i]['status'] = $status;
            $data[$i]['edit'] = $edit;
            $data[$i]['delete'] = $delete;
            $data[$i]['send_email'] = $sendEmail;
            $i++;
            // }
        }
        return $data;
    }

    public function corporate_employee_status($id, $status)
    {
        //echo $id.' staus '.$status;die;
        $update['status'] = ($status == 1) ? 0 : 1;
        $updateStatus = User::whereid($id)->update($update);
        if ($updateStatus) {
            CorporateUsers::where('user_id', $id)->update($update);
            Session::flash('msg', 'Employee Status Changed Successfully');
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function selectdata_count($search, $where, $startdate, $enddate)
    {

        $query = User::query();
        $query->where('users.is_delete', '=', '1');
        $query->where('corporate_users.corporate_id', $where);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');
        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('users.created_at', '>=', $startdate);
            $query->whereDate('users.created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('users.created_at', 'DESC');

        $gym_data = $query->get();
        return $gym_data->count();
    }


    public function edit($id)
    {

        $data['user'] = User::where('id', $id)->first();
//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();

        return view('admin.corporate_users.edit_employee', $data);

    }


    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);


        $r = $request->all();

        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user->id)],
        ]);

        $user = $user->update($r);


        if ($user) {
            $msg = "Corporate Employee updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();
    }

    public function delete_corporate_employee($employee_id, $corporate_id, $corporate_name)
    {
//        $userCard = Usercard::where('user_id', $employee_id)->delete();
        $corporateUsers = CorporateUsers::where('user_id', $employee_id)->update(['is_delete' => '0']);
//        $userVisitedGym = Uservisitedgym::where('user_id', $employee_id)->delete();
//        $autoRenewalFaild = Autorenewalfaild::where('user_id', $employee_id)->delete();
//        $userUseCoupon = Userusecoupon::where('user_id', $employee_id)->delete();
//        if (DB::table('user_bank_detail')->where('user_id', $employee_id)->get()) {
//            DB::table('user_bank_detail')->where('user_id', $employee_id)->delete();
//        }
//        $userSubscription = Usersubscription::where('user_id', $employee_id)->delete();
        $user = User::where('id', $employee_id)->update(['is_delete' => '0']);
        if ($user) {
            Session::flash('msg', 'Employee Deleted Successfully');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot delete this employee right now!');
            Session::flash('message', 'alert-danger');
        }

        return redirect()->route('corporate-employee-list', [$corporate_id, $corporate_name]);
    }

    public function sendCredentials($id)
    {
        $r = User::find($id);
        $message = ['user_name' => $r->first_name . ' ' . $r->last_name, 'email' => $r->email, 'password' => $r->plain_password];

        \Mail::to($r->email)->send(new SendMailable($message, 2));
        $msg = "Email has been sent successfully";
        Session::flash('msg', $msg);
        Session::flash('message', 'alert-success');


        return redirect()->back();

    }
}

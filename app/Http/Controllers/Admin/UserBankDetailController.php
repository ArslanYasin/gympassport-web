<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bankdetail;
use App\Usergym;

class UserBankDetailController extends Controller
{
        public function index(){
            return view('admin.user_bank_details.user_bank_detail');
        }

        public function bankDetailList(Request $req)
        {
            $order_by = $_GET['order'][0]['dir'];
		$columnIndex = $_GET['order'][0]['column'];
		$columnName = $_GET['columns'][$columnIndex]['data'];
		$columnName =  ($columnName=='username') ? 'first_name' : 'created_at';
                
		$offset = $_GET['start'] ? $_GET['start'] :"0";
		$limit_t = ($_GET['length'] !='-1') ? $_GET['length'] :"";
		//$offset = $_GET['iDisplayStart'] ? $_GET['iDisplayStart'] :"0";
		//$limit_t = ($_GET['iDisplayLength'] !='-1') ? $_GET['iDisplayLength'] :"";
		$startdate  = ($_GET['startdate']) ? date('Y-m-d',strtotime($_GET['startdate'])) : '';
		$enddate  = ($_GET['enddate']) ? date('Y-m-d',strtotime($_GET['enddate'])) : '';
		$draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $totalbankdetail = Bankdetail::get()->count();
            $obj = new Bankdetail;
         $details=$obj->getbankdetail($startdate, $enddate, $search, $offset, $limit_t);
        // dd($data);
         //return $details;
        $array=[];
         foreach ($details as $detail ){
             $gym = new Usergym;
             $gym_name = $gym->get_gym_name($detail->user_id);
             $arr = [];
             foreach ($gym_name as $key => $val) {
                 array_push($arr, $val['gym_name']);
             }
            $array[]=array(
                "username"=>$detail->username->first_name ." " . $detail->username->last_name,
                "gym_name"=> implode(',', $arr),//$detail->gym_name['gym_name'],
                "bank_name"=>$detail->bank_name == null ? "N/A" : $detail->bank_name,
                "branch_name"=>$detail->branch_name == null ? "N/A" : $detail->branch_name,
                "account_name"=>$detail->account_name,
                "account_number"=>$detail->account_number,
                "account_type"=>$detail->account_type == 1 ? "Jazz Cash" : "Bank",
            );}

            $response = array(
                "draw" => intval($draw),
                "iTotalRecords" => $totalbankdetail,
                "iTotalDisplayRecords" => $totalbankdetail,
                "aaData" => $array
            );
            echo json_encode($response);
            exit;
        }
   
    //
}

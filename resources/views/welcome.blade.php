<!DOCTYPE html>
<html>
<head>
    <title>Gym Passport</title>
    <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">
    <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>
    <!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <!--  <style>
@import url('https://fonts.googleapis.com/css2?family=Jura:wght@700&display=swap');
</style> -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-6BVWR011PF"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-6BVWR011PF');
    </script>

    <style>
        h2 {
            text-align: center;
            padding: 20px;
        }

        /* Slider */

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-slider {
            position: relative;
            display: block;
            box-sizing: border-box;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-touch-callout: none;
            -khtml-user-select: none;
            -ms-touch-action: pan-y;
            touch-action: pan-y;
            -webkit-tap-highlight-color: transparent;
        }

        .slick-list {
            position: relative;
            display: block;
            overflow: hidden;
            margin: 0;
            padding: 0;
        }

        .slick-list:focus {
            outline: none;
        }

        .slick-list.dragging {
            cursor: pointer;
            cursor: hand;
        }

        .slick-slider .slick-track,
        .slick-slider .slick-list {
            -webkit-transform: translate3d(0, 0, 0);
            -moz-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .slick-track {
            position: relative;
            top: 0;
            left: 0;
            display: block;
        }

        .slick-track:before,
        .slick-track:after {
            display: table;
            content: '';
        }

        .slick-track:after {
            clear: both;
        }

        .slick-loading .slick-track {
            visibility: hidden;
        }

        .slick-slide {
            display: none;
            float: left;
            height: 100%;
            min-height: 1px;
        }

        [dir='rtl'] .slick-slide {
            float: right;
        }

        .slick-slide img {
            display: block;
        }

        .slick-slide.slick-loading img {
            display: none;
        }

        .slick-slide.dragging img {
            pointer-events: none;
        }

        .slick-initialized .slick-slide {
            display: block;
        }

        .slick-loading .slick-slide {
            visibility: hidden;
        }

        .slick-vertical .slick-slide {
            display: block;
            height: auto;
            border: 1px solid transparent;
        }

        .slick-arrow.slick-hidden {
            display: none;
        }

        .trial {
            background: #be0027;
            border-radius: 10px;
            margin: 20px;
            width: 60%;
            float: right;
        }

        .check-out-headings {
            margin-bottom: 20px;
            margin-top: 12px;
        }

        .scrollMouse {
            bottom: 0px;
        }

        .store-signup-hm-pg {
            margin-bottom: 0px;
            margin-top: 0px;
        }
    </style>
</head>
<body>

<header class="header">
    <div class="container-fluid">
        <div class="icon-hm-pg">
            <!-- <img src="images/logo.png" alt="icon" class="logo-image img-fluid"> -->
            <a href="{{url('/')}}">
                <img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="">
            </a>
        </div>
        <div class="custom-navbar-hm-pg">
            <ul>
                @if(isset(Auth::user()->id))
                    @if(Auth::user()->user_type=='3')
                        <li><a href="{{route('userhome')}}">Home</a></li>
                        {{--                        <li><a href="{{route('findgym')}}">Find Gyms</a></li>--}}
                        <li><a href="{{route('subscriptionplan')}}">Membership Plan</a></li>
                    @elseif(Auth::user()->user_type=='2')
                        <li><a href="{{route('ufppatnerhome')}}">Home</a></li>
                    @else
                        <li><a href="{{route('home')}}">Home</a></li>
                    @endif
                @else

                    {{--                    <li><a href="{{route('findgym')}}">Find Gyms</a></li>--}}
                    {{--                        <li><a href="{{route('subscriptionplan')}}">Pricing</a></li>--}}
                    <li><a href="{{route('becomepatner')}}">Become a Gym Partner</a></li>
                    <li class="@if(request()->route()->getName()=='corporate-partner') active @endif">
                        <a href="{{route('corporate-partner')}}">Corporate Partners</a></li>

                    {{--                        <li><a href="{{route('singup')}}">Sign Up</a></li>--}}
                    <li><a href="{{route('singin')}}">Login</a></li>
                @endif
            </ul>
        </div>
        <div class="trigger-navbar-home-page">
            <span></span>
        </div>
    </div>
    <style type="text/css">
        .text {
            color: red;
            border-color: red;
        }

        .becomepatner {
            float: right;
            margin-top: 31px;
            color: #e95177;
            font-size: 21px;
            font-family: 'Raleway', sans-serif;
            font-weight: 600;
            padding: 0px 0px 0px 20px;
        }
    </style>
    <style type="text/css">
        #loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }
            to {
                bottom: 0px;
                opacity: 1
            }
        }

        @keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }
            to {
                bottom: 0;
                opacity: 1
            }
        }

        #myDiv {
            display: none;
            text-align: center;
        }

        .launching-infobx p {
            font-size: 28px !important;
            /*    color: #fff !important;*/
        }

        .red {
            color: #fff !important;
        }

    </style>
</header>

<section class="banner-wraper">
    <div class="container">
        <div class="banner-info">
            <h6>
                        <span class="d-block">
                            <span class="one">one</span>
                            <span style="text-transform: capitalize !important;color: #be0027">membership</span>
                            <span>and access over <span style="color:#be0027">{{$active_gym}}</span></span>
                        </span>
                <span class="d-block " style="color: #be0027">Gyms <span style="color:white">across</span></span>
                <!-- <span class="d-block">gym</span> -->
                <span class="d-block" style="color: #be0027">Pakistan</span>
            </h6>

            <!--                onclick="showpopup();"-->
        {{--<button class="custom-btn" onclick="clickbutton();">Coming soon</button>--}}
        <!-- <div class="search-box">
                       </span> <h4>Search</h4></span>
                    </div> -->
{{--            <div class="search-box-div">--}}
{{--                        <span class="search-box">--}}
{{--                            <input type="text" name="gym_data" id="gym_data" class="search-box-text" placeholder="Please enter Gym name">--}}

{{--                        </span>--}}
{{--                <span class="search-text" id="searchgym">--}}
{{--                            Search--}}
{{--                        </span>--}}
{{--            </div>--}}
            <div class="search-box-div color-red trial">
                {{--                        <span class="search-box">--}}
                {{--                            <h2 style="font-size: 30px;font-weight: bold;color: white;font-family: Raleway">3 VISIT FREE TRIAL</h2>--}}
                {{--                        </span>--}}
                <span class="search-box">
                            <h2 style="font-size: 20px;font-weight: bold;color: white;font-family: Raleway">
{{--                                Join thousands of members who use Gym Passport to train at Hundreds of Gyms with a single membership.--}}
                                Pakistan's Largest Gym Network. Gym Passport is Pakistan's #1 Fitness platform.
                            </h2>
                </span>
            </div>
            <div class="col-lg-8" style="float:right;">
                <div class="col-12 col-sm-12">
                    <div class="access-fitness-wrapper">

                        <div class="store-signup-hm-pg" id="store-signup-hm-pg-id">
                            <a href="https://apps.apple.com/au/app/gym-passport/id1534995333" target="_blank"><img src="{{ asset('landing_page/images/app-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid apple-store-image"></a>
                            <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport" target="_blank"><img src="{{ asset('landing_page/images/google-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid google-store-image"></a>
                        </div>
                    </div>
                </div>
                <h2 style="font-size: 20px;text-align: center;font-weight: bold;color: white;font-family: Raleway">Gym Passport is registered with SECP in Pakistan with NTN 7095374</h2>
            </div>
        </div>
        <?php /*
                <div class="col-lg-12" style="float:left;">
                    <div class="col-12 col-sm-12">
                        <div class="access-fitness-wrapper">

                            <div class="store-signup-hm-pg" id="store-signup-hm-pg-id">
                                <a href="https://apps.apple.com/au/app/gym-passport/id1534995333" target="_blank"><img src="{{ asset('landing_page/images/app-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid apple-store-image"></a>
                                <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport" target="_blank"><img src="{{ asset('landing_page/images/google-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid google-store-image"></a>
                            </div>
                        </div>
                        <h2 style="font-size: 20px;text-align: center;font-weight: bold;color: white;font-family: Raleway">Gym Passport is registered with SECP in Pakistan with NTN 8070333</h2>

                    </div>
                </div>
                */ ?>
        <a href="#startPage" class="scrollMouse scrollto" onclick="clickbuttonscroll()">
                    <span class="smInner">
                        <span class="smDot"></span>
                    </span>
        </a>

        <div class="left-social-spritebx">
            <ul>
                <li><a href="https://www.instagram.com/gym.passport/?hl=en" class="insta" target="_blank"></a></li>
                <li><a href="https://www.facebook.com/GymPassportPk" target="_blank" class="facebook"></a></li>

            </ul>
        </div>
    </div>
</section>

<section class="text-img-wraper">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="text-div">
                    <div class="heading-one">
                        <h4><span class="color-grey">Start your fitness journey with</span></h4>
                        <h4><span class="color-grey">The </span><span class="color-red">GYM PASSPORT</span></h4>
                    </div>

                    <h4 class="color-grey heading-two">Workout wherever, whenever!</h4>
                    <h4 class="color-grey heading-three">Whether it's near <span class="color-red" data-period="2000" data-rotate='[ "HOME.", "Work or School."]'></h4>
                    <button type="button" class="btn btn-danger register-now-btn" onclick="clickbutton();">DOWNLOAD NOW</button>
                </div>

                <div class="heading-one" style="margin-top: 40px">
                    <h4><span class="color-grey">Gym Passport is Pakistan's Largest Premium Gym Network</span></h4>

                </div>

            </div>
            <div class="col-12 col-sm-7">
                <div class="img-div"><img src="{{ asset('landing_page/images/gym.png')}}" alt="image" class="img-fluid"></div>
            </div>
        </div>
    </div>
</section>

<section class="check-out-membership-wrapper" style="margin-top: 5%">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="check-out-headings">
                    <h4 class="color-grey-visit-pass neww" style="text-align: center">WE HAVE PARTNERED WITH SOME OF THE BEST FITNESS CLUBS IN PAKISTAN</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12">
                <section class="customer-logos slider" style="margin: 5%">
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo1.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo2.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo3.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo4.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo5.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo6.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo7.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo8.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo9.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo10.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo11.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo12.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo13.jpg"></div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="check-out-headings">
                    <h4 class="color-grey-visit-pass neww" style="text-align: center">PAYMENTS WE ACCEPT</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12">
                <div style="text-align: center;margin-top:10px"><img src="https://gympassport.pk/public/gymlogos/payment.png" width="400" height="300"></div>
            </div>
        </div>
    </div>
</section>

<section class="howitwork-wraper">
    <div class="container">
        <h2>How It Works</h2>
        <div class="howitworks-tabsbx">
            <div class="tabcontent-bx">
                <div class="tabcontent-imgbx show" id="tab1">
                    <img src="{{ asset('landing_page/images/htw1.png')}}" alt="">
                </div>
                <div class="tabcontent-imgbx" id="tab2">
                    <img src="{{ asset('landing_page/images/htw2.png')}}" alt="">
                </div>
                <div class="tabcontent-imgbx" id="tab3">
                    <img src="{{ asset('landing_page/images/htw3.png')}}" alt="">
                </div>
                <div class="tabcontent-imgbx" id="tab4">
                    <img src="{{ asset('landing_page/images/htw3.png')}}" alt="">
                </div>
            </div>
            <div class="tabslink-bx-wrap">
                <div class="tablinkbx active" datamenu="#tab1">
                    <div class="processbox">
                        <div class="processbar-circle"></div>
                        <span></span>
                    </div>
                    <div class="tablink-contentbx">
                        <span>1</span>
                        <p>First Download our app and sign up to get started!
                            <span>your gym passport lives here!</span></p>
                    </div>
                </div>

                <div class="tablinkbx" datamenu="#tab2">
                    <div class="processbox">
                        <div class="processbar-circle"></div>
                        <span></span>
                    </div>
                    <div class="tablink-contentbx">
                        <span>2</span>
                        <!-- <p>Choose a plan from our available plans to unlock your Ultimate
                            Fitness Pass!</p> -->
                        <p>Purchase our Membership pass from the App
                            <span>Our pass is a monthly subscription pass. You can cancel at anytime!</span></p>
                    </div>
                </div>

                <!-- <div class="tablinkbx" datamenu="#tab3">
                    <div class="processbox">
                        <div class="processbar-circle"></div>
                        <span></span>
                    </div>
                    <div class="tablink-contentbx">
                        <span>3</span>
                        <p>Download our mobile app. Your Ultimate Fitness Pass lives here.</p>
                    </div>
                </div> -->

                <div class="tablinkbx" datamenu="#tab4">
                    <div class="processbox">
                        <div class="processbar-circle"></div>
                    </div>
                    <div class="tablink-contentbx">
                        <span>3</span>
                        <p>Visit any fitness centre! <span>We have partnered with centers across Pakistan, so the last thing that will come between you and your fitness goals,will be an excuse!</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                <iframe width="100%" height="500px" src="https://www.youtube.com/embed/I2SFgjz1RAU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>


<section class="howitwork-wraper mobile-show">
    <div class="container">
        <h2>How It Works</h2>
        <div class="howitworks-tabsbx">
            <div class="tabcontent-bx">
                <div class="tabcontent-imgbx show" id="tab1">
                    <img src="{{ asset('landing_page/images/htw1.png')}}" alt="">
                <!--<img src="{{ asset('landing_page/images/howitwork1.png')}}" alt="">-->
                    <div class="tablink-contentbx">
                        <p><b>1. </b>So you want the ultimate access to fitness?
                            <span>First sign up to get started!</span></p>
                    </div>
                </div>

                <div class="tabcontent-imgbx" id="tab2">
                <!--<img src="{{ asset('landing_page/images/howitwork2.png')}}" alt="">-->
                    <img src="{{ asset('landing_page/images/htw2.png')}}" alt="">
                    <div class="tablink-contentbx">
                        <p><b>2. </b> Purchase our Membership pass from the App
                            <span>Our pass is a monthly subscription pass. You can cancel at anytime!</span></p>
                    </div>
                </div>
            <!--
                                            <div class="tabcontent-imgbx" id="tab3">
                                                <img src="{{ asset('landing_page/images/howitwork3.png')}}" alt="">
                                                <div class="tablink-contentbx">
                                                    <p> <b> 3. </b> Download our mobile app. Your Ultimate Fitness Pass lives here.</p>
                                                </div>
                                            </div> -->

                <div class="tabcontent-imgbx" id="tab4">
                    <img src="{{ asset('landing_page/images/htw3.png')}}" alt="">
                    <div class="tablink-contentbx">
                        <p><b>3. </b> Visit any fitness centre! <span>We have partnered with centers across Pakistan, so the last thing that will come between you and your fitness goals,will be an excuse!</span></p>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12 text-right">
                <iframe width="100%" height="500px" src="https://www.youtube.com/embed/I2SFgjz1RAU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="check-out-membership-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-5">
                <div class="check-out-headings">
                    <h5 class="check-out-color-grey">Check out</h5>
                    <h5 class="check-out-color-red">OUR MEMBERSHIP PLAN</h5>
                </div>
            </div>
            <div class="col-12 col-sm-7">
                <img src="{{ asset('landing_page/images/arrow-hm-pg.png')}}" alt="arrow-image" class="arrow-image-hm-pg">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="for-line-height">
                    <h4 class="color-grey">We provide 3 membership plans which will allow you to visit a range of Gyms across Pakistan. Our pass is a monthly subscription pass. We have no registration fees and you can cancel at anytime! </h4>

                    <h4 class="color-grey" style="margin-top: 20px;">Fitness enthusiasts in Pakistan visit a Gym 3 times a week on average. This equals to 13 visits in a month on average. Gym Passport provides you 20 visits each month to any Gym in Pakistan! That's surely enough to get your Gym on! </h4>
                    <!--<h4 class="color-red size-color-red">The Monthly &#8734; Pass.</h4> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <div class="visit-pass-main-box">
                    <h4 class="color-grey-visit-pass" style="font-size: 30px">Gym Passport-Lite</h4>
                    <div class="para-passes">

                        <p>Purchase our pass and have access to {{$gold_gym_count}} Gyms across Pakistan.</p>
                        <p>This pass gives you 20 visits to any non premium Gym in a monthly period!</p>

                        <!--                                 <p>passes to visit gyms at</p>
                                                        <p>your convenience.</p> -->

                        <!--  <div class="for-line-height-pass">
                             <p>Purchase from our</p>
                             <p>1, 5 or 10-day visit passes.</p>
                         </div> -->
                    </div>
                    <h6 class="color-grey-visit-pass neww">
                        Price: Rs {{$lite_price}}/month
                    </h6>
                    <h6 class="color-grey-visit-pass neww">
                        No Registration Fees
                    </h6>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <div class="visit-pass-main-box">
                    <h4 class="color-grey-visit-pass" style="font-size: 30px">Gym Passport-Standard</h4>
                    <div class="para-passes">

                        <p>Purchase our pass and have access to {{$silver_gym_count}} Premium Gyms across Pakistan.</p>
                        <p>This pass gives you 20 visits to any Gym in a monthly period!</p>

                        <!--                                 <p>passes to visit gyms at</p>
                                                        <p>your convenience.</p> -->

                        <!--  <div class="for-line-height-pass">
                             <p>Purchase from our</p>
                             <p>1, 5 or 10-day visit passes.</p>
                         </div> -->
                    </div>
                    <h6 class="color-grey-visit-pass neww">
                        Price: Rs {{$standard_price}}/month
                    </h6>
                    <h6 class="color-grey-visit-pass neww">
                        No Registration Fees
                    </h6>
                </div>
            </div>

        </div>


        <div class="row ">
            <div class="col-3 col-sm-3 col-md-3 col-lg-3"></div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <div class="visit-pass-main-box">
                    <h4 class="color-grey-visit-pass" style="font-size: 30px">Gym Passport-Pro</h4>
                    <div class="para-passes">

                        <p>Purchase our pass and have access to {{$active_gym}} Premium Gyms across Pakistan.</p>
                        <p>This pass gives you 20 visits to any Gym in a monthly period!</p>

                        <!--                                 <p>passes to visit gyms at</p>
                                                        <p>your convenience.</p> -->

                        <!--  <div class="for-line-height-pass">
                             <p>Purchase from our</p>
                             <p>1, 5 or 10-day visit passes.</p>
                         </div> -->
                    </div>
                    <h6 class="color-grey-visit-pass neww">
                        Price: Rs {{$pro_price}}/month
                    </h6>
                    <h6 class="color-grey-visit-pass neww">
                        No Registration Fees
                    </h6>
                </div>
            </div>
            <div class="col-3 col-sm-3 col-md-3 col-lg-3"></div>
        </div>


        <div class="row">
            <div class="col-12 col-sm-12">
                {{--                        <a href="#"><button type="button" class="btn btn-danger see-pricing-btn">BUY NOW</button></a>--}}
            </div>
        </div>
    </div>

</section>

{{--
<section class="check-out-membership-wrapper" style="margin-top: 5%">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="check-out-headings">
                    <h4 class="color-grey-visit-pass neww" style="text-align: center">WE HAVE PARTNERED WITH SOME OF THE BEST FITNESS CLUBS IN PAKISTAN</h4>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-12 col-sm-12">
                <section class="customer-logos slider" style="margin: 5%">
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo1.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo2.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo3.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo4.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo5.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo6.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo7.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo8.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo9.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo10.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo11.png"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo12.jpg"></div>
                    <div class="slide"><img src="https://gympassport.pk/public/gymlogos/logo13.jpg"></div>
                </section>
            </div>
        </div>

    </div>
</section>
--}}
<section class="want-to-partner-section">
    <div class="container">
        <div class="row">
            <div class="want-to-partner-div">
                <div class="col-12 col-sm-5">
                </div>
                <div class="col-12 col-sm-7">
                    <div class="right-part-black-section">
                        <h2 class="r-u-gym">Are you a Gym owner?</h2>
                        <h2 class="r-u-gym">Want to partner with us?</h2>
                        <div class="same-p-want-to-div">
                            <p class="same-p-want-to">Expand your business and add a</p>
                            <p class="same-p-want-to">second revenue stream with the</p>
                        </div>
                        <p class="u-partnership">ULTIMATE PARTNERSHIP.</p>
                        <p class="same-p-want-to">Our partnership is absolutely</p>
                        <p class="same-p-want-to-free">free</p>
                        <div class="lrn-free-btn-box">
                            <a href="{{route('becomepatner')}}">
                                <button type="button" class="btn btn-default learn-free-btn">Learn More</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="access-fitness-section">
    <!-- <div class="container-fluid"> -->
    <div class="row">
        <div class="col-12 col-sm-4">
            <!-- <div class="hm-pg-last-img"> -->
            <div class="access-fitness-section-image">
                <img src="{{ asset('landing_page/images/ufp-bottom.png')}}" alt="image" class="img-fluid">
            </div>
            <!--  </div> -->
        </div>
        <div class="col-12 col-sm-8">
            <div class="access-fitness-wrapper">
                <h4 class="access-fitness-heading">Access fitness</h4>
                <p class="fitness-section-red">WITH OUR MOBILE APPLICATION</p>
                <p class="last-grey">Download the Gym Passport app and</p>
                <p class="last-grey">get started to your ultimate access to fitness!</p>
                <div class="store-signup-hm-pg" id="store-signup-hm-pg-id">
                    <a href="https://apps.apple.com/au/app/gym-passport/id1534995333" target="_blank"><img src="{{ asset('landing_page/images/app-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid apple-store-image"></a>
                    <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport" target="_blank"><img src="{{ asset('landing_page/images/google-store.png')}}" alt="newpass-page-image-hm-pg" class="img-fluid google-store-image"></a>
                </div>
            </div>
            {{--                    <div class="coming_soon">--}}
            {{--                        <p class="text_soon">Coming Soon...</p>--}}
            {{--                    </div>--}}
        </div>
    </div>
    <!-- </div> -->
</section>

<style>
    p.text_soon {
        margin-top: 5rem;
        text-align: center;
        font-size: 37px;
        font-weight: 600;
        color: #be0027;
    }
</style>
<!-- Footer started here -->
<footer class="footer">
    <div class="container-fluid">
        <ul>
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{route('terms_conditions')}}">Terms & Conditions</a></li>
            <li><a href="{{route('privacy_policy')}}">Privacy Policy</a></li>
            <li><a href="{{route('refund_policy')}}">Refund Policy</a></li>
            <li><a href="{{route('aboutus')}}">About us</a></li>
            <li><a href="{{route('contactus')}}">Contact us</a></li>
            <li><a href="{{route('faq')}}">FAQ</a></li>
            <li><a href="{{route('becomepatner')}}">Partnerships</a></li>
        <!--                    <li><a href="{{route('subscriptionplan')}}" >Membership</a></li>-->
            {{--            <li><a href="{{route('findgym')}}">Gyms Near Me</a></li>--}}
        </ul>
    </div>
</footer>
<!-- Footer Ended Here -->


<div class="successmsg-from-popup-wraper" id="myPopup" style="display: none;">
    <div class="successmsg-popup">
        <div class="close-btnbx">
            <button type="button" class="close close-popup" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(Session::has('message'))
            <script>
                $("#myPopup").show();
                //setTimeout(function() { $("#myPopup").hide(); }, 5000);
            </script>
            {!! session('msg') !!}

        @endif
    </div>
</div>
<!--    <div class="form-popup-wraper" id="formpopup">
                 <div id="loader"></div>
                <h6>Register now</h6>
                <form action="{{route('adduser')}}" method="post">
                      {{ csrf_field() }}
        <div class="custom-formgroup">
            <input type="text" name="first_name" id="first_name" class="" placeholder="First Name" autocomplete="off" value="{{ old('first_name') }}">
                         @if ($errors->has('first_name'))<strong class="text">{{ $errors->first('first_name') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
            <input type="text" name="last_name" id="last_name" placeholder="Last Name" autocomplete="off" value="{{ old('last_name') }}">
                        @if ($errors->has('last_name'))<strong class="text">{{ $errors->first('last_name') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
            <input type="text"  placeholder="Email"  name="email" id="email" autocomplete="off" value="{{ old('email') }}">
                        @if ($errors->has('email'))<strong class="text">{{ $errors->first('email') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
            <select class="" name="country" id="country" >

                <option value=""> Select Country</option>
@if(!empty($country))
    @foreach($country as $key=>$data)
        <option value="{{$data->nicename}}" @if($data->nicename==old('country')) selected @endif> {{$data->nicename}}</option>
                                @endforeach
@endif
        </select>
@if ($errors->has('country'))<strong class="text">{{ $errors->first('country') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
             <select class="" name="state" id="state" >
                 <option value="">Select State</option>
@if(!empty($state))
    @foreach($state  as  $key=>$value)
        <option value="{{$value->state}}" @if($value->state==old('state')) selected @endif>{{$value->state}}</option>
                            @endforeach
@endif

        </select>
        <input type="text" placeholder="State" name="state" id="state" autocomplete="off" value="{{ old('state') }}">
                        @if ($errors->has('state'))<strong class="text">{{ $errors->first('state') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
            <input type="text" placeholder="Suburb" name="suburb" id="suburb" autocomplete="off" value="{{old('suburb')}}">
                        @if ($errors->has('suburb'))<strong class="text">{{ $errors->first('suburb') }}</strong>@endif
        </div>


        <div class="custom-formgroup">
            <input type="text" placeholder="Postal Code" name="postal_code" id="postal_code" autocomplete="off" value="{{ old('postal_code') }}">
                        @if ($errors->has('postal_code'))<strong class="text">{{ $errors->first('postal_code') }}</strong>@endif
        </div>

        <div class="checkbx">
           <label class="signup-checkbx">
                <input type="checkbox" class="" name="terms_service" required="" id="bk_check_box" aria-required="true" autocomplete="off">
                you agree to Ultimate Fitness Pass's Terms and Conditions of Use
                <span class="signup-checkmark"></span>
            </label>
        </div>
<p>By registering, you agree to Ultimate Fitness Pass's <a href="{{route('terms_conditions')}}" target="_blank">Terms and Conditions</a> of Use and our <a href="{{route('privacy_policy')}}" target="_blank">Privacy Policy</a></p>

                    onclick="return confirm('You agree to Ultimate Fitness Pass Terms and Conditions of Use.');"
                    <div class="submitbtn-bx">
                        <button type="submit" class="submit">Submit</button>
                    </div>
                </form>
            </div>-->


<script type="text/javascript">
    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

    function showpopup() {
        document.getElementById('formpopup').classList.add('show');
    }

    //         $(function() {
    //   $('.scrollMouse').on('click', function(e) {
    //     e.preventDefault();
    //     $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
    //   });
    // });


    $('.close-popup').click(function () {
        $('#myPopup').hide();
    });

    $('#searchgym').click(function () {
        var gym_data = $('#gym_data').val();
        var url = "{{route('findgym')}}";
        if (gym_data != '') {
            localStorage.setItem("search_gym", gym_data);
            window.location.href = '{{route("findgym")}}';
            //window.open(url);
            //$(this).attr('href', url);
        }
    });
</script>
<script>
    var TxtRotate = function (el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtRotate.prototype.tick = function () {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];
        if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

        var that = this;
        var delta = 300 - Math.random() * 500;

        if (this.isDeleting) {
            delta /= 2;
        }

        if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
        }

        setTimeout(function () {
            that.tick();
        }, delta);
    };

    window.onload = function () {
        var elements = document.getElementsByClassName('color-red');
        for (var i = 0; i < elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-rotate');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtRotate(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".color-red > .wrap { border-right: 0.08em solid #666 }";
        document.body.appendChild(css);
    };
</script>
<script>
    $(document).ready(function () {
        $(".trigger-navbar-home-page").click(function () {
            $(".custom-navbar-hm-pg").slideToggle();
        });


    });
</script>
<script type="text/javascript">
    function clickbutton() {
        window.scrollTo(500, 0);
        $('html, body').animate({
            scrollTop: $(".store-signup-hm-pg").offset().top - 200
        }, 1000);
    }
</script>

<script type="text/javascript">
    function clickbuttonscroll() {
        window.scrollTo(500, 0);
        $('html, body').animate({
            scrollTop: $(".text-img-wraper").offset().top - 200
        }, 1000);
    }
</script>


<style>
    #searchgym {
        cursor: pointer;
    }
</style>
</body>
</html>
<script>
    $(document).ready(function () {
        $('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    });
</script>

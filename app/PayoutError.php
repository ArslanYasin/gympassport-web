<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutError extends Model
{
    protected $table = 'payout_error';
    protected $fillable = ['gym_partner_id','error_msg','amount_on_month'];
}

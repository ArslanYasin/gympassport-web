<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usercard extends Model
{
    protected $table = 'user_card';
    
    protected $fillable = ['user_id','usersubscription_id','card_brand','card_country',
        'card_number','card_exp_month','card_exp_year','status','user_card_number','user_card_exp_month','user_card_exp_year','user_card_brand','code'];

    //08-01-2020
    
    public function usercard($user_id){
       $data = self::select('id','user_card_number','user_card_exp_month','user_card_exp_year')->where('user_id',$user_id)->orderBy('id','desc')->first();
       return $data;
    }
}

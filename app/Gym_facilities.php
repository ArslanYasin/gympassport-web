<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Facilities;
class Gym_facilities extends Model
{
    protected $table = 'gym_facilities';
    
    protected $fillable = ['facilities_id','gym_id'];
    
   
    
    public function gym_facilities(){
        return $this->belongsTo(Facilities::class, 'facilities_id','id')->select(array('id', 'facilities','imag_url'));
    	//return $this->hasMany('App\Facilities','id','facilities_id')->select(['id','facilities','imag_url']);
    }
}

<!DOCTYPE html>
<html>
    <head>
<!--        @if(!isset(Auth::user()->id))
        <script>
            window.location.href = '{{url("/")}}';
        </script>
        @endif-->
    <title>Gym Passport</title>
@if(isset(Auth::user()->id) && !empty(Auth::user()->id))  <!-- after user login css  start here....-->
     <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
@if(request()->route()->getName() =='userhome' || request()->route()->getName() =='ufppatnerhome')
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
@elseif(request()->route()->getName() =='findgym')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('website_file/js/owl.carousel.min.js')}}"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/owl.carousel.min.css')}}">
@elseif(request()->route()->getName() =='subscriptionplan')
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
     <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
@elseif(request()->route()->getName() =='contactus')
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
@elseif(request()->route()->getName() =='sub_plan')
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
@elseif(request()->route()->getName() =='sub_plan_detail')
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
@elseif(request()->route()->getName() =='card_detail')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
@elseif(request()->route()->getName() =='paymenthistory' || request()->route()->getName() =='checkin')
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/owl.carousel.min.css')}}">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
   <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
  <script src="{{ asset('website_file/js/owl.carousel.min.js')}}"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
@else
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
     <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
@endif
@if(request()->route()->getName() =='owner_edit_gym')
  <link rel="stylesheet" href="{{ asset('css/plugins/iCheck/all.css')}}">
  <script src="{{ asset('css/plugins/iCheck/icheck.min.js')}}"></script>
  <link rel="stylesheet" href="{{ asset('css/plugins/timepicker/bootstrap-timepicker.min.css')}}">
  <script src="{{ asset('css/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
@endif
@if(request()->route()->getName() =='owner_edit_gym')
<script>
  $(document).ready(function(){
      //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
      })
      //Red color scheme for iCheck
      $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
      })
      //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
      })

      $('.timepicker').timepicker();    
  });
</script>
@endif
 <script src="{{ asset('website_file/js/sweetalert.min.js')}}"></script>
 <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
  //setTimeout(function() { $(".gym_msg").hide(); }, 5000);
</script>
 <div id="loader-bg"></div> 
    <style type="text/css">
            li.active a {
    color: #ba2527;
}
            #loader {
                  position: absolute;
                  left: 27%;
                  top: 50%;
                  z-index: 1;
                  width: 150px;
                  height: 150px;
                  margin: -75px 0 0 -75px;
                  border: 16px solid #f3f3f3;
                  border-radius: 50%;
                  border-top: 16px solid #3498db;
                  width: 120px;
                  height: 120px;
                  -webkit-animation: spin 2s linear infinite;
                  animation: spin 2s linear infinite;
                }

                @-webkit-keyframes spin {
                  0% { -webkit-transform: rotate(0deg); }
                  100% { -webkit-transform: rotate(360deg); }
                }

                @keyframes spin {
                  0% { transform: rotate(0deg); }
                  100% { transform: rotate(360deg); }
                }

                /* Add animation to "page content" */
                .animate-bottom {
                  position: relative;
                  -webkit-animation-name: animatebottom;
                  -webkit-animation-duration: 1s;
                  animation-name: animatebottom;
                  animation-duration: 1s
                }

                @-webkit-keyframes animatebottom {
                  from { bottom:-100px; opacity:0 } 
                  to { bottom:0px; opacity:1 }
                }

                @keyframes animatebottom { 
                  from{ bottom:-100px; opacity:0 } 
                  to{ bottom:0; opacity:1 }
                }

                #myDiv {
                  display: none;
                  text-align: center;
                  }
              </style>
 <style type="text/css">
        .pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
        background-color: #373737;
        border-color: #757373;
        }
        .pagination>li>a, .pagination>li>span {
            background-color: #373737;
            border: 1px solid #757373;
        }

       .custom-navbar a:active {
    border-bottom: 2px solid red;
   }

  </style>

</head>
@else
<!-----    Here add css from landing page  based on user id  ------->
  <head>
  <title>UltimateFitnessPass</title>
  <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/media.css')}}">
  <script type="text/javascript" src="{{ asset('landing_page/js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('landing_page/js/script.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
   @if(request()->route()->getName() =='contactus')
      <script src="{{ asset('website_file/js/sweetalert.min.js')}}"></script>
   @endif
  @if(request()->route()->getName() =='findgym')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <script src="{{ asset('website_file/js/jquery.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('website_file/js/owl.carousel.min.js')}}"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/owl.carousel.min.css')}}">
    <script src="{{ asset('website_file/js/jquery.magnific-popup.min.js')}}"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/magnific-popup.min.css')}}">
    <script src="{{ asset('website_file/js/sweetalert.min.js')}}"></script>
    <style>
      button.gm-control-active.gm-fullscreen-control {
    display: none;
      }

      /*.header {
           background-color: #10151975;
        }*/
        .gm-style-mtc {
             display: none !important;
          }
          .left-part-mapview-gymdetails {
             margin-top: 0px;
        }
        .modal-content {
            margin-top: 81px;
            margin-left: -383px;
         }
         .first-part {
          margin-top: 165px;
       }
       .closebtn.cross2 {
          margin-top: 24px;
   }
   .cross-fit h5 {
    margin-top: 73px;
  }

    @media(max-width: 1200px){
    .schedule {
        width: 100%;
        max-width: 90%;
      }
    }
       @media(max-width: 768px){
      
    .check-in-wrapper {
        width: unset;
    }
          .left-part h2 {
        font-size: 16px;
        margin-top: -4px;
    }
   
 .left-part h3 {
    width: 100%;
    max-width: 50%;
}
.left-part h2 {
    width: 100%;
    max-width: 72%;
}


  }
    @media(max-width: 767px){
   
.four-stmt-image {
    width: 11%;
}
.four-a-stmt-new-text {
    margin-top: -30px;
    text-align: center;
}
 .class-for-direction-phone{

    width: 100%;
    max-width: 50%;
    display: inline-block;
    float: left;
 }
 .image-man {
    /* margin: 5px 0px 14px 26px; */
    text-align: right;
    margin-top: -100px;
}
.view-det {
    margin-left: 195px;
    /*margin-bottom: 0px;
    margin-top: 20px;*/
}
.distance-button {
   
    padding: 2px 0px;
  
}
}
@media(max-width: 575px){
  .view-det {
   margin-top: 35px;
   margin-bottom: 5px;
}
.image-man {
  
    margin-top: -95px;
}
}
/*@media(max-width: 400px){
.image-man img{
    margin-top: -14px;
    margin-left: 110px;
    height: 74px;
    }
  }*/
 
  @media(max-width: 400px){
.image-man{
    margin-left: 152px;
  
    margin-top: -93px;
    width: 80px;
  }
  .image-man img{
    margin-left: -142px;
   
  }
   
  .view-det a {
   
    margin: 0px 24px 0px -59px;
}
.map-coll-box-scroll {
    margin-top: 22px;
}
.search-container {
   
    margin-top: 70px;
}
.view-det {
    margin-top: 0px;
    margin-bottom: 0px;
    text-align: left;
    float: left;
}
  .distance-button {
    margin-left: -92px;
}
  
  /*.left-part h2 {
   
    margin-left: -89px;
}

.left-part h3 {
    
    margin-left: -78px;
  }*/
.left-part h2 {
   
    font-size: 15px;
   
    margin-bottom: 36px;
}
  .box-one, .box-two {
    padding: 22px 9px 26px 0px;
  }
.four-a-stmt-new-text {
    margin-top: -23px;
}
.four-stmt-image {
    width: 16%;
}
}
    </style>
   @endif
    @if(request()->route()->getName() =='plan_wise_gym_list')
    <style>
      header.header{
        margin-top: -100px;
      }
      @media(max-width: 992px){
    .msgcolor { 
    font-size: 17px !important; 
     }
      }
       @media(max-width: 575px){
    .msgcolor { 
    font-size: 12px !important; 
  }
      }
       @media(max-width: 480px){
    .msgcolor { 
    font-size: 9px !important; 
  }
      }
     
    </style>
    @endif
<script>
  $(document).ready(function() {
    $(".trigger-navbar-home-page").click(function() {
      $(".custom-navbar-hm-pg").slideToggle();
    });
  });
</script>
 


</head>
<!-- <style>
.box-one.class_orange {
background-color: #ea4e1d;
}
.box-one.class_red {
background-color: #c32839;
}
.box-one.class_yellow {
background-color: #f0982a;
}
.box-one.class_green {
background-color: #52a751;
}
.box-one.class_blue {
background-color: #2b72bb;
}
</style> -->
@endif
    <body>
        @if(isset(Auth::user()->id))
        @include('web_user_dash.header')
        @else
        @include('web_user_dash.before_login_header')
        @endif

        @yield('content')

        @include('web_user_dash.footer')
    </body>
 
</html>
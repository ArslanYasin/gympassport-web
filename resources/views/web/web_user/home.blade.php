@extends('web_user_dash.design')
@section('content')
<style type="text/css">
   #page-selection{
   float: right;
   }
   #checkinpage{
   float: right;
   }
@media (max-width: 768px){
.three-main-heading {
  margin-left: -51px;
}
}
   @media (max-width: 655px){
   .three-two-btns {
    margin-left: 203px;
}
}
 @media (max-width: 575px){
  .three-main-heading {
    margin-left: -91px;
}
.custom-form-key-value {
    max-width: 80%;
}
.custom-form-key-value {
     padding-right: 0px; 
}
.three-two-btns {
    margin-left: 143px;
}
}
 @media (max-width: 470px){
  .three-two-btns {
    margin-left: 107px;
}
.three-main-heading {
    margin-left: -67px;
}
}
 @media (max-width: 450px){
.three-two-btns {
    margin-left: 126px;
}
}
@media (max-width: 400px){
.three-main-heading {
    margin-left: -42px;
}
}
@media (max-width: 385px){
  .three-two-btns {
    margin-left: 111px;
}
}
@media (max-width: 350px){
.three-main-heading {
    margin-left: -22px;
}
}
@media (max-width: 330px){
.three-two-btns {
    margin-left: 101px;
}
}

h6.color-grey-visit-pass.neww {
    font-size: 23px;
    margin-top: 42px;
    margin-bottom: -21px;
    font-family: Raleway;
    text-align: center;
    color: #be0027;
    font-weight: 700;
}

button.btn.btn-danger.see-pricing-btn {
    background-color: #0000;
    color: #be0027;
    font-size: 20px;
    padding: 9px 29px;
    font-family: Raleway;
    font-weight: 700;
    border: 2px solid #be0027;
    border-radius: 10px;
    margin: 0 auto;
    display: flex;
    margin-top: 47px;
    cursor: pointer;
}
</style>
<section class="three-user-profile-wrapper">
   <div class="three-user-profile-part1">
      <div class="three-for-bg">
         @if(Session::has('message'))
          @if(session('message')=='alert-danger')
          <script type="text/javascript">
            swal({
                  title : "{!! session('msg') !!}",
                  text  :  "",
                  icon  : 'error'
                });
          </script>
         @else
           <script type="text/javascript">
            swal({
                  title : "{!! session('msg') !!}",
                  text  :  "",
                  icon  : 'success'
                });
          </script>
         @endif
            <!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               
            </div> -->
         @endif
         <h3 class="three-main-heading">User Profile</h3>
        <form class="three-left-form" method="post" action="{{route('update_profile')}}">
          {{ csrf_field() }}
            <div class="custom-form-key-value">
               <label class="three-label">User Id</label>
               <input type="number" class="three-values" placeholder="12345" value="{{$user_data->user_ufp_id}}" disabled>

               <label class="three-label">First Name</label>
               <input type="text" name="first_name" class="three-values bord enable-class" placeholder="John" disabled=""  value="{{$user_data->first_name}}" required>
               @if ($errors->has('first_name'))<label class="error" for="">{{ $errors->first('first_name') }}</label>@endif
               <label class="three-label">Last Name</label>
               <input type="text" name="last_name" class="three-values bord enable-class" placeholder="Doe" disabled=""  value="{{$user_data->last_name}}" required>
               @if ($errors->has('last_name'))<label class="error" for="">{{ $errors->first('last_name') }}</label>@endif
               <label class="three-label">Email</label>
               <input type="email" name="email" class="three-values bord enable-class" placeholder="abc@gmail.com" disabled="" value="{{$user_data->email}}" required>
               @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif
               <label class="three-label">D.O.B</label>
               <input type="date" name="d_o_b" class="three-values bord enable-class" placeholder="07/09/1995" disabled="" value="{{date('Y-m-d',strtotime(str_replace('/','-',$user_data->d_o_b)))}}" required>
               @if ($errors->has('d_o_b'))<label class="error" for="">{{ $errors->first('d_o_b') }}</label>@endif
               <label class="three-label">Country</label>
               <select class="three-values bord enable-class" name="country" disabled>
                  <option value="">Select Country</option>
                    @if(!empty($country))
                    @foreach($country as $key=>$value)
                    <option value="{{$value->id}}" class="options" @if($value->id==$user_data->country_id) selected @endif>{{$value->nicename}}</option>
                    @endforeach
                    @endif
               </select>
               @if ($errors->has('country'))<label class="error" for="">{{ $errors->first('country') }}</label>@endif
            </div>
            <div class="three-two-btns">
               <button type="button" class="btn btn-danger three-edit-profile edit">Edit Profile</button>
               <button type="submit" class="btn btn-danger three-edit-profile update" style="display: none;">Update</button>
               <button type="button" class="btn btn-danger three-cancel">Cancel</button>
               <a href="{{route('UserPasswordChange')}}"><button type="button" class="btn btn-danger three-change-password">Change Password</button></a>
            </div>
         </form>
      </div>
   </div>
   <div class="three-user-profile-part2">
      <div class="three-user-profile-menu-box">
         <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#menu1">Current Plan</a></li>
            <li><a data-toggle="pill" href="#menu2">Billing History</a></li>
            <li><a data-toggle="pill" href="#menu3">Check-In Details</a></li>
            <li><a data-toggle="pill" href="#menu4">Card Details</a></li>
         </ul>
      </div>
      <div class="tab-content">
         <div id="menu1" class="tab-pane fade in active">
          @if(!empty($current_plan))
            <div class="plan-box">
               <h5>You are currently using {{$current_plan['plan_name']}} ({{$current_plan['visit_pass']}})</h5>
               <span class="amt-subscription">Amount paid for Subscription :</span><span class="value-subscription">$ {{$current_plan['subscription_amount']}}</span>
               <div class="purchased-box"><span class="purchased-payment">Purchased on :</span><span class="purchased-payment-date">{{$current_plan['purchased_date']}}</span></div>
               <div class="payment-box">
                  <h3><span class="purchased-payment">Next Payment Date :</span><span class="purchased-payment-date">{{$current_plan['expired_date']}}</span></h3>
               </div>
               <span class="subscription">Subscription</span>
               <label class="switch " @if($current_plan['is_subscribe']=='1')onclick = "cancle_subscription();" @endif>
              <input type="checkbox" id="checkbox" @if($current_plan['is_subscribe']=='1')checked @endif>
              <span class="slider round"></span>
              </label>
               <!-- <label class="switch">
               <input type="checkbox">  
               <span class="slider round"></span>
               </label> -->
              <!--  <span class="learn-more"><a href="#">Learn More <i class="fa fa-chevron-right chevron-right-symbol"></i></a></span> -->
            </div>
         @else
         <div class="plan-box" style="align-content: center">
            <h5 style="margin-left:50px">You currently have no active plans</h5>
            <h6 class="color-grey-visit-pass neww" style="margin-left: 0px;">
                Please use our mobile app to purchase a plan.
            </h6>
{{--         <div style="margin-top:80px; margin-left:220px">--}}
{{--            <a href="{{route('card_detail')}}" class="btn btn-danger see-pricing-btn">BUY NOW</a>--}}
{{--         </div>--}}
         </div>
         @endif
         </div>
         <div id="menu2" class="tab-pane fade">
            <div class="right-box-for-scroll-billing-history" id="billing_detail">
               <!-- <div class="billing-history-box">
                  <div class="billing-history-wrapper">
                     <h2>Ultimate</h2>
                     <span class="amt-subscription-billing">5 visit Pass</span>
                     <div class="purchased-box-billing">
                        <h4>
                           <span class="purchased-payment-billing">Purchased on :</span><span class="purchased-payment-date-billing">07/010/2018</span>
                        </h4>
                        <h4>
                           <span class="purchased-payment-billing">Expired on :</span>
                           <span class="purchased-payment-date-billing">07/010/2018</span>
                        </h4>
                     </div>
                  </div>
               </div> -->
              
            </div>
         </div>
         <div id="menu3" class="tab-pane fade">
            <div class="right-box-for-scroll"  id="gym_check_in_list">
              <!--  <div class="checkin-wrapper">
                  <h2>Cross Fit</h2>
                  <span class="address-checkin">Sector 63, Noida</span><span class="get-direction"><a href="#">Get Direction</a><span class="gt-symbol-checkin">&gt</span></span>
                  <div class="date-time-box">
                     <h4><span class="date-caption-box">Date :</span><span class="date-value-box">07/08/2019</span>
                        <span class="time-caption-box">Time :</span><span class="time-value-box">08:00 AM</span>
                     </h4>
                  </div>
               </div> -->
              
            </div>
         </div>
          <div id="menu4" class="tab-pane fade">
                        <div class="checkin-wrapper-fourth-tab">
                            @if(!empty($card_detail))
                            <form>
                                <label class="fourth-tab">Card Number</label>
                                <!-- <input type="number" name="" class="form-control fourth-tab-input-box" placeholder="1233 34343 6565 4545"> -->
                                <div class="form-control fourth-tab-input-box">{{$card_detail->user_card_number}}</div>
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                        <label class="fourth-tab">Expiry Month</label>
                                        
                                         <div class="form-control fourth-tab-input-box">@if(strlen($card_detail->card_exp_month)=='1') 0{{$card_detail->card_exp_month}} @else {{$card_detail->card_exp_month}} @endif</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="fourth-tab">Expiry Year</label>
                                        <!-- <input type="number" name="" class="form-control fourth-tab-input-box" placeholder="2027"> -->
                                         <div class="form-control fourth-tab-input-box">{{$card_detail->user_card_exp_year}}</div>
                                    </div>
                                </div>

                            </form>
                            @else
                                <form>
                                <label class="fourth-tab">Card Number</label>
                                <!-- <input type="number" name="" class="form-control fourth-tab-input-box" placeholder="1233 34343 6565 4545"> -->
                                <div class="form-control fourth-tab-input-box"></div>
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                        <label class="fourth-tab">Expiry Month</label>
                                         <div class="form-control fourth-tab-input-box"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="fourth-tab">Expiry Year</label>
                                        <!-- <input type="number" name="" class="form-control fourth-tab-input-box" placeholder="2027"> -->
                                         <div class="form-control fourth-tab-input-box"></div>
                                    </div>
                                </div>

                            </form>
                            @endif

                            
                        </div>
                </div>
      </div>
   </div>
</section>
<script>  
   
   function cancle_subscription(){
       swal({
      'title' : "Are you sure you want cancel your subscription!",
      'text'  : "if you cancel this plan, all the remaining visits will be lost",
      'icon'  : "warning",
      buttons: ["No", "Yes, Cancel"],
      dangerMode : true,
    }).then(function(isConfirm){
      if (!isConfirm) {
        $("#checkbox").prop("checked", true);
        return false;
      } else {
     $.ajax({
            url:"{{route('cancel_subscription')}}",
            data: {'_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              if (data.flag) {
                  swal({
                        title : "Successfully!",
                        text  : data.message,
                        icon  : "success",
                      }).then(function(isConfirm){
                            location.reload();
                      });
              } else {
                swal({
                  title : "Error!",
                  text  : data.message,
                  icon  : 'error'
                });
              }
            }
           });
      }
    });
   }

   $('.three-cancel').click(function() {
       $(".bord").css("background-color", "transparent");
       $(".enable-class").prop("disabled", true);
        $(".three-cancel").hide();
         $(".update").hide();
       $(".edit").show();
   });
   
   $('.edit').click(function() {
       $(".bord").css("background-color", "#fff");
       $(".bord").css("padding", "0 5px");
       $(".enable-class").prop("disabled", false);
       $(".update").show();
       $(".three-cancel").show();
       $(".edit").hide();
   }); 
   
   $(".three-cancel").hide();
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
            url:"{{route('billing_histroy')}}",
            data: {},
            type: "get",
            success: function(data){
              $("#billing_detail").html(data);
            }
        });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
            url:"{{route('check_in_histroy')}}",
            data: {},
            type: "get",
            success: function(data){
              $("#gym_check_in_list").html(data);
            }
        });
  });
</script>
@endsection
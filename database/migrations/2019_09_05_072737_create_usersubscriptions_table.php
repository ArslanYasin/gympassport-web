<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersubscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('subscription_id')->unsigned();
            $table->integer('transaction_id');
            $table->timestamp('purchased_at')->useCurrent();
            $table->string('plan_name');
            $table->tinyInteger('plan_duration');
            $table->string('plan_time');
            $table->decimal('amount',10,2);
            $table->text('description');
            $table->tinyInteger('status')->default(1)->comment = '0 =>expired_plan , 1=>active_plan ,2=>upcoming_plan';
            $table->timestamps();
        });
        Schema::table('usersubscriptions', function($table) { 
            $table->foreign('subscription_id')->references('id')->on('subscriptions');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersubscriptions');
    }
}

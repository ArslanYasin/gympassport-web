@extends('web_user_dash.design')
@section('content')
<section class="booking-details-wrapper">
      <div class="container-fluid">
        <div class="sub-plan-details-box1">
          <form action="{{route('sub_plan_detail')}}" method="post">
                {{ csrf_field() }}
          <div class="first-wrapper">
      <div class="key-value-div-sub-plan">
          <h4 class="same-h4-sub-plan">Plan Name</h4>
        </div>
        <div class="key-value-div-sub-plan value-sub-plan">
          <input type="text" name="plan_name" class="payable-value-sub-plan border-class" placeholder="Ultimate Fitness Pass" value="{{$membership_detail['plan_cat_name']}}" disabled>
        </div>
        <div class="key-value-div-sub-plan">
           <h4 class="same-h4-sub-plan">Plan type</h4>
        </div>
        <div class="key-value-div-sub-plan value-sub-plan list-icon-sub-plan">
          <select class="list-sub-plan" id="plan_type" name="plan_id">
             <option value="Master-card" class="master-card" >Select plan type</option>
              @if($membership_detail['plan_detail'])
                @foreach($membership_detail['plan_detail'] as $key=>$value)
                  <option value="{{$value['plan_id']}}" class="master-card">{{$value['visit_pass']}}</option>
                @endforeach
              @endif
          </select>
        </div>
       <div class="key-value-div-sub-plan">
          <h4 class="same-h4-sub-plan">Plan Amount</h4>
          </div>
          <div class="key-value-div-sub-plan value-sub-plan">
            <input type="text" name="amount" id="amount" value=""  class="payable-value-sub-plan border-class" placeholder="$0" style="width: 30%;" disabled>
           </div>
           <div class="key-value-div-sub-plan">
            <h4 class="same-h4-sub-plan">Plan Description</h4>
            </div>
            
             <!-- <div class="col-sm-12"> -->
                <div class="purchase-plan-btn-box">
                  <textarea rows="4" cols="3" id="text" class="textarea" placeholder="plan description"></textarea>
                </div>
              <!-- </div> -->
         <div class="purchase-plan-btn-box">
                  <button type="submit" class="btn btn-danger purchase-plan-btn">Purchase Plan</button>
              </div>
              </div>
          </form>
          </div>
         </div>
    </section>
  <script>
         $(document).ready(function(){
      $(".trigger-navbar").click(function(){
        $(".custom-navbar-book-det").slideToggle();
      });
      });
  
  </script>
  <script type="text/javascript">
    $('#plan_type').change(function(){
        var plan_id = this.value;
      if(plan_id !=''){
           $.ajax({
            url:"{{route('ajax_price')}}",
            data: {'plan_id':plan_id, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              //alert(data);
              var value = JSON.parse(data);
              $('#amount').val(value.amount);
              $('textarea').val(value.description);  
              $(':input[type="submit"]').prop('disabled', false);
            }
        });
      }
        
    });
   </script>
   <script type="text/javascript">
     $(document).ready(function() {
     $(':input[type="submit"]').prop('disabled', true);
 });
   </script>

@endsection
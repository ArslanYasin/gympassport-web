<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiCommanFunctionController extends Controller
{
     public function sendResponse($status,$result, $message)
    {
    	$response['data'] = [
            'status' => $status,
            'message' => $message,
            'result'    => $result,
        ];
        //$data = array($response);
        return response()->json($response, 200);
    }
    
    public function json_sendResponse($status,$result, $message,$subscribe)
    {
    	$response['data'] = [
            'status' => $status,
            'message' => $message,
            'is_subscribed' => $subscribe,
            'result'    => $result,
        ];
        //$data = array($response);
        return response()->json($response, 200);
    }

    public function checkIngym_sendResponse($status,$result, $message,$total_visit,$subscribe)
    {
        $response['data'] = [
            'status' => $status,
            'message' => $message,
            'total_visit'=>$total_visit,
            'is_subscribed' => $subscribe,
            'result'    => $result,
        ];
        //$data = array($response);
        return response()->json($response, 200);
    }

    public function sendError_arr($error, $errorMessages = [], $code = 200)
    {
        // $erroree =array();
        $response['data'] = [
            'status' => 0,
            'message' => $error,
            'result' => []
        ];
        if(!empty($errorMessages)){
            $response['data'] = $error;
        }
        return response()->json($response, $code);
    }

    public function sendError_obj($error, $errorMessages = [], $code = 200)
    {
        // $erroree =array();
        $response['data'] = [
            'status' => 0,
            'message' => $error,
            'is_subscribed' => (boolean)0,
            'result' => (object)[]
        ];
        if(!empty($errorMessages)){
            $response['data'] = $error;
        }
        return response()->json($response, $code);
    }
    public function get_post_array($req_keys = array()){
        $data = array();
        foreach ($req_keys as $key => $value) {
            echo 'jkhcdskc= '.$key;
            if(in_array($key, $req_keys)){
                if($value == ''){
                    echo $key;
                    return $key.' field is require';
                }
            }
            $data[$key] = $value;
        }
        if(count($data)<=0){
            return array();
        }
        return $data;
    }
}

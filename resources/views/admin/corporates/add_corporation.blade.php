@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Corporation Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="#">Corporate-List</a></li>
                <li class="active">add-corporation</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('store-corporation')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                            <!--  <input type="hidden" name="user_type" value="2"> -->
                                <div class="box-body">


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Name<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{ old('first_name') }}" placeholder="Enter Corporate Name">
                                            @if ($errors->has('first_name'))
                                                <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Company<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('corporate_name')) help-block @endif" id="corporate_name" name="corporate_name" placeholder="Enter Company Name" value="{{ old('corporate_name') }}">
                                            @if ($errors->has('corporate_name'))
                                                <strong class="help-block"> {{ $errors->first('corporate_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Employees<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('no_of_employees')) help-block @endif" id="no_of_employees" name="no_of_employees" placeholder="Enter No of Employees" value="{{ old('no_of_employees') }}">
                                            @if ($errors->has('no_of_employees'))
                                                <strong class="help-block"> {{ $errors->first('no_of_employees') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Email<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('password')) help-block @endif" id="password" name="password" placeholder="Enter password" value="{{ old('password') }}">

                                            @if ($errors->has('password'))
                                                <strong class="help-block"> {{ $errors->first('password') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Phone Number<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control @if($errors->has('phone_number')) help-block @endif" id="phone_number" name="phone_number" placeholder="Enter Phone Number" value="{{ old('phone_number') }}">
                                            @if ($errors->has('phone_number'))
                                                <strong class="help-block"> {{ $errors->first('phone_number') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Address<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control @if($errors->has('address')) help-block-text @endif" name="address" id="address" placeholder="Enter address">{{ old('address') }}</textarea>
                                            @if ($errors->has('address'))
                                                <strong class="help-block"> {{ $errors->first('address') }}</strong>
                                            @endif

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Plan<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('plan_id')) help-block @endif" onchange="planChange(this.value)" id="plan_id" name="plan_id">
                                                @if(!empty($city))
                                                    <option value=""> Plan</option>
                                                    @foreach($plan as $key=>$value)
                                                        @php $get = $value->plan_cat_name==old('plan_id') ? "selected" :"";@endphp
                                                        <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->plan_cat_name}}</option>
                                                    @endforeach
                                                @else
                                                    <option value=""> Plan Unavailable</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('plan_id'))
                                                <strong class="help-block"> {{ $errors->first('plan_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Subscription<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('subscription_id')) help-block @endif" id="subscription_id" name="subscription_id">
                                                <option value=""> Select Subscription</option>
                                            </select>
                                            @if ($errors->has('subscription_id'))
                                                <strong class="help-block"> {{ $errors->first('subscription_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">


        function planChange(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('get-subscription')}}";
            data = {plan: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {

                    html = '';
                    for (i = 0; i < response.length; i++) {
                        html += "<option value='" + response[i].id + "'>" + response[i].plan_duration + ' ' + response[i].plan_time + "</option>";
                    }
                    $("#subscription_id").html(html);
                }, error: function (err) {

                }

            });
        }

    </script>
@endsection

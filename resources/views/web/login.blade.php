<!DOCTYPE html>
<html>
   <head>
      <title>Login</title>
      <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
      <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
      <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style type="text/css">
        .tnc.row{
          float: left;
          width: 100%;
        }
        .signup-checkbx{
          width: 65%;
        }
        .login-pg-btn-box{
          width: 35%;
          float: right;
        }
        button.btn.btn-danger.a-login-btn{
          float: right;
          width: 100%;
          text-align: right;
          margin-top: 38px;
        }
      </style>

   </head>
   <body>
     
      <header class="head-signup">
         <div class="container-fluid">
            <div class="icon-signup">
               <a href="{{url('/')}}"><img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="icon" class="img-fluid signup-icon"></a>
            </div>
         </div>
      </header>
      <section class="login-left-part">
         <img src="{{ asset('website_file/images/login-pg.png')}}" alt="newpass-page-image" class="img-fluid login-left-img">
<!--         <div class="login-stores-icon">
            <a href="#" target="_blank"><img src="{{ asset('website_file/images/app-store.png')}}" alt="newpass-page-image" class="img-fluid apple-store-image"></a>
            <a href="#" target="_blank"><img src="{{ asset('website_file/images/google-store.png')}}" alt="newpass-page-image" class="img-fluid google-store-image"></a>
         </div>-->
      </section>
      <section class="login-right-part">
         @if(Session::has('message'))
         <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {!! session('msg') !!}
         </div>
         @endif 
         <h5 class="newpass-heading">Login</h5>
         <form action="{{ route('signin.submit')}}" method="post">
            {{ csrf_field() }}
            <label class="newpass-labels">Email Address</label>
            <input type="email" name="email" id="email" placeholder="gympassport@gmail.com" class="login-form" autocomplete="off" value="{{old('email')}}">
            @if ($errors->has('email'))<p class="error">{{ $errors->first('email') }}</p>@endif
            <label class="newpass-labels">Password</label>
            <div class="pass-box-wrapper">
               <input type="password" name="password" id="password" class="login-form" placeholder="*********" id="pwd" autocomplete="off" value="{{old('password')}}">
               <p class="show-hide-para-login" id="pwdbtn">Show</p>
               @if ($errors->has('password'))<p class="error">{{ $errors->first('password') }}</p>@endif
            </div>
            <!--  <div class="forgot-password-btn-box">
               <button type="button" class="forgot-password-btnn">Forgot Password?</button>
               </div> -->
            <div class="tnc row">
               <div class="signup-checkbx">
                <input type="checkbox" id="signupcheckbx"  name="checked" value="1">
                <label for="signupcheckbx">
                   <span></span>
                </label>
                 <h4>Remember me</h4>
                </div>
                <div class="login-pg-btn-box">
<!--               <a href="{{route('singup')}}"><button type="button" class="btn btn-danger login-create-acc-btn">Create Account</button></a>-->
              <button type="submit" class="btn btn-danger a-login-btn">Login</button>
            </div>
            </div>
            <div class="forgot-password-btn-box">
               <a href="{{route('forgate_password')}}" class="forgot-password-btnn">Forgot Password?</a>
            </div>
            
           
         </form>

         <label class="newpass-labels" style="color:red;font-size: 17px; color: #be0027;"><b style="font-size: 30px;">•</b> Users please download our mobile app to create an account</label>
         <label class="newpass-labels" style="color:red;font-size: 16px; color: #be0027;"><b style="font-size: 30px;">•</b> Gym Owners please login here to monitor and track users</label>

         <!--         <div class="store">
             <a href="#" target="_blank"><img src="{{ asset('website_file/images/app-store.png')}}" alt="newpass-page-image" class="img-fluid apple-store-image"></a>
             <a href="#" target="_blank"><img src="{{ asset('website_file/images/google-store.png')}}" alt="newpass-page-image" class="img-fluid google-store-image"></a>
         </div>-->
      </section>
       
      <script>
         $(document).ready(function(){
         $('#pwdbtn').click(function(){
             //alert('sd');
           if($('#password').attr('type') == 'password'){
             $('#password').attr("type","text");
             $(this).text("hide");
           }else {
             $('#password').attr("type","password");
             $(this).text("show");
           }
         })
         });
      </script>
       <script>
            $(function() {
 
                if (localStorage.chkbx && localStorage.chkbx != '') {
                    $('#signupcheckbx').attr('checked', 'checked');
                    $('#email').val(localStorage.usrname);
                    $('#password').val(localStorage.pass);
                } else {
                    $('#signupcheckbx').removeAttr('checked');
                    $('#email').val('');
                    $('#password').val('');
                }
 
                $('#signupcheckbx').click(function() {
 
                    if ($('#signupcheckbx').is(':checked')) {
                        // save username and password
                        localStorage.usrname = $('#email').val();
                        localStorage.pass = $('#password').val();
                        localStorage.chkbx = $('#signupcheckbx').val();
                    } else {
                        localStorage.usrname = '';
                        localStorage.pass = '';
                        localStorage.chkbx = '';
                    }
                });
            });
 
        </script>
   </body>
</html>
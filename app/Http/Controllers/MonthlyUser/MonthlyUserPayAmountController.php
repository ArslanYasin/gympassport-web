<?php

namespace App\Http\Controllers\MonthlyUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Usersubscription;
use App\Uservisitedgym;

class MonthlyUserPayAmountController extends Controller
{

    function __construct() {
        $this->uvg = new Usersubscription;
    }

    public function get_user(){
        //echo'sd';die;
        $Get_monthly_user = $this->uvg->Last_Month_user_data();
        foreach($Get_monthly_user as $key=>$value){
            $total_payable_amount = $value->amount;
            $total_visit = $value->total_visit;
            $amount = $this->given_amount_after_month($total_payable_amount, $total_visit);
            if(!empty($amount)){
                $sub_id = $value->id;
                $update = Uservisitedgym::where('usersubscription_id',$sub_id)->update(['gym_earn_amount'=>$amount]);
                //echo 'update';
            }
        }
        //echo'<pre>';print_r($Get_monthly_user);die;
    }

    public function given_amount_after_month($user_payable_amount, $total_visit){
            if($user_payable_amount != '0'){
                $for_patner_percentage = 81;
                $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
                $amount_for_patner = ($for_final_distribute_amount/$total_visit);
                $final_amount_for_gym_patner = number_format((float) $amount_for_patner, 2, '.', '');
            }else{
                $final_amount_for_gym_patner ='';
            }
            
            return $final_amount_for_gym_patner;
        
    }
    
}

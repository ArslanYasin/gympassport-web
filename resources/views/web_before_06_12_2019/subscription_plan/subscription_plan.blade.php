@extends('web_user_dash.design')
@section('content')
<style type="text/css">
.link-venues a {
    background-color: #f1f1f100 !important;
}
</style>
<section class="subsciption-section">
  <div class="container-fluid">
    <div class="subscription-plan-page-box">
    <!--   <div class="box-one-subscription">
        <img src="{{ asset('website_file/images/grey2.png')}}" alt="image" class="img-fluid">
        <h4 class="saver-sub">Saver Fit Pass</h4>
        <p class="access-to">Access to</p>
        <span class="twenty">20</span><span class="ufp-partner"> UFP partnered clubs</span>
        <div class="link-venues">
          <a href="#" data-toggle="modal" data-target="#myModal">Click here to see all venues</a>
          <div class="next-part-sub">
            <div class="white-section">
              <h4>Choose your No. of Visits</h4>
              <div class="for-partition">
                <p class="ab pass-num">1 Visit Pass</p>
                <p class="ab pass-value">5$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">5 Visit Pass</p>
                  <p class="ab pass-value">10$</p>
                </div>
                <p class="ab pass-num">10 Visit Pass</p>
                <p class="ab pass-value">15$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">Monthly Visit Pass</p>
                  <p class="ab pass-value">250$</p>
                </div>
              </div>
            </div>
          </div>
          <div class="button-sub-box">
            <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
          </div>
        </div>
      </div> -->
  @if(!empty($membership))
  	@foreach($membership as $key=>$value)
      <div class="box-one-subscription">
        <img src="{{ asset('website_file/images/grey2.png')}}" alt="image" class="img-fluid">
        <h4 class="saver-sub">{{$value['plan_cat_name']}}</h4>
        <p class="access-to">Access to</p>
        <span class="twenty">{{$value['total_gym']}}</span><span class="ufp-partner"> UFP partnered clubs</span>
        <div class="link-venues">
        	<!-- data-toggle="modal" data-target="#myModal" -->
          <a href="#" onclick="myfunction('{{$value["plan_cat_id"]}}');" >Click here to see all venues</a>
          <div class="next-part-sub">
            <div class="white-section">
              <h4>Choose your No. of Visits</h4>
              <div class="for-partition">
              	@php $i=0;@endphp
	              	@foreach($value['plan_detail'] as $key=>$plan)
		              	@if($i%2=='0')
			                <p class="ab pass-num">{{$plan['visit_pass']}}</p>
			                <p class="ab pass-value">{{$plan['amount']}}$</p>
		                @else
			                <div class="bg-shadow">
			                  <p class="ab pass-num ">{{$plan['visit_pass']}}</p>
			                  <p class="ab pass-value">{{$plan['amount']}}$</p>
			                </div>
		                @endif
	                @php $i++;@endphp
                @endforeach
                <!-- <p class="ab pass-value">5$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">5 Visit Pass</p>
                  <p class="ab pass-value">10$</p>
                </div>
                <p class="ab pass-num">10 Visit Pass</p>
                <p class="ab pass-value">15$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">Monthly Visit Pass</p>
                  <p class="ab pass-value">250$</p>
                </div> -->
              </div>
            </div>
          </div>
          <div class="button-sub-box">
           <a href="{{route('sub_plan',['plan_id'=>$value['plan_cat_id']])}}"><button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button></a>
          </div>
        </div>
      </div>
      @endforeach
  @endif

  <script type="text/javascript">
  	function myfunction(plan_id){
  		var search = '';
  		//alert(plan_id);
  		 $.ajax({
            url:"{{route('plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
            	//alert(data);
            	$('#gym_list').html(data);
            	$('#myModal').modal('show');
            }
        });
  	}
  </script>
  <script type="text/javascript">
  		function search(){
  		var search = $('#searchdata').val();
  		var plan_id = '';
  		 $.ajax({
            url:"{{route('plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
  	}
  </script>
  <script type="text/javascript">
  	function keyupfunction(){
  		var search = $('#searchdata').val();
  		var plan_id = '';
  		 $.ajax({
            url:"{{route('plan_wise_gym_list')}}",
            data: {'plan_id':plan_id, 'search':search, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $('#gym_list').html(data);
            }
        });
  	}
  </script>
    <!--   <div class="box-one-subscription">
        <img src="{{ asset('website_file/images/grey2.png')}}" alt="image" class="img-fluid">
        <h4 class="saver-sub">Saver Fit Pass</h4>
        <p class="access-to">Access to</p>
        <span class="twenty">20</span><span class="ufp-partner"> UFP partnered clubs</span>
        <div class="link-venues">
          <a href="#">Click here to see all venues</a>
          <div class="next-part-sub">
            <div class="white-section">
              <h4>Choose your No. of Visits</h4>
              <div class="for-partition">
                <p class="ab pass-num">1 Visit Pass</p>
                <p class="ab pass-value">5$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">5 Visit Pass</p>
                  <p class="ab pass-value">10$</p>
                </div>
                <p class="ab pass-num">10 Visit Pass</p>
                <p class="ab pass-value">15$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">Monthly Visit Pass</p>
                  <p class="ab pass-value">250$</p>
                </div>
              </div>
            </div>
          </div>
          <div class="button-sub-box">
            <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
          </div>
        </div>
      </div> -->
     <!--  <div class="box-one-subscription">
        <img src="{{ asset('website_file/images/grey2.png')}}" alt="image" class="img-fluid">
        <h4 class="saver-sub">Saver Fit Pass</h4>
        <p class="access-to">Access to</p>
        <span class="twenty">20</span><span class="ufp-partner"> UFP partnered clubs</span>
        <div class="link-venues">
          <a href="#">Click here to see all venues</a>
          <div class="next-part-sub">
            <div class="white-section">
              <h4>Choose your No. of Visits</h4>
              <div class="for-partition">
                <p class="ab pass-num">1 Visit Pass</p>
                <p class="ab pass-value">5$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">5 Visit Pass</p>
                  <p class="ab pass-value">10$</p>
                </div>
                <p class="ab pass-num">10 Visit Pass</p>
                <p class="ab pass-value">15$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">Monthly Visit Pass</p>
                  <p class="ab pass-value">250$</p>
                </div>
              </div>
            </div>
          </div>
          <div class="button-sub-box">
            <button type="submit" class="btn btn-danger subc-btn">Subscribe Now</button>
          </div>
        </div>
      </div> -->
     <!--  <div class="box-one-subscription">
        <img src="{{ asset('website_file/images/red2.png')}}" alt="image" class="img-fluid">
        <h4 class="saver-sub">Saver Fit Pass</h4>
        <p class="access-to">Access to</p>
        <span class="twenty">20</span><span class="ufp-partner"> UFP partnered clubs</span>
        <div class="link-venues red-link">
          <a href="#">Click here to see all venues</a>
          <div class="next-part-sub">
            <div class="white-section">
              <h4>Choose your No. of Visits</h4>
              <div class="for-partition">
                <p class="ab pass-num">1 Visit Pass</p>
                <div class="bg-shadow">
                  <p class="ab pass-value red-box">5$</p>
                  <p class="ab pass-num">5 Visit Pass</p>
                </div>
                <p class="ab pass-value red-box">10$</p>
                <p class="ab pass-num">10 Visit Pass</p>
                <p class="ab pass-value red-box">15$</p>
                <div class="bg-shadow">
                  <p class="ab pass-num">Monthly Visit Pass</p>
                  <p class="ab pass-value red-box">250$</p>
                </div>
              </div>
            </div>
          </div>
          <div class="button-sub-box">
            <button type="submit" class="btn btn-danger subc-btn red-btn">Subscribe Now</button>
          </div>
        </div>
      </div> -->
    </div>
  </div>
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top: 127px;">
      <div class="modal-content">
        <div class="modal-body">
          <div class="popup">
            <button type="button" class="btn btn-default cross-btn" data-dismiss="modal">X</button>
            <div class="popup-box-heading">
              <h4>Ultimate Fit Pass</h4>
            </div>
            <div class="search-wrapper">
              <div class="wrap">
                <div class="search">
                  <input type="text" name="searchdata" id="searchdata" onkeyup="keyupfunction();" class="searchTerm" placeholder="Search GYM Name">
                  <button type="button" id ="search" class="searchButton" onclick="search();">
                  </button>
                </div>
              </div>
            </div>
            <div class="row-fst" id="gym_list">
           <!--    <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div> -->
              <!--<div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
              <div class="card-box">
                <img src="{{ asset('website_file/images/sub-popup-img.png.png')}}" alt="image" class="img-fluid">
                <h3 class="gym-name">Cross Fit Gym</h3>
                <div class="address-box">
                  <p>PQR Sector,</p>
                  <p>ABC Street,</p>
                  <p>Noida, UP, India</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> -->
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function(){
  	$(".trigger-navbar").click(function(){
  		$(".custom-navbar").slideToggle();
  	});
  	});
</script>
@endsection
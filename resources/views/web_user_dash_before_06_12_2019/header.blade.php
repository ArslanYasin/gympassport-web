 <header class="head">
  <style type="text/css">
    .pro-image{
      width: 100px;
      height: 68px;
      border-radius: 10px;
    }
  </style>
    <div class="container-fluid">
      <!-- <div class="icon">
          <img src="images/logo.png" alt="icon" class="img-fluid logo-ufp">
      </div> -->
      <div class="icon">
            <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="logo-image img-fluid">
        </div>
      <div class="custom-navbar-three">
        <ul>
                  
                    @if(Auth::user()->user_type=='3')
                      <li class="@if(request()->route()->getName()=='userhome' || request()->route()->getName()=='UserPasswordChange') active @endif"><a href="{{route('userhome')}}">Home</a></li>
                    <li class="@if(request()->route()->getName()=='findgym')active @endif"><a href="{{route('findgym')}}">Find Gym</a></li>
                <!--     <li><a href="#">Become a Partner</a></li> -->
                    <li><a href="{{route('subscriptionplan')}}">Membership Plans</a></li>
                    @else
                     <li class=""><a href="{{route('ufppatnerhome')}}">Home</a></li>
                     <li class=""><a href="#">Check-in</a></li>
                     <li><a href="{{route('paymenthistory')}}">Payments</a></li>
                    @endif
                    <li><a href="{{route('contactus')}}">Contact us</a></li>
                </ul>
      </div>
      <div class="trigger-navbar">
        <span></span>
      </div>
      
      <div class="image">
      <!--    <img src="{{ asset('website_file/images/user-pic.png')}}" alt="profile-image"> -->
      @if(isset(Auth::user()->user_image))

         <img src="http://localhost/ultimateFitness/{{Auth::user()->user_image}}" class="pro-image" alt="profile-image">
      @else
         <img src="{{ asset('website_file/images/user-pic.png')}}" alt="profile-image">
      @endif
      </div>
      <div class="popup-paymentaa">
             <div class="profile-paymentaa"><a href="#">Profile</a></div>
              <div class="logout-paymentaa"> <a href="{{ route('signout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" >Log out</a>
                      <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                      </form></div>
      </div>
      </div>
  </header>
<script>
      $(document).ready(function(){
      $(".image").click(function(){
        $(".popup-paymentaa").toggle();
      });
      });

    $(document).ready(function(){
      $(".trigger-navbar").click(function(){
        $(".custom-navbar").slideToggle();
      });
      });
</script>
@extends('web_user_dash.design')
@section('content')
<style type="text/css">
	.placeholder{
		color: #fff;
	}
</style>
	 <section class="scc">
	 	  {!! $map['js'] !!}
		<div class="container-fluid">
	        <div class="row ">
	        	 {!! $map['html'] !!}
<div id="directionsDiv"></div>
	    		<div class="col-sm-2">
		          <!--  <div class="list-view"><a href="
		           	map-coll-page2.html"><button class="btn-list-view">List View</button><span class="gt-symbol">&gt</span></a></div> -->
		           	<div class="list-view"><button class="btn-list-view">List View</button><span class="gt-symbol">&gt</span></div>
                </div>
                <div class="col-sm-4">
                             <div class="first-part">
							<div class="search-container">
								<input type="text" placeholder="Search your UFP Gym" name="search" class="placeholder"><span><i class="fa fa-search search-icon"></i></span>
							</div>


							 <div class="range">
							    <input type="range" min="0" max="5" steps="1" value="0">
							  </div>

							  <ul class="range-labels">
							    <li class="active selected"><span class="speed">20</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">40</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">60</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">80</span><span class="kil-meters">Km</span></li>
							    <li><span class="speed">100</span><span class="kil-meters">Km</span></li>
							  </ul>
						    
							<div class="map-coll-box-scroll">
							<div class="box-one">
								<div class="row">
									<div class="col-7 col-sm-7 col-md-7 col-lg-7">
										<div class="left-part">
										    <h2>Cross Fit</h2>
											<h3>Sector 63, Noida</h3>
											<button type="button" class="distance-button"><p>5 Km Away</p></button>
									   </div>
									</div>
									<div class="col-5 col-sm-5 col-md-5 col-lg-5">
										<div class="image-man"><img src="{{ asset('website_file/images/man-img-map-col.png')}}" alt="man-image">
										</div>
									   <div class="view-det"><a href="#">View Details</a></div>
									 <!--  <div class="view-det">View Details</div> -->
									   
									</div>
								</div>
							</div>
							<div class="box-two">
								<div class="row">
									<div class="col-7 col-sm-7 col-md-7 col-lg-7">
										<div class="left-part">
											<h2>ABC Gym</h2>
										    <h3>Sector 63, Noida</h3>
										    <button type="button" class="distance-button"><p>5 Km Away</p></button>
									    </div>
								    </div>
									<div class="col-5 col-sm-5 col-md-5 col-lg-5">
										<div class="right-part">
											<div class="image-man"><img src="{{ asset('website_file/images/man-img-map-col.png')}}" alt="man-image">
											</div>
											<div class="view-det"> <a href="#">View Details</a>
											</div>
									    </div>
								   </div>
							    </div>
						   </div>
						</div>
							<div class="bar"><span></span>
							</div>
						  <div class="row">
								<div class="col-8 col-sm-8 col-md-8 col-lg-8">
								    <div class="result"><h6>Showing results 1-10</h6></div>
								</div>
							</div>
							
					       </div>
					   </div>
					   <div class="col-sm-2">
					   <div class="second-part">
					       	  <a href="#"><span><div class="lt-symbol">&lt</div></span></a>
					       </div>
					   </div>
					       
					       <!-- <div class=""> -->
			<div class="col-sm-8">
			<div class="left-part-mapview-gymdetails">
				<div class="cross-fit"><h5>Cross Fit</h5></div>
				<div class="mapview-box-for-scroll">
				<div class="four-images">

			<div class="owl-carousel owl-theme" id="slider1">
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                  <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                  <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-1.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                   <div class="item">
                   	 <div class="blank-photo">
				 <img src="{{ asset('website_file/images/image-2.png')}}" alt="profile-image">
				</div>
                   </div>
                 <!--   <div class="item">
                   	 <div class="blank-photo">
				 <img src="images/image-2.png" alt="profile-image">
				</div>
                   </div> -->
            </div>
			<div class="hr-bar-top"><span></span>
							</div>
				</div>
				<div class="row">
					<div class="col-4 col-sm-4">
						<img src="{{ asset('website_file/images/direction.png')}}" alt="get-direction-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Get Direction</p>
					</div>
					<div class="col-4 col-sm-4">
						<img src="{{ asset('website_file/images/contact-us.png')}}" alt="contact-us-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Contact Us</p>
					</div>
					<div class="col-4 col-sm-4">
						<div class="check-in-wrapper">
						<img src="{{ asset('website_file/images/check-ins.png')}}" alt="check-ins-icon" class="icon-gymdetails img-fluid">
						<p class="for-info">Total Check-In's</p>
					     </div>
					</div>
				</div>
				<div class="hr-bar-top"><span></span>
				</div>

				<div class="row m-0">
					<h5 class="four-a">About Us</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
					</div>
				</div>
			    <div class="row m-0">
			    	<h5 class="four-a">Address</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
					</div>
				</div>
				<div class="row m-0">
					<h5 class="four-a">Activities</h5>
					<div class="col-sm-12">
						<p class="four-a-stmt">Weight Loosing Exercise, Body Fit</p>
					</div>
				</div>
				    <h5 class="four-a">Available Days</h5>
					<div class="schedule">
				<div class="row m-0">
                     <div class="col-sm-12">
                        <div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Sunday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								   <p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Monday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								   <p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Tuesday</p>
							</div>
							<div class="col-4 col-sm-3">
							<div class="time-box">
								   <p class="time">10:00 AM</p>
							</div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Wednesday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Thursday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
						     	</div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Friday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
				    </div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-12 col-sm-3">
								<p class="days">Saturday</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
								<p class="time">10:00 AM</p>
							    </div>
							</div>
							<div class="col-4 col-sm-3">
								<p class="dash">-</p>
							</div>
							<div class="col-4 col-sm-3">
								<div class="time-box">
							    <p class="time">10:00 PM</p>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>
		</div>
		<div class="col-sm-2">
			
			<div class="right-part-third-page">
				<a href="#"><span><div class="lt-symbol">&lt</div></span></a>
			</div>
		
		</div>
            </div>
        </div>
    </section>
        <script>
    	$(document).ready(function(){
			$(".image").click(function(){
				$(".popup-paymentaa").toggle();
			});
			});

		$(document).ready(function(){
			$(".trigger-navbar").click(function(){
				$(".custom-navbar").slideToggle();
			});
			});
		$(document).ready(function(){
			$(".list-view").click(function(){
				$(".first-part").show();
				$(".second-part").show();
				$(".list-view").hide();
			});
			});
		$(document).ready(function(){
			$(".view-det").click(function(){
				$(".first-part").hide();
				$(".left-part-mapview-gymdetails").show();
				$(".right-part-third-page").show();
			});
			});
		$(document).ready(function(){
			$(".right-part-third-page").click(function(){
				$(".first-part").show();
				$(".left-part-mapview-gymdetails").hide();
				$(".right-part-third-page").hide();
			});
			});
		$(document).ready(function(){
			$(".second-part").click(function(){
				$(".list-view").show();
				$(".first-part").hide();
				$(".second-part").hide();
			});
			});
		
		$('#slider1').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:false,
		    dots:true,
		    responsive:{
		        0:{
		           items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		})
    </script>
@endsection
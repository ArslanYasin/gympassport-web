<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gymimage extends Model
{
    protected $table ="gym_image";
    
    protected $fillable = ['users_gym_id','gym_image'];
}

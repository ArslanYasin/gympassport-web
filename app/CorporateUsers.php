<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorporateUsers extends Model
{
    protected $table = 'corporate_users';

    protected $fillable = ['corporate_id', 'user_id', 'status', 'is_delete'];

    public function corporateuser($user_id)
    {
        $data = self::select('id', 'corporate_id', 'status', 'is_delete', 'user_id')->where('user_id', $user_id)->orderBy('id', 'desc')->first();
        return $data;
    }
}

@extends('web_user_dash.design')
@section('content')
<style type="text/css">
.password{
    width: 100%;
    max-width: 52%;
    color: #fff;
    font-size: 16px;
    font-weight: 600;
    text-align: left;
    margin: 0px auto;
    color: red;
}
</style>
	<section class="change-password-wrapper h-200">
			<div class="container">
				<div class="row m-0">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					@if(Session::has('message'))
							@if(session('message')=='alert-danger')
					          <script type="text/javascript">
					            swal({
					                  title : "{!! session('msg') !!}",
					                  text  :  "",
					                  icon  : 'error'
					                });
					          </script>
					         @else
					           <script type="text/javascript">
					            swal({
					                  title : "{!! session('msg') !!}",
					                  text  :  "",
					                  icon  : 'success'
					                });
					          </script>
					         @endif
						<!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! session('msg') !!}
						</div> -->
					@endif
					<form action="{{ isset($staff)?route('update_staff'):route('store_staff') }}"  method="post">
						 {{ csrf_field() }}

{{--						<input type="text" hidden name="gym_user_id"  value="{{\Illuminate\Support\Facades\Auth::id()}}">--}}
{{--						<input type="text" hidden name="user_type"  value="4">--}}

						<div class="main-part text-center">
						   <h2 class="password-heading">First Name</h2>
						   <input type="text" name="first_name" class="placeholder-clr" placeholder="Enter your First Name" value="{{ isset($staff)?$staff->first_name: '' }}">
						   @if ($errors->has('first_name'))<label class="error first name" for="">{{ $errors->first('first_name') }}</label>@endif

						   <h2 class="password-heading">Last Name</h2>
						   <input type="text" name="last_name" class="placeholder-clr" placeholder="Enter your Last Name" value="{{ isset($staff)?$staff->last_name: '' }}">
						   @if ($errors->has('last_name'))<label class="error last name" for="">{{ $errors->first('last_name') }}</label>@endif
							
						   <h2 class="password-heading">Email</h2>
						   <input type="email" name="email" class="placeholder-clr" placeholder="Enter your Email" value="{{ isset($staff)?$staff->email: '' }}">
						   @if ($errors->has('email'))<label class="error email" for="">{{ $errors->first('email') }}</label>@endif

							<h2 class="password-heading">New Password</h2>
								<input type="password" name="password" class="placeholder-clr" placeholder="Enter  password" value="{{ old('password') }}">
								   @if ($errors->has('password'))<label class="error password" for="">{{ $errors->first('password') }}</label>@endif
							<h2 class="password-heading">Confirm New Password</h2>
								<input type="password" name="password_confirmation" class="placeholder-clr" placeholder="Enter Confirm password" value="{{ old('password_confirmation') }}">
								   @if ($errors->has('confirm_password'))<label class="error confirm password" for="">{{ $errors->first('password_confirmation') }}</label>@endif
					   </div>
						<button type="submit" class="btn btn-danger change-password-save-btn">{{ isset($staff)?"Update": 'Save' }}</button>

					</form>
				    </div>
				</div>
			</div>
			
	</section>
	<script>
	 $(document).ready(function(){
            $(".trigger-navbar").click(function(){
                $(".custom-navbar").slideToggle();
            });
            });
	</script>
@endsection
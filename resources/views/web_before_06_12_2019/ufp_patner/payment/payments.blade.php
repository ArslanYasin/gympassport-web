@extends('web_user_dash.design')
@section('content')
<!--    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/owl.carousel.min.css')}}"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{ asset('website_file/js/owl.carousel.min.js')}}"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<section class="pay-hist-section">
  <div class="pay-hist-menu-box">
    <ul class="nav nav-pills" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="pill" href="#menu-one">Bank Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="pill" href="#menu-two">Payment History</a>
      </li>
    </ul>
  </div>
  <style type="text/css">
    span.error {
      color: red;
    }
    input.form-control.value-paym{
          margin-bottom: 1px;
    }
  </style>
  <div class="tab-content">
    <div id="menu-one" class="container tab-pane active">
      <div class="main-box-bank-det">
        @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
         @endif
      @if(empty($bank_detail))
        <form action="{{route('add_bank_detaiil')}}" method="post">
           {{ csrf_field() }}
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">Account Number</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="number" name="account_number" class="form-control value-paym" placeholder="8974587125632548" value="{{old('account_number')}}">
               @if ($errors->has('account_number'))<span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
            </div>
          </div>
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">Account Holder Name</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="text" name="account_holder_name" class="form-control value-paym" placeholder="John Doe" value="{{old('account_holder_name')}}">
               @if ($errors->has('account_holder_name'))<span class="error" for="">{{ $errors->first('account_holder_name') }}</span>@endif
            </div>
          </div>
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">BSB Number</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="text" name="bsb_number" class="form-control value-paym" placeholder="ABC547865" value="{{old('bsb_number')}}">
              @if ($errors->has('bsb_number'))<span class="error" for="">{{ $errors->first('bsb_number') }}</span>@endif
            </div>
          </div>
          <div class="save-btn-paym-box">
            <button  type="submit" class="btn btn-danger save-btn-paym">Save</button>
          </div>
        </form>
      @else
        <form action="{{route('update_bank_detaiil',['id'=>$bank_detail->id])}}" method="post">
           {{ csrf_field() }}
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">Account Number</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="number" name="account_number" class="form-control value-paym" placeholder="8974587125632548" value="{{$bank_detail->account_number}}">
               @if ($errors->has('account_number'))<span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
            </div>
          </div>
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">Account Holder Name</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="text" name="account_holder_name" class="form-control value-paym" placeholder="John Doe" value="{{$bank_detail->account_holder_name}}">
               @if ($errors->has('account_holder_name'))<span class="error" for="">{{ $errors->first('account_holder_name') }}</span>@endif
            </div>
          </div>
          <div class="box-one-paym">
            <div class="bank-det">
              <h3 class="key-paym">BSB Number</h3>
            </div>
            <div class="bank-det value-box-paym">
              <input type="text" name="bsb_number" class="form-control value-paym" placeholder="ABC547865"  value="{{$bank_detail->bsb_number}}">
              @if ($errors->has('bsb_number'))<span class="error" for="">{{ $errors->first('bsb_number') }}</span>@endif
            </div>
          </div>
          <div class="save-btn-paym-box">
            <button  type="submit" class="btn btn-danger save-btn-paym">Update</button>
          </div>
        </form>
      @endif
      </div>
    </div>
    <div id="menu-two" class="tab-pane fade">
      <div class="owl-carousel owl-theme" id="slider2">
        <div class="item">
          <div class="month-name">
            <h4 class="march">January' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">February' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">March' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">April' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">May' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">June' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">July' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">August' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">September' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">October' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">November' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
        <div class="item">
          <div class="month-name">
            <h4 class="march">December' 19</h4>
            <p class="earnings-dollar">Earnings - $400</p>
          </div>
        </div>
      </div>
      <div class="earnings-heading">
        <h5>Total Earnings $400</h5>
      </div>
      <div class="pay-hist-wrapper">
        <div class="row">
          <div class="col-4 col-sm-6">
            <p class="cus-details-paym">Customer Name</p>
          </div>
          <div class="col-4 col-sm-3">
            <p class="cus-details-paym">Date</p>
          </div>
          <div class="col-4 col-sm-3">
            <p class="cus-details-paym">Earnings</p>
          </div>
        </div>
      </div>
      <div class="four-rows-paym">
        <div class="row-fst-paym">
          <div class="row">
            <div class="col-4 col-sm-6">
              <p class="row-one-details">John Doe</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">27/05</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">$5</p>
            </div>
          </div>
        </div>
        <div class="row-fst-paym">
          <div class="row">
            <div class="col-4 col-sm-6">
              <p class="row-one-details">John Doe</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">27/05</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">$5</p>
            </div>
          </div>
        </div>
        <div class="row-fst-paym">
          <div class="row">
            <div class="col-4 col-sm-6">
              <p class="row-one-details">John Doe</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">27/05</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">$5</p>
            </div>
          </div>
        </div>
        <div class="row-fst-paym">
          <div class="row">
            <div class="col-4 col-sm-6">
              <p class="row-one-details">John Doe</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">27/05</p>
            </div>
            <div class="col-4 col-sm-3">
              <p class="row-one-details">$5</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function(){
  $(".trigger-navbar").click(function(){
    $(".custom-navbar-pay-hist").slideToggle();
  });
  });
  
  $(document).ready(function(){
  $(".image-pay-hist").click(function(){
    $(".popup-payment").toggle();
  });
  });
  
  $('#slider2').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    responsive:{
        0:{
           items:1
        },
        600:{
            items:1
        },
        1000:{
            items:6
        }
    }
  })
  
</script>
@endsection